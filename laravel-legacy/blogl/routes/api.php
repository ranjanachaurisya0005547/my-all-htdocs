<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Route::get('/contacts',function(){
// 	echo "this is contacts route";
// });

Route::get('/contacts','contactcontroller@index');
Route::post('/contacts/create','ContactController@store');
Route::get('/contacts/show/{id}','ContactController@show');
Route::get('/contacts/delete/{id}','ContactController@destroy');
Route::get('/contacts/update/{id}','ContactController@update');


