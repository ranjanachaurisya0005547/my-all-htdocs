<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Posts;
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/posts',function(){
    $posts=Posts::all();
  //   dd($posts);
  $url = url('/posts/add');
  if(count($posts)>0):
  echo "<h1>All Posts Data</h1>";
  echo "<a href='$url'>Add Post</a>";
  echo "<table width='100%' border='1'>";
  echo "<thead>";
  echo "<tr><th>Title</th><th>Description</th><th>Created-at</th><th>updated-at</th><th>VIEW</th><th>EDIT</th><th>DELETE</th></tr>";
  echo "</thead>";
  echo "<tbody>";
  $i=1;
     foreach($posts as $post):
echo "<tr><td>{$post->title}</td><td>{$post->description}</td><td>{$post->created_at}</td><td>{$post->updated_at}</td><td><a href='".url("/posts/{$post->id}")."'>VIEW</a></td><td><a href='".url("/posts/edit/{$post->id}")."'>edit</a></td><td><a href='".url("/posts/delete/{$post->id}")."'>DELETE</a></td></tr>";
$i++;
     endforeach;
     echo "<tbody>";
     echo "</table>";
else :
    echo "no record found";
endif;
});
ROute::get('/posts/add',function(){

    $base_url=url('save'); # url is a function
    $csrf_token=csrf_token();
    #echo $csrf_token;
    echo "<h1>Created The Post</h1>";
    echo "<hr/>";

    echo <<<FORM
    <html>
    <head></head>
    <body>
    <h1>POST PAGE..</h1>
    <form action="/posts/save/" method="post">
    <p>Title:<input type="text" name="title"/></p>
    <p><input type="hidden" name="_token" value="{$csrf_token}"/></p>
    <p>Descripttion:<textarea name="desc"></textarea></p>
    <input type="submit" name="submit-btn"/>
    </form>
    </body>
    </html>
    FORM;
});

Route::post('/posts/save',function(Request $request){
  //Get the value from the Form
  $title = $request->get('title');
  $description = $request->get('desc');

  //Inserting in Database using single Assignment
//   $posts = new App\Post();
//   $posts->title=$title;
//   $posts->description=$desc;
//   $posts->save();

//Mass Assignment
$posts = App\Posts::create([
'title'=>$title,
'description'=>$description,
]);
#return redirect()->back();
return redirect()->to(url('posts'));
});



Route::get('/posts/{id}',function($id){

   $posts = new Posts();
   $res = $posts->find($id);
   echo "id = ".$res->id."<br>";
   echo "Title = ".$res->title."<br>";
   echo "Description = ".$res->description."<br>";
//$posts=Post::find($id);

#return $Posts; //Json data


// foreach($Posts as $Post):
//     echo $Post->id."<br/>";
//     echo $Post->title."<br/>";
//     echo $Post->description."<br/>";
// endforeach;
});

Route::get('/posts/delete/{id}',function($id){
    $posts = Posts::find($id);
    $posts->delete();
    return redirect()->back();
});

Route::get('/posts/edit/{id}',function($id){
   $posts = Posts::find($id);
//    echo $posts->id;
//    echo $posts->title;
//    echo $posts->description;
$csrf_token=csrf_token();

echo <<<FORM
    <html>
    <head></head>
    <body>
    <h1>POST PAGE..</h1>
    <form action="/posts/update" method="post">
    <input type="hidden" name="id" value="{$posts->id}"/>
    <p>Title:<input type="text" name="title" value="{$posts->title}"/></p>
    <p>Title:<input type="hidden" name="_token" value="{$csrf_token}"/></p>
    <p>Descripttion:<textarea name="desc">$posts->description</textarea></p>
    <input type="submit" name="submit-btn"/>
    </form>
    </body>
    </html>
    FORM;

});

Route::post('posts/update',function(Request $request){
$id=$request->id;
$title=$request->title;
$desc=$request->desc;

$posts = Posts::where('id',$id)->update([
    'title'=>$title,
    'description'=>$desc
]);
return redirect()->to(url('posts'));

});

//prefix routing in the previous version

// Route::get('/notes',function(){
// 	return "<h1><center>This is show Notes route !</center></h1>";
// });

// Route::get('/notes/create',function(){
// 	return "<h1><center>This is create Notes Route !</center></h1>";
// });

// Route::get('/notes/update',function(){
// 	return "<h1><center>This is update Notes Route !</center></h1>";
// });

// Route::get('/notes/delete',function(){
// 	return "<h1><center>This is Delete Notes Route !</center></h1>";
// });

//prefix routing which was supported by laravel legacyversion

Route::prefix('/notes')->group(function(){

    Route::get('/create',function(){
    	return "<h1><center>By Prefix This is create Notes Route !</center></h1>";
    });

    Route::get('/update',function(){
    		return "<h1><center>By Prefix This is update Notes Route !</center></h1>";
    });

    Route::get('/delete',function(){
    	return "<h1><center>By Prefix This is Delete Notes Route !</center></h1>";
    });

    Route::get('/show',function(){
       return "<h1><center>By Prefix This is Show Notes Route !</center></h1>";
    });

    Route::get('/',function(){
    	return "<h1><center>By Prefix This is Notes Route !</center></h1>";
    });

});

//blade.php file practice 
Route::get('/test',function(){
   return view('test');
});

//.blade.php file our another practice
Route::get('/dynamic-title',function(){
	$title="Dynamic Title Inside The Laravel ";
	$fruits=array('Mango','Banana','Pineapple','Apple','Grapes','Papaya','Lichy');
	return view('dynamic-title',compact('title','fruits'));
});


//crud using blade file
Route::get('/show',function(){
  return view('show');
});

Route::get('/insert',function(){
  return view('insert');
});


Route::get('/faqs',function(){
   return view('faq.add');//here dot used bcs this file is available outside the public folder
});

Route::get('/faqs','faqController@index');

// Route::get('/faq/add',function(){
//   return view('faq.add');
// });
// //Route::view('/faq/add','faq.add');
// Route::get('/faqs/create','faqController@create');
// Route::get('/faqs/edit','faqController@edit');
// Route::get('/faqs/update','faqController@update');
// Route::get('/faqs/delete','faqController@delete');

//Change Password Route
Route::get('/password/{id}','ChangepassController@index');
Route::post('/password/change','ChangepassController@changepass');

Route::view('/master/home','master.home_view');
Route::view('/master/about','master.aboutus_view');
Route::view('/master/contact','master.contactus_view');

// Route::view('/master','templates.master');












