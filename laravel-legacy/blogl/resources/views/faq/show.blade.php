<!DOCTYPE html>
<html>
<head>
	<title>Show The FAQ DATA</title>
</head>
<body>
    <h1>All Faq Data without Using Blade Tags</h1>
    <?php if(count($faqs)>0): ?>
    <table width="100%" border="1">
    	<tr>
    		<th>#</th>
    		<th>Question</th>
    		<th>Answer</th>
    		<th>Edit</th>
    		<th>Delete</th>
    	</tr>
    	<?php foreach($faqs as $key): ?>
    	<tr>
    		<td><?php echo $key->id; ?></td>
    		<td><?php echo $key->question; ?></td>
            <td><?php echo $key->answer; ?></td>
            <td><a href="#">Edit</a></td>
            <td><a href="#">Delete</a></td>
    	</tr>
    <?php endforeach; ?>
<?php endif; ?>
    </table>

    <h1>All Faq Data with Using Blade Tags</h1>
    @if(count($faqs)>0)
    <table width="100%" border="1">
    	<tr>
    		<th>#</th>
    		<th>Question</th>
    		<th>Answer</th>
    		<th>Edit</th>
    		<th>Delete</th>
    	</tr>
    	<?php foreach($faqs as $key): ?>
    	<tr>
    		<td>{{$key->id}}</td>
    		<td>{{$key->question}}</td>
            <td>{{$key->answer}}</td>
            <td><a href="#">Edit</a></td>
            <td><a href="delete.php?id={{$key->id}}">Delete</a></td>
    	</tr>
    @endforeach
@endif
    </table>
</body>
</html>