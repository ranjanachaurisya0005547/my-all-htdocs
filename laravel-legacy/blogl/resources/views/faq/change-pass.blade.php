<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <h1>Change Password Logic</h1>
    <hr/>

    <form action="{{url('password/change')}}" method="post" >
        <p> @csrf </p>
        <p><input type="hidden" name="email" value="{{$email}}"></p>
        <p>Old Pass: <input type="text" name="old_pass">  
        <?php isset($errors['old'])?print($errors['old']):""; ?> </p>
        <p>New Pass: <input type="text" name="new_pass"> </p>
        <p>Confirm Pass: <input type="text" name="cnf_pass"> 
        <?php isset($errors['new'])?print($errors['new']):""; ?> 
        <?php isset($errors['same'])?print($errors['same']):""; ?>
        </p>
        <p><input type="submit" name="submit" value="Change"> </p>
    </form>
    

</body>
</html>