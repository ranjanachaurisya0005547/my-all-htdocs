<!DOCTYPE html>
<html>
<head>
	<title>Registration...</title>
	<style>
		form{
			min-height:350px;
			width:40%;
			background:#ddd;
            padding:50px 150px;
            margin:2% 20%;
            font-size:20px;
            font-family:regular;
            border-radius:5px;
		}
        .form-control{
        	width:100%;
        	height:40px;
        	outline:none;
        	border:2px solid gray;
        	margin-top:3px;
        	border-radius:5px;
        	padding-left:15px;
        }
        .txtarea{
        	width:100%;
        	height:75px;
        	outline:none;
        	border:2px solid gray;
        	margin-top:3px;
        	border-radius:5px;
        	padding-left:15px;
        	padding-top:5px;
            font-size:19px;

        }

        input[type="submit"]{
        	width:50%;
        	height:47px;
        	margin:0 28%;
        	outline:none;
        	border:2px solid gray;
        	font-family:calibri;
        	font-size:22px;
        	border-radius:7px;
        }
	</style>
</head>
<body>
<center>
	<h1>Registraion Here......</h1>
</center>
	<form action="" method="POST">
		<p>
			Enter Your Name:<br/>
			<input type="text" name="name" class="form-control"/>
		</p>
		<p>
			Enter Your Email :<br/>
			<input type="email" name="email" class="form-control"/>
		</p>
        <p>
        	Enter Your Address:<br/>
           <textarea name="address" class="txtarea"></textarea>			
		</p>
		<input type="submit" name="sub-btn" value="SUBMIT"/>

	</form>
</body>
</html>