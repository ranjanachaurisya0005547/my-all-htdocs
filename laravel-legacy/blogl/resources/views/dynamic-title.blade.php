<!DOCTYPE html>
<html>
<head>
	<title>{{$title}}</title>
</head>
<body>
<center><h1><u><mark>This is Our blade file practice inside the laravel framework !</mark></u></h1></center>

<h2>Dynamic array value Bind The unordered List</h2>
<h3>By Using PHP Tags</h3>
<ul>
	<?php foreach($fruits as $key => $value):?>
		<li><?php echo $value; ?></li>
	<?php endforeach; ?>
</ul>
<h3>Without using PHP Tags !</h3>
<ul>
	@foreach($fruits as $key => $value):
        <li>{{$value}}</li>
	@endforeach;
</ul>
</body>
</html>