<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contacts;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contact = Contacts::all();

        $response = array();
        $response['code'] = 200;
        $response['status'] =true;
        $response['message']='record Found';
        $response['data'] = $contact ;

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo "okey";

        $errors = array();
        $response = array();

        foreach($request->all() as $field => $value){
              if(is_null($value) or empty($value)){
                 $errors[] = "$field is required !";
              }

              $response['code']=201;
              $response['status']=false;
              $response['message']="cannot add record";
              $response['error']=$errors;
              $response['error_count']=count($errors);

              if(count($errors)>0){
              return response()->json($response);
          }else{

               $contact=Contacts::create([
                    "name"=>$request->get('name'),
                    "email"=>$request->get('email'),
                    "mobile"=>$request->get('mobile'),
                    "message"=>$request->get('message')
               ]);
               $user_id=$contact->id;
               $user_data=Contacts::where('id',$user_id)->get();

               //$user_data=Contacts::find($user_id);
            
              $response['code']=200;
              $response['status']=true;
              $response['message']="record added successfully !";
              $response['data']=$user_data;
              $response['error_count']=count($errors);

              return response()->json($response);

          }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
            $errors=array();
            $data=array();
            $response=array();

            if(is_null($id) or empty($id)){

                  $errors['id']=" is missing !";

            }elseif(isset($id)){

                $count = Contacts::where('id',$id)->count();

                if($count>0){
                    $contacts = Contacts::find($id);
                    $data[]=$contacts;

                    $response['code']=200;
                    $response['status']=true;
                    $response['data']=$data;
                    $response['message']='Record Found';
                    $response['error']=$errors;
                    $response['error_count']=count($errors);
                    
            }else{

                    $errors['id']='Invaild Id supplied !';

                    $response['code']=201;
                    $response['status']=false;
                    $response['error']=$errors;
                    $response['message']='Record Not Found';
                    $response['error']=$errors;
                    $response['error_count']=count($errors);


                }
            }
            return response()->json($response);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $data=Contacts::where('id',$id)->update([
            
           'name'=>$request->get('name'),
           'email'=>$request->get('email'),
           'mobile'=>$request->get('mobile')
        ]);

        if($data==true){
                    $response['code']=200;
                    $response['status']=true;
                    $response['message']='Record Updated successfully';

        }else{
                    $response['code']=201;
                    $response['status']=false;
                    $response['message']='Record Not Updated successfully';
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
            $errors=array();
            $data=array();
            $response=array();

            if(is_null($id) or empty($id)){

                  $errors['id']=" is missing !";

            }elseif(isset($id)){

                $count = Contacts::where('id',$id)->count();

                if($count>0){
                    $user_id = Contacts::find($id);
                    $del=$user_id->delete();
                    
                    if($del==true){

                    $response['code']=200;
                    $response['status']=true;
                    $response['message']='Record Deleted successfully';
                    $response['error']=$errors;
                    $response['error_count']=count($errors);
                }else{
                     $response['code']=201;
                    $response['status']=false;
                    $response['message']='Record Not Deleted successfully';
                    $response['error']=$errors;
                    $response['error_count']=count($errors);
                }
                    
            }else{

                    $errors['id']='Invaild Id supplied !';

                    $response['code']=202;
                    $response['status']=false;
                    $response['error']=$errors;
                    $response['message']='Record Not Found';
                    $response['error']=$errors;
                    $response['error_count']=count($errors);


                }
            }
            return response()->json($response);

    }
}
