<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
class ChangepassController extends Controller
{
    //
    public function index(Request $request,$id){

        $errors = array();

        $msg = $request->get('msg');
        echo $msg;
        switch($msg){
            case 1:
            $errors['old'] = " Old Password Does not Match ";
            break;
            case 2:
                $errors['new'] = " New Password and Confirm Password Not Matched ";
                break;
            case 3:
                $errors['same'] = " New Password Cannot same as Old Password ";
                break;
        }

        $task = Task::find($id);
        $email = $task->email; 

        return view('change-pass',compact('email','errors'));


    }
    public function changepass(Request $request){
        
        $email = $request->get('email');
        $task = Task::where('email',$email)->get();
        $dbpass = $task[0]->password;
        $id = $task[0]->id;
        $old_pass = $request->get('old_pass');
        $new_pass = $request->get('new_pass');
        $cnf_pass = $request->get('cnf_pass');

        if($dbpass == $old_pass):   
            if($new_pass ==  $cnf_pass):
                if($cnf_pass == $old_pass):
                    return redirect()->to(url("password/{$id}?msg=3"));
                else:  
                    $task = Task::find($id);
                    $task->password = $cnf_pass;
                    $task->save();
                    echo 'Password Updated Successfully';
                endif;
            else:
                return redirect()->to(url("password/{$id}?msg=2"));
            endif;
        else:
            return redirect()->to(url("password/{$id}?msg=1"));
        endif;
    }
}
