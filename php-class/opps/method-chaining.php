<?php
/******************

method chaining:
it is a concept of chaining the attributes and methods together with a single object.

if your class does not support method chaining then it lead to null object.
#Exception
can not call to methodname() to null object.
******************/

class Test{

	public $a;

	public function setA($a){
		$this->a = $a;
		//TO enable chaining
		return $this;
	}

	public function getA(){
		echo "a = {$this->a}";

		//to enable the method chainging
		return $this;
	}


}
$obj=new Test();
#method chaining
$obj->setA(100)->getA()->setA(200)->getA();

#method chainning
echo "<br/>Method chainning";
$obj->setA(100)
     ->getA()
     ->setA(200)
     ->getA();