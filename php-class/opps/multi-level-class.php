<?php

 class Test1{

 	public function display1(){
 		echo "Namskarm from".__CLASS__."from method ".__METHOD__." and ".__FUNCTION__;
 	}

 }

 class Test2{

 	public function display2(){
 		echo "<br/>namaskarm from ".__CLASS__." from method ".__METHOD__." and ".__FUNCTION__;
 	}

 }

 #main driver part
 #class Test1 object
 $test1=new Test1();
 $test1->display1();

#$test1->display2(); # Call to undefined method Test1::display2()
 #class Test2 object

 $test2=new Test2();
 $test2->display2();