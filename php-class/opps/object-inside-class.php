<?php
#Object creation inside the class
#or instance within the class

class Papa{
  
  private $balance=100;

  public function car(){
  	echo "This is a car of papa<br/>";
  }

  public function bankbalance(){
  	  echo "Get Balance ";
  	  echo "{$this->balance}<br/>";
  }

  public function setbalance($money){
  	$this->balance=$this->balance+$money;
  }


}

class Son{

	public function bike(){
		echo "This is boke of son !<br/>";
	}

	
	public function getdependency(){
		$papa=new Papa();//local variable holds the instance(object) of the class papa
		$papa->bankbalance();
		$papa->setbalance(-500);
		$papa->bankbalance();
	}

	public function __destruct(){
          echo "<hr/>Destruct called !";
          var_dump($papa);
	}

}

#object of the class papa
$papa=new Papa();#this independent
$papa->bankbalance();
$papa->car();
#object of class son

$son=new Son();  #Its also independent
$son->bike();
$son->getdependency();

#if you want to achieve dependency of the class
#you have two options
#1)Go for Inheritance
#2)dependency Injection
