<?php

//real time example
class Papa{
	public $balance=0;

	public function __construct(){
		$this->credit();
	}

	private function credit(){
		$this->balance=$this->balance+5000;
	}

	public function withdraw(){
		return $this->balance;
	}

}

class Child extends Papa{

	public function __construct(){
		echo "child constructor Before parent Cunstructor call<br/>";
		echo "Withdrawal Money =".$this->withdraw();
		echo "<br/>";
		echo "Total Balance =".$this->balance;
      
         parent::__construct();
		echo "<br/>child constructor after parent Cunstructor call<br/>";
		echo "Withdrawal Money =".$this->withdraw();
		echo "<br/>";
		echo "Total Balance =".$this->balance;
	}
	
}
//child class object
$child = new child();
