<?php

#cunstructor Overridding

class Papa{

 public $x;
   public function __construct(){
   	  echo "papa constructor <br/>";
   	  $this->x=100;
   }

}

//child class
class Child extends Papa{
 
   public function __construct(){

   	//out of scope
   	echo "Hii<br/>";

//parent constructor is calling
   	  parent::__construct();

   	  //inside the scope
   	  echo "Child cunstrutor <br/>";
   }

}
//Child class object
$child=new Child();

//$papa=new Papa();