<?php 

class Test{

	//costructor
	public function __construct(){
		echo "I am constructor !<br/>";
	}

	//destructor
	public function __destruct(){
		echo "I am destruct !";
	}

	//public 
	public function display(){
		echo "this is display method of class Test !<br/>";
	}

}
$obj= new Test();
$obj->display();
/**********************
NOte:destruct() always be called at the end of the program ,if nothing to remains to execute then destruct is executed.

******************/