<?php

#class file are the core file

class Student{
	//Data member /states/attributes/variable/instance/instance variable/instance/instance
	//default access modifier member
	var $name; #undefined
	var $myclass; #undefined
	
	//memeber function/methods/behaviour/actions
	//default access modifier methods
	
	function getname($name){//$name is formal argument of method
		echo $this->name;
	}
	
	function getclass($class){//$class is formal argument of method
		echo $this->myclass;
		
	}
	
}

#since class till now have not got any memory allocation hence class is a logical Entiry
#where is object gets memory allocation in heap and its signature is given in stack.
#Hence Objects is a Physical Entity.

#instantiating class
/****
       $x=  //when we assign the value/datatypeinside the $x then its store at the stack memory.
        |
		|---->stack
		
		#when we provide a variable a datatype then that's called Initialization 
		
		$x=new className()
		here new keyword defines that provide the memory inside the heap,which makes a variables
		to diffrent from another normal variable.
		
		we call it initialization of class or making of object ,its also called Initiating
		of class.
		#Instantiating inside heap
		
		Incase of vailid Pointers:
		$x=&10;//pointer its store inside the heap
		$this is also is pointer
		
		


***/

//garbage collector:its responsible to providing the null value to the undefined variable.
var_dump((new Student())->name);//output:NULL
echo "<br/>";
var_dump(new Student());//output:object(Student)#1 (2) { ["name"]=> NULL ["myclass"]=> NULL }

//Anonymous variable:which variable has no name that's called anonymous variable,object is a 
//anonymous variable

//using object you can access any member and any method under its scope(context).

echo "<hr/>";
var_dump((new Student()));//create anonymous variable
//output:object(Student)#1 (2) { ["name"]=> NULL ["myclass"]=> NULL }

#Refrence variable = name object
echo "<hr/>";
$obj= new Student();
var_dump($obj); //create the named object
//output:object(Student)#1 (2) { ["name"]=> NULL ["myclass"]=> NULL }
echo "<hr/>";
var_dump($obj->name);//output:NULL






























