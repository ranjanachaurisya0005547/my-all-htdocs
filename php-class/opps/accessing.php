<?php
#Once Object is created you can access the non-static(context) members and member functions
class Student{
	var $name="ranjana"; //default public
	var $myclass=12; //default public
	
	
	function getname(){
		echo $this->name;
	}
	
	function getclass(){
		echo $this->myclass;
		
	}
	
}
$std_obj=new Student();#refrence variable to Student class

#How to acces the member variables
echo "The Student name $std_obj->name<br/>";
echo "Student class $std_obj->myclass<br/>";

echo "The Student name {$std_obj->getname()}<br/>";
echo "Student class {$std_obj->getclass()}<br/>";

echo "The Student name $std_obj->getname()<br/>";
echo "Student class $std_obj->getclass()<br/>";

echo "The Student name ";
echo $std_obj->getname();
echo "<br/>";
echo "Student class ";
echo $std_obj->getclass();
echo "<br/>";

//outputs:
// The Student name ranjana
// Student class 12
// ranjanaThe Student name
// 12Student class

// Notice: Undefined property: Student::$getname in C:\xampp\htdocs\php-class\opps\accessing.php on line 27
// The Student name ()

// Notice: Undefined property: Student::$getclass in C:\xampp\htdocs\php-class\opps\accessing.php on line 28
// Student class ()
// The Student name ranjana
// Student class 12

