<?php

//static contect within the class

class Test{

	private static $x;

	#Non static method
	public function display(){
		echo "non static method running !<br/>";
	}

	public static function display2(){
		echo "static method is running !<br/>";
	}

}

#Main Driver Code
#calling of the static method 
Test::display2();

#calling of the Non-static method
Test::display();//its generates the deprecated error in the above the v.5.* of the php

/** Note:Best Way:you always acces the non-static from the non-static context and static to static context.**/
$obj1=new Test();

$obj1->display();
$obj1->display2();








