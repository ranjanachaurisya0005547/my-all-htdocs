<?php

//$this:$this is refere to the current scope of the object.hence $this is called super refrence object.

class Test{

	public $x=100;
	public $y=200;

	public function setvalue($x,$y){

		$sum=$x+$y;
		echo "Value pf the local variable x = {$x}<br/>";
		echo "Value of the local variable y= {$y}<br/>";
		echo "sum of the local variable sum = {$sum}<br/>";

		echo "Accessing the class lavel variable <br/>";
		echo (new Test())->x;
		echo (new Test())->y;

		//instead of the new Test() anonymous class object we can use the $this operator which point to the current class object

		echo "<br/>";
		echo $this->x;
		echo $this->y;
	}

}
$obj = new Test();
$obj->setvalue(10,20);
//$x and $y public
//note :$this can only used inside the class only

echo "<br/>";
var_dump($obj);

echo "<br/>";
//var_dump($this);

echo "<br/>";
//$this=$obj;
//echo $this;
//output:Fatal error: Cannot re-assign $this in C:\xampp\htdocs\php-class\opps\this1.php on line 39

echo "Accessing the class level object out side the class :<br/>";
echo $obj->x;
echo $obj->y;













