<?php

/**************
In oops there is two types of context
1)static context

2)Non-Static Context


class:properties +method
how to define class in terms these two context.

static context:
    class :static properties+static methods
    using object no allowed 
    this means current also can not be used .$this not allowed .

    Non static context:
    class :normal properties +normal method

    How to declare static context:
    use the static keyword

    Syntax:
    properties
    <access-modifier> static $<properties-name>;
    ex:
    public static $x;

    Methods:
    <access-modifier> static function <function-name>(){
	
    }

    #class with static context and non static context is possible.
    #static context will class rather than object.
    Non static context will use object rather than class.
**************/