<?php

#PSR-4 Autoloading
#Namespacing autoloading

spl_autoload_register(function($namespace){
  
  $path=__DIR__."/vendor/{$namespace}.php";
  
  require_once $path;

});