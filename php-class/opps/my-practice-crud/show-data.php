<?php
require_once __DIR__."/dbconnection.php";

$query="select * from tbl_logs";
$result_set=mysqli_query($con,$query);
$count=mysqli_num_rows($result_set);

if($count>0):

?>
<table border="2" cellspacing="0" cellpadding="10">
	<tr>
		<th>ID</th>
		<th>NAME</th>
		<th>ACTION</th>
		<th>Delete</th>
		<th>Edit</th>
	</tr>
	<?php while($row=mysqli_fetch_assoc($result_set)): ?>
		<tr>
			<td><?php echo $row['id']; ?></td>
			<td><?php echo $row['name']; ?></td>
			<td><?php echo $row['action']; ?></td>
			<td><a href="delete-data.php?id=<?php echo $row['id'];?>">Delete</a></td>
			<td><a href="edit-data.php?id=<?php echo $row['id']; ?>">Edit</a></td>
		</tr>
	<?php endwhile; ?>
	<?php else: ?>
		<h1>No Record Are Available In database !</h1>
    <?php endif; ?>		
</table>