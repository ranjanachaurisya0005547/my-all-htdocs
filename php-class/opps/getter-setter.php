<?php

//getter:method used to get the values from the instance variables
//setter:method is used to set the values  of the instance variables

//getters:getsomething():return or may not return or echo
//setters:setsomething($arg1,$arg2,...)

class Area{


	private $x;
	private $y;

	#setters

	public function setX($x){
		$this->x=$x;
	}

	public function setY($y){
		$this->y=$y;
	}

	//getters

	public function getX(){
		echo $this->x;
	}

	public function getY(){
		var_dump($this->y);
	}
}

$obj = new Area();

//echo $obj->x;  //we can not access the private data member x
//echo $obj->y;   // can not access the private data member y

$obj->getX();
$obj->getY();

$obj->setX(20);
$obj->setY(40);

$obj->getX();
$obj->getY();







