<?php

//Instanceof :instanceof operator is used to check weather a object is instance of given class

class student{

	public $name="ranjana";
	public $class="12th";

	public function display(){
		echo $this->name;
		echo $this->class;

	}

}

//making object of the class
$obj1=new Student();
$obj1->display();
var_dump($obj1 instanceof Student);


echo "<br/>";
$obj2=new Stdclass();
var_dump($obj2 instanceof Stdclass);
var_dump($obj1 instanceof Stdclass);
var_dump($obj2 instanceof Student);


