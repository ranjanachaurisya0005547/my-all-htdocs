<?php

class Database{

	private $host;
	private $user;  
	private $pass;
	private $dbname;
	public $con;

	//parameterized custructor                                                                 

	public function __construct($host,$user,$pass,$dbname){

		$this->host=$host;
		$this->user=$user;
		$this->pass=$pass;
		$this->dbname=$dbname;
		$this->connect();
	}

//connect database
	private function connect(){

        
		   $con=mysqli_connect($this->host,$this->user,$this->pass,$this->dbname) or die("connection error ".mysqli_connect_error($con));
         
		$this->con=$con;

		  
	 }

    //return the database connetion object
	 public function getconnectionobject(){
	 	return $this->con;
	 }

}

#for student databse
$db1=new Database('localhost','student','12345','student');
$sql="select * from tbl_user";
$result_set=mysqli_query($db1->getconnectionobject(),$sql);
#$count=mysqli_num_rows($result_set);

if($row=mysqli_fetch_assoc($result_set)){
	echo $row['email'];
}

echo "<hr/>";
//for the teacher database

$db2=new Database('localhost','teacher','1234','teacher');
$sql1="select * from tbl_user";
$result_set=mysqli_query($db2->getconnectionobject(),$sql1);

if($row=mysqli_fetch_assoc($result_set)){
	echo $row['email'];
}











