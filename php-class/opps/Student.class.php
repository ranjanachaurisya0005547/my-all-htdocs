<?php
#parameterized class:In which we pass the parameter,parenthesis are mendatory
#Non Parameterized class:In which we do not pass parameter hence parenthesis are not 
#mendetory
class Student{
	var $name; 
	var $myclass; 
	
	
	function getname($name){
		echo $this->name;
	}
	
	function getclass($class){
		echo $this->myclass;
		
	}
	
}

$std_obj1=new Student();//Parameterized class to object level
var_dump($std_obj1);
echo "<hr/>";

$std_obj2=new Student;//Non-Parameterized class to object level
var_dump($std_obj2);
echo "<hr/>";
$std_obj3=new student();//parameterized class to object level
var_dump($std_obj2);
//outputs:
//object(Student)#1 (2) { ["name"]=> NULL ["myclass"]=> NULL }
//object(Student)#2 (2) { ["name"]=> NULL ["myclass"]=> NULL }
//object(Student)#2 (2) { ["name"]=> NULL ["myclass"]=> NULL }

//class name is initiating is not case sensitive


