<?php

class Student{
	private $name="ranjana"; 
	private $myclass=12; 
	
	
	function getname(){
		echo $this->name;
	}
	
	function getclass(){
		echo $this->myclass;
		
	}
	
	function setname($name){
		$this->name=$name;
	}
}
$std_obj=new Student();

/************************
//Accessing the private member

echo "The Student name $std_obj->name<br/>";
echo "Student class $std_obj->myclass<br/>";

Output:
Fatal error: Uncaught Error: Cannot access private property Student::$name in 
C:\xampp\htdocs\php-class\opps\correct-pattern.php:20 Stack trace: #0 {main} thrown in 
C:\xampp\htdocs\php-class\opps\correct-pattern.php on line 20

**************************/

echo "The Student name ";
echo $std_obj->getname();
echo "<br/>";
echo "Student class ";
echo $std_obj->getclass();
echo "<br/>";

// Output:
// The Student name ranjana
// Student class 12

//Accesing methods
$std_obj->getname();
echo "<br/>";
$std_obj->setname('hii');
$std_obj->getname();

// Outputs:
// ranjana
// hii

