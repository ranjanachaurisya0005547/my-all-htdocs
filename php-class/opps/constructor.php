<?php

class Test{

   #constructor
	public function __construct(){
		echo "I will called at the time of object creation ! <br/>";
	}

   #constructor
	public function Test(){
		echo "I also called at the time of the object creation !<br/>";
	}

    #display function
	public function display(){
		echo "I will called with the help of the class object !<br/>";
	}
}

$obj1=new Test();
$obj1->display();