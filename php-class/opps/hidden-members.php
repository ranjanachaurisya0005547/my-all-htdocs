<?php

class Test{

	public $y=100;
	//inside class security remains same
	public $z=200;
	protected $w=300;
	private $c;

	public function disp1(){
		$x=10;
		$this->x=$x;  //hidden members
		echo "The local value of x from disp1 is x={$x}<br/>";
		echo "The Local value of y={$this->y}<br/>";
		echo "value of w={$this->w}<br/>";
		echo "value of c before";
	    var_dump($this->c);
        $this->c=$x;
        echo "value of c After =";
        var_dump($this->c);

	}

	public function disp2(){
		echo "value of from disp2 x={$x}<br/>";
		echo "value of x from disp2 x={$this->x}<br/>";
		echo "value of y ={$this->y}";
		echo "value of w ={$this->w}";
		echo "value of c ={$this->c}<br/>";
	}
}

//craeting objectnof the class Test
$test= new Test();

$test->disp1();
$test->disp2();

echo "<br/>";
echo "<pre>";
var_dump($test);