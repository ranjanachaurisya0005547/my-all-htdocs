<?php

function write_log($data_arr){

	$ip=$_SERVER['REMOTE_ADDR'];
	$fp=fopen('output.txt',"a+");

	$code = "";

	$data = "Data Written On ".date('Y-m-d')."at time ".date('H:i:s')."\n";
	$data .= "Ip OF REQUEST = {$ip} \n";

	foreach($data_arr as $line => $variable){

		if(is_array($variable)){
			$variable= json_encode($variable);
		}

		 $code .= "{$line} = {$variable} \n";

	}

	fwrite($fp,$data.$code);
	fclose($fp);

}