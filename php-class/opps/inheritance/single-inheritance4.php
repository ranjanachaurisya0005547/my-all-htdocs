<?php

class Papa{
 
	public function car(){
		echo "papa ki car <br>";
	  	}

	public function truck(){
			echo "papa ki truck <br/>";
	}

	public function house($won_class=__CLASS__){
		echo $won_class." Ka Ghar !<br/>";
	}

	private function insorance(){
		echo "papa ki private property !<br/>";
	}
}

class Child{

	public function Bike(){
		echo "Child ki Bike<br/>";
	}

	public function getParantProperties($classname,$method){
		$this->papa=$classname;
		$this->papa->$method();
	}

	public function myHouse(){
		$this->papa->house(__CLASS__);
	}

	public function getinsorance(){
		$this->papa->insorance();
	}


}

#papa class object
$papa =new Papa();
$papa->car();

#child class object
$child=new Child();
$child->Bike();
$child->getParantProperties(new Papa(),'car');
$child->getParantProperties(new Papa(),'truck');
$child->getParantProperties(new Papa(),'House');

//try to access the private function of the class papa
$child->myHouse();
$child->getinsorance();







