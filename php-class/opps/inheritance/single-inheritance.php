<?php

#single Inheritance with no relationship

class Papa{

	public function car(){
       echo "Papa's Car !<br/>";
	}

	public function truck(){
		echo "Papa's truck !<br/>";
	}

}

//Child class
class Child{

	public function Bike(){
		echo "This is child bike<br/>";
	}

	public function car(){
		echo "This is child car<br/>";
	}
	public function truck(){
		echo "this is child truck<br/>";
	}
}

#Main Driver Code
#parent class object

$papa=new Papa();
$papa->car();
$papa->truck();

#child class object
$child=new Child();
$child->Bike();
$child->car();
$child->truck();                  


