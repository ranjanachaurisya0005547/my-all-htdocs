<?php       




#single Inheritance with no relationship

class Papa{

	public function car(){
       echo "Papa's Car !<br/>";
	}

	public function truck(){
		echo "Papa's truck !<br/>";
	}

}

//Child class
class Child extends Papa{

	public function Bike(){
		echo "This is child bike<br/>";
	}

	
}

#Main Driver Code

#child class object
$child=new Child();
$child->Bike();
$child->car();
$child->truck();                  


