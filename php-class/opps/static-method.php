<?php
/**********************
#static method from outside the class
if object is created then first priority to run the custructor after that any non-static methhod or data members can run.

In class Level :An static method can run before the constructor.

**********************/

class Test{

    #static context
	public static function run(){
		echo "This is static Run Function<br/>";
	}

	#constructor
	public function __construct(){
		echo "I am constructor<br/>";
	}

    #non-static context
	public function sayHello(){
		echo "Hello From non-static Context <br/>";
	}
}

$obj=new Test();