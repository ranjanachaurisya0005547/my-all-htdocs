 <?php

 class Test{
 public $x;
 public $y;
 public $z;

 public function __construct($x,$y,$z){

 	$this->x=$x;
 	$this->y=$y;
 	$this->z=$z;

 	echo "Callin the inside class :";
 	$this->getValue();

 }

 public function getValue(){

 	echo "$this->x ";
 	echo "$this->y ";
 	echo "$this->z ";    
 }

}
$obj=new Test(10,20,30);

echo "<br/>Calling outside the class :";
$obj->getValue();