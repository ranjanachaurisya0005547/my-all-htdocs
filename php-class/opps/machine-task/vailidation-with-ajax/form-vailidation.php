<!DOCTYPE html>
<html>
<head><!-----ajax error network me------->
	<title>Vailidation With Ajax</title>
</head>
<body>
	<h1>Vailidation With Ajax</h1>
	<form action="<?php echo basename($_SERVER['PHP_SELF']); ?>" method="POST">
        <p>
            Enter Your Name<br/>
            <input type="text" id="name"/>
            <span id="name-error"></span>
         </p>
         <p>
            Enter Your Mobile  Number<br/>
            <input type="text" id="mobile"/>
            <span id="mobile-error"></span>
         </p>
         <input type="submit" name="sub-btn" value="Submit"/>
	</form>	
</body>
<script src="jquery.js" type="text/javascript"></script>
<script type="text/javascript">
	

    var base_url ="http://localhost:8000/php-class/opps/machine-task/vailidation-with-ajax/";
    $(document).ready(function(){

       $("#mobile").on("keyup",function(){
            
            var mobile_no=$("#mobile").val();
            $.ajax({
               url:base_url+"ajax-vailidation/vailidate-mobile.php",
               type:"POST",
               data:{
                   mobileno:mobile_no,
               },
               success:function(responce){
                    $("#mobile-error").html(responce);

               }
            });
 
       });

       //name vailidation
       $("#name").on("keyup",function(){
            
            var uname=$("#name").val();
            $.ajax({
               url:base_url+"ajax-vailidation/vailidate-name.php",
               type:"POST",
               data:{
                   name:uname,
               },
               success:function(responce){
                    $("#name-error").html(responce);

               }
            });
 
       });
    });

</script>
</html>