<?php

#File Include

spl_autoload_register(function($namespace){
	
	$vendor_path = __DIR__.'/vendor';

	require_once "{$vendor_path}/{$namespace}.php";

});

#File Include

