<?php
require_once __DIR__.'/crud/dbconnect.php';
$key=isset($_GET['id'])?$_GET['id']:NULL;
if(!(is_null($key))){
	echo "Data is Updated successfully !";
}

$select_query="SELECT * FROM information";
$result_set=mysqli_query($con,$select_query);
$count=mysqli_num_rows($result_set);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Show Employee Information</title>
</head>
<body>
	<center>
	<table border="1" cellspacing="0" cellpadding="10">
		<tr>
			<td>User Id</td>
			<td>Name</td>
			<td>Email</td>
			<td>Password</td>
			<td>Mobile Number</td>
			<td>City</td>
			<td>Image</td>
			<td>Edit</td>
			<td>Delete</td>
		</tr>
		<?php if($count>0): ?>
		<?php while($row=mysqli_fetch_assoc($result_set)): ?>
			<tr>
				<td><?php echo $row['id']; ?></td>
				<td><?php echo $row['name']; ?></td>
				<td><?php echo $row['emial']; ?></td>
				<td><?php echo $row['password']; ?></td>
				<td><?php echo $row['mobile']; ?></td>
				<td><?php echo $row['city']; ?></td>
				<td><?php echo $row['file']; ?></td>
				<td><a href="edit.php?id=<?php echo $row['id']; ?>" style='color:red;font-size:20px;'>Edit</a></td>
				<td><a href="delete.php?id=<?php echo $row['id']; ?>" style='color:red;font-size:20px;'>Delete</a></td>
			</tr>
	    <?php endwhile; ?>	
	    <?php endif; ?>	
	</table>
</center>
</body>
</html>