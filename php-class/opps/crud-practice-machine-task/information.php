<?php
require_once __DIR__."/crud/dbconnect.php";

$name_error="";
$email_error="";
$pass_error="";
$mobile_error="";
$file_error="";

if($_SERVER['REQUEST_METHOD']=='POST'){
    if(isset($_POST['sub-btn']) and !empty($_POST['sub-btn'])){
       
          $name=isset($_POST['name'])?$_POST['name']:NULL;
          $email=isset($_POST['email'])?$_POST['email']:NULL;
          $password=isset($_POST['password'])?$_POST['password']:NULL;
          $mobile=isset($_POST['mobile'])?$_POST['mobile']:NULL;
          $city=isset($_POST['city_name'])?$_POST['city_name']:NULL;
          $fileupload=isset($_FILES['fileupload'])?$_FILES['fileupload']:NULL;

          $filename=$fileupload['name'];
          $file_tmpname=$fileupload['tmp_name'];
          $file_type=$fileupload['type'];
          $file_error=$fileupload['error'];
          $file_size=$fileupload['size'];
         

   
          if(is_null($name) or empty($name)){
          	  $name_error="Name can Not Be Empty !";
          }

          if(is_null($email) or empty($email)){
          	  $email_error="Email can Not Be Empty !";
          }

          if(is_null($password) or empty($password)){
          	 $pass_error="Password is can not be empty !";
          }

          if(is_null($mobile) or empty($mobile)){
          	  $mobile_error="Mobile Number Can Not Be Empty !";
          }

          if(is_null($fileupload) or empty($fileupload)){
          	$file_error="File can not be empty !";
          }


         if(empty($name_error) or empty($email_error) or empty($pass_error) or empty($mobile_error)){

         	
         	$upload_path=__DIR__."/uploads-file/".$filename;
         	if(move_uploaded_file($file_tmpname, $upload_path)){
                
         			$insert_query="INSERT INTO Information (name,emial,password,mobile,city,file) values('$name','$email','$password','$mobile','$city','$filename')" or die("Insert Query error ".mysqli_connect_error($con));
         	
         	
      
         	if(mysqli_query($con,$insert_query)){
         		echo "Data inserted successfully";
         	}else{
         		echo "Data not inserted".mysqli_error($con);
         	}

          }else{
          	echo "File Not Uploaded not successfully";
         }	

         	
         }
          

    }else{
    	echo "<span class='error'>Invailid Button Type !</span>";
    }
}else{
	echo "<span class='error'>Invaild Request !</span>";
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>User Information Inserttion</title>
	<style>
		.error{
			color:red;
			font-size:19px;
			text-transform:capitalize;
		}
	</style>
</head>
<body>
	<h1>User Information................</h1>
<form action="<?php echo basename($_SERVER['PHP_SELF']); ?>" method="POST" enctype="multipart/form-data">
   <p>
   	Enter Your Name<br/>
   	<input type="text" name="name" id="name" value="<?php echo isset($name)?$name:""; ?>"/>
   	<span class="error"><?php echo $name_error; ?></span>
   </p>
   <p>
   	Enter Your Email<br/>
   	<input type="email" name="email" id="email" value="<?php echo isset($email)?$email:""; ?>"/>
   	 	<span class="error"><?php echo $email_error; ?></span>
   </p>
   <p>
   	Enter Password<br/>
   	<input type="password" name="password" id="password" value="<?php echo isset($password)?$password:""; ?>"/>
   	 	<span class="error"><?php echo $pass_error; ?></span>
   </p>
   <p>
   	Enter Your Mobile Number<br/>
   	<input type="text" name="mobile" id="mobile" value="<?php echo isset($mobile)?$mobile:""; ?>"/>
   	 	<span class="error"><?php echo $mobile_error; ?></span>
   </p>
   	<p>
   		<?php 
                $city_select="select * from city";
                $city_resultset=mysqli_query($con,$city_select) or die("Connectio Error ".mysqli_connect_error($con));
                $count=mysqli_num_rows($city_resultset);
                
                if($count>0):

   			?>
   		Select Your City<br/>
   		<select name="city_name">
   			<?php 
                 while($city_arr=mysqli_fetch_assoc($city_resultset)):
   			?>
   			<option><?php echo $city_arr['city_name']; ?></option>
   		<?php endwhile; endif; ?>
   		</select>
   	</p>
    <p>
    	Upload File<br/>
    	<input type="file" name="fileupload" id="file" value="<?php echo isset($fileupload)?$fileupload:""; ?>"/>
         <span class="error"><?php echo $file_error; ?></span>
    	<br/><br/>
    <input type="submit" name="sub-btn" value="SUBMIT">
</form>	
</body>
</html>