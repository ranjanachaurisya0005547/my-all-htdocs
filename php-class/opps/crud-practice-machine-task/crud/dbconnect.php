<?php

$host="localhost";
$user="root";
$pass="";
$dbname="test_db";

try{
	$con=mysqli_connect($host,$user,$pass,$dbname);

	if(!$con){
		throw new Exception;
	}
}catch(Exception $ex){
	echo "Error ".mysqli_connect_error($con);
	echo $ex->getMessage();
}

