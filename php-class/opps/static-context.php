<?php

#static context example
class Test{
	
	public static $x=10; #static contxet No object possible
	public $y=500;  #Non static object possible

	#context non-static
	public function __construct(){
         
         echo "Non static constructor running<br/>";
         echo "the value of y in non static context {$this->y}<br/>";

         #Test::$x;
         #self::$x;
         #Test::$y;
         #self::$y;
	}
/*************
Static context:Leads error
public static function __construct(){
	echo "static constructor running !";
}
*************/

}

$obj=new Test();
echo $obj->y;
echo $obj->x; #error static context 

echo "How to access static context member x<br/>";
#echo self::$x;

#using className
echo Test::$x;