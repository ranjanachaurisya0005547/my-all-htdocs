<?php

#Achive Method Overloading

class Shape{

public function __construct(){
	echo "This is method overloading example !<br/>";
}

public function __call($functionname,$argument){
  
  // echo "Function name : {$functionname} and <br/>";
  // print_r($argument);
  //output: This is method overloading example !
  //  Function name : area and
  //  Array ( )

	switch(count($argument)):
     
     case 1:
       echo PI()*$argument[0]*$argument[0]."<br/>";
     break;

     case 2:
       echo $argument[0]*$argument[1]."<br/>";
     break;

     case 3:
       echo $argument[0]*$argument[1]*$argument[2]."<br/>";
     break;

     default:
     echo "This is Area() Method !<br/>";
     break;

	endswitch;
}

}

#Driver Code

$shape =new Shape();
$shape->area();
$shape->area(10);
$shape->area(10,20);
$shape->area(10,20,30);