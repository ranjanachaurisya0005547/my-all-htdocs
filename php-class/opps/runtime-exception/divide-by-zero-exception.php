<?php

$no1=10;
$no2=3;

$result=$no1/$no2;
echo "result without exception= {$result}<br/>";

$no1=57;
$no2=0;

//$result=$no1/$no2;
//echo "Ressult with Exception = {$result}<br/>";

try{
     if($no2==0){
     	throw new Exception("Try Another No");
     }
     $result=$no1/$no2;
     echo "Result is = ".$result;

}catch(Exception $ex){
     echo "Excention occured !";
}
