<?php
class Test{
	private $x=100;
	private function display(){
		echo "The Display function from test<br/>";
	}
	public function hi(){
		echo "Hii from Test";
	}
}
#class child
class Child extends Test{
	private $y=100;
	private function hello(){
		echo 'Hellow from child <br/>';
	}
	public function __construct(){
		echo "Hellow from the child constructor<br/>";
		#calling hello
		$this->hello();  // private function of child
		#calling private y
		echo "<hr/>";
		echo $this->y;
		echo "<hr/>";
		#calling private x
		#echo $this->x;
		$this->hi();
		$this->display();
		
	}
}
#object
$child=new  Child();
#Accessing child private member from outside
#$child->hello();#Fatal Error
#$child->y;