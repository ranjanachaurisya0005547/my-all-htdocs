<?php
#without Inheritance public accessblity
class Test{
	public $x=100;
	public $y=200;
	#scope public:within the class
	public function display(){
		#public: within the class
		echo $this->x;
		echo $this->y;
	}
	public function __construct(){
		#within the class
		$this->display();
		echo "Access inside the constructor<br/>";
		echo $this->x;
		echo $this->y;
	}
}
#object
#Public scope outside the class
$test=new Test();