<?php

#using Singleton class
class Test{
	private static $object;
	private function __construct(){
		echo 'Private constructor';
	}
	public static function createObject(){
		self::$object=new Test();
	}
	public static function getObject(){
		if(self::$object==NULL){
			self::createObject();
			
		}else{
			return self::$object;
		}
	}
	
}
#$test=new Test();
Test::getObject();
Test::getObject();
Test::getObject();
Test::getObject();