<?php
class Test{
	private function __construct(){
		echo "Constructor running <br/>";
	}
}

$test1=new Test(); #you cannot Make Private Constructor
                    #constructor should be always public