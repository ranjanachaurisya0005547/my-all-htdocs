<?php
#Example of private:without Inheritance
class Papa{
	private $x=99;
	private function display(){
		echo 'The value of x from inside private function';
		$this->x;
	}
	public function __construct(){
		echo 'The public constructor<br/>';
		echo $this->display();
	}
}

$papa=new Papa();
#$this->display();   #Fatal Error can not be accessed
#echo $this->x; #Fatal Error can not be accessed

#if you want to call this-
echo "<hr/>";
$papa->__construct();
