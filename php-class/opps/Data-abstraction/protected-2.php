<?php
class Papa{
	protected $property=" Your house";
	private $pention="20 laks Rs";
	public function vakil(){
		echo $this->property;
	}
	
	protected function getProperty(){
		echo $this->pention;
	}
}

#child class
class Child extends Papa{
	public function __construct(){
		echo " Child constructor called....";
		echo $this->property;
		echo "<hr/>";
		echo $this->pention;
		echo '<hr/>';
		echo $this->vakil();
		echo "Protected property from the child context<br/>";
		$this->getProperty();
	}
}
$child= new Child();
