<?php
class Papa{
	public $x=100;
	public function display(){
		echo "Display from class Papa<br/>";
	}
}
#within the second class
class child extends Papa{
	public function __construct(){
		#var_dump($this);
		echo $this->x;
		echo "<hr/>";
		#Method calling
		echo "calloing from child";
		echo $this->display();
	}
}

$child=new child();

$child->display();
echo $child->x;