<?php
#save =vailidate.php

if($_SERVER['REQUEST_METHOD']=='POST'){
	if(isset($_POST['btn']) and !empty($_POST['btn'])){
		
		$mobile=isset($_POST['mobile'])?$_POST['mobile']:NULL;
		$fname=isset($_POST['fname'])?trim($_POST['fname']):NULL;
		$lname=isset($_POST['lname'])?trim($_POST['lname']):NULL;
		
		if(is_null($fname) or empty($fname)){
			//name is blank
			header("location:{$_REQUEST['/']}?action=fname&msg=invailid-fname");
		}else{
			//name is not blank
			if(!(preg_match("/^[a-zA-Z]+$/",$fname))){
				header("location:{$_REQUEST['/']}?action=fname&msg=invailid-fname");
			}else{
				echo "<br/>{$fname}";
			}
		}
		
		if(is_null($lname) or empty($lname)){
			//lname is blank
			header("location:{$_REQUEST['/']}?action=lname&msg=invailid-lname");
		}else{
			//lname is not blank
			if(!(preg_match("/^[a-zA-Z]+$/",$lname))){
				header("location:{$_REQUEST['/']}?action=lname&msg=invailid-lname");
			}else{
				echo "<br/>{$lname}";
			}
		}
		if(is_null($mobile) or empty($mobile)){
			//blank vailidation
			header("location:{$_REQUEST['/']}?action=mobile&msg=invailid-mobile");
		}else{
			//mobile is not blank
			if(!(preg_match("/^[6-9]{1}[0-9]{9}$/",$mobile))){
				header("location:{$_REQUEST['/']}?action=mobile&msg=invailid-mobile");
			}else{
				echo "<br/>$mobile";
			}
		}
		//form input
	}else{
		header("location:{$_REQUEST['/']}?action=mobile&msg=blank-btn");
	}
	
}else{
	header("location:{$_REQUEST['/']}?msg=auth-failed");
}

?>