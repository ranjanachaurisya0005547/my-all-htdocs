<?php
$name="ravi";
$class="12th";
$marks="45";
$status="pass";

echo "<pre>";
print_r(get_defined_vars());

echo "<pre>";
print_r(compact('name','class','marks','status'));
echo "</pre>";

#code for making own compact function
foreach(get_defined_vars() as $key => $value){
   
   if(!in_array($key,['_GET','_POST','_COOKIE','_FILES'])){
   	   $tmp[$key]=$value;
   }else{
   	continue;
   }

}
/*
get_defined_vars():it is inbuilt function of PHPwhich is used to returns an array of all defined variables this function returns the multi-dimensional array which contains all the list of variables
enviroment etc.
Syntax:
get_defined_vars();
Parameters:This function does not accepts any parameter.

Returs:This function returns a multi dimensional array of all the variables

<?php
$name="ravi";
$class="12th";
$marks="45";
$status="pass";

echo "<pre>";
print_r(get_defined_vars());

Output:
Array
(
    [_GET] => Array
        (
        )

    [_POST] => Array
        (
        )

    [_COOKIE] => Array
        (
        )

    [_FILES] => Array
        (
        )

    [name] => ravi
    [class] => 12th
    [marks] => 45
    [status] => pass
)
*/
//Print the array value
//in_array($key,['_GET','_POST','_COOKIE','_FILES'])):its check the $key value is 
echo "<pre>";
print_r($tmp);
echo "</pre>";

?>