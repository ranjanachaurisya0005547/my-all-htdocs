<?php

define('DB_HOST','localhost');
define('DB_USER','root');
define('DB_PASS','');
define('DB_NAME','slug_db');

try{
	$con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	// if($con){
	// echo "Connection created !";
	// }else{
	// 	throw new Exception;
	// }

    if(!$con){
    	throw new Exception;
    }

}catch(Exception $ex){
	echo "Connection Error".mysqli_error($con);
	echo $ex->getmessage();
	exit;
}
#After Query operation close the Connection
#finally{ mysqli_close($con); }
#connection close in footer.php