<?php

require_once __DIR__."/config/settings.php";
require_once __DIR__."/config/dbconnect.php";
require_once __DIR__."/helpers/debugger_helper.php";

$id=isset($_GET['id'])?$_GET['id']:"";
$select_query="select * from tbl_post where id='{$id}'";
$result_set=mysqli_query($con,$select_query);
$count=mysqli_num_rows($result_set);
if($count>0){
    if($rows=mysqli_fetch_assoc($result_set)){
    extract($rows);
   }

}

if($_SERVER['REQUEST_METHOD']=='POST'){
	if(isset($_POST['add-btn']) and !empty($_POST['add-btn'])){

  $title=isset($_POST['title'])?$_POST['title']:NULL;
  $content=isset($_POST['content'])?$_POST['content']:NULL;
  $sid=isset($_POST['sid'])?$_POST['sid']:NULL;
  	   
  $slug=implode("-",explode(" ",$title));
  $slug=strtolower($slug); 
 
  if($count>0){
    echo "Such Type of title is all ready exists !";
    exit;
  }else{

    date_default_timezone_set('Asia/Kolkata');

  $date=date('Y-m-d');
  $time=date('H:i:s');

  //Prepare the query and Fire The query
  $sql="update tbl_post set title='{$title}',content='{$content}',slug='{$slug}',date='{$date}',time='{$time}' where id='{$sid}'";


//Fire The Query 
  if(mysqli_query($con,$sql)){

  	$change_count=mysqli_affected_rows($con);
  	if($change_count>0){
        header("location:view-post.php?msg=101");
    }else{
    	echo "Data Not updated !";
    }

  }else{
      echo 'Update Error'.mysqli_error($con);
  }
  }

  

	}
}

?>

<h1>EDIT Post</h1>
<form action="<?php echo basename($_SERVER['PHP_SELF']); ?>" method="POST">
	<p><input type="hidden" name="sid" value="<?php echo $id; ?>"/></p>
	<p>Title:<br/><input type="text" name="title" value="<?php echo $title; ?>"/></p>
	<p>Content:<br/><textarea name="content"><?php echo $content; ?></textarea></p>
	<p><input type="submit" name="add-btn" value="ADD POST"></p>
</form>