<?php

$msg=@$_GET['msg'];
if($msg==11){
	echo "Data deleted successfully !";
}elseif($msg=='101'){
	echo "Record Updated SUCCESSFULLY !";
}

?>
<!-------------------------Load header and debugger function---------------------------->


<?php
 require_once __DIR__."/layout/header.php";
 require_once __DIR__."/helpers/debugger_helper.php";
 require_once __DIR__.'/config/settings.php';
?>
<!-----------------------Load Database Connection------------------------->
<?php
require_once __DIR__.'/config/dbconnect.php';
?>
<!---------------------------GET THE Data from the database----------------->
<?php

//Prepare the query and fire the query
$query="select * from tbl_post";
$rs=mysqli_query($con,$query);
$count=mysqli_num_rows($rs);
if($count>0):
?>
<table border="2" rules="all" width="100%">
	<tr style="font-size:19px;height:45px;background:black;color:white;font-weight:bold;text-align:center;font-family:formal;">
		<th>Sr No.</th>
		<th>Title</th>
		<th>Slug</th>
		<th>Content</th>
		<th>DELETE</th>
		<th>EDIT</th>
		<th>POST</th>
	</tr>
	<?php 
	     while( $row=mysqli_fetch_assoc($rs)):
	?>
	<tr style="text-align:center;height:40px;">
		<td><?php echo $row['id']; ?></td>
		<td><?php echo $row['title']; ?></td>
		<td><?php echo $row['slug']; ?></td>
		<td><?php echo $row['content']; ?></td>
		<td><a href="edit-post.php?id=<?php echo $row['id']; ?>">EDIT</a></td>
		<td><a href="javascript:delete_data('<?php echo $row["id"]; ?>');">DELETE</a></td>
		<td><a href="posts/<?php echo $row['slug']; ?>">POST</a></td>
	</tr>
	<?php
      endwhile;
	?>
</table>


<?php
else:
	echo "No Record Exists !";
endif;

?>
<script>
	function delete_data(id){
		cnf=window.confirm("Are You Sure ,want to Delete !");
		if(cnf===true){
			var base_url="<?php echo BASE_URL; ?>";
			window.location.href=base_url+"delete.php?id="+id;
		}
	}
</script>
<br/><br/>
<?php
require_once __DIR__.'/layout/footer.php';
?>