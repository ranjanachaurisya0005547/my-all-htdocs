<?php
echo date("y-m-d")."<br/>";
echo date('Y/M/D')."<br/>";
/*
d:date on digit ,D:Day
m:month on digit,M:Name of month
y:year in two digit,Y:Four digit year
*/
echo date_default_timezone_get()."<br/>";
//for getting timezone default of server ::default=Europe/Berline
date_default_timezone_set("Asia/Kolkata");

echo date('h:i:s:a')."<br/>";//for showing Antimeridian and post meridian
echo date('H-i-s')."<br/>";//24 hours
echo date_default_timezone_get()."<br/>";//for getting default timezone of server

/*
H:24hours
i:iterval
s:seconds
*/
/*
output:
21-01-19
2021/Jan/Tue
10:04:34:am
10-04-34
Europe/Berlin
*/
?>