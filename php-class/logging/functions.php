<?php 

function write_log($msg,$line=__LINE__){

    #set Timezone
	date_default_timezone_set("Asia/Kolkata");

	#date and Time
	$date=date("y-m-d");
	$time=date("h:i:m");

	$formdata="At Date {$date} and Time {$time}";

	//file handling
	$fp=fopen(__DIR__."/log/data.log","a");
	$code="";
	$code.="Logged ".$formdata;
	$code.=" ".$msg;
	$system_path=dirname($_SERVER['PHP_SELF']);
	$filename=basename($_SERVER['PHP_SELF']);

	$code.="In File {$system_path}/{$filename}";
	$code.="On Line ".$line."\n";


	fwrite($fp,$code);
	#close the file
	fclose($fp);



}