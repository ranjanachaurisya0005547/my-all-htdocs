<?php
    include "init.php";
	$name_error="";
	$mobile_error="";
	
if($_SERVER['REQUEST_METHOD']=='POST'){
	$name=isset($_POST['name'])?$_POST['name']:null;
	$mobile=isset($_POST['mobile'])?$_POST['mobile']:null;
	
	if(isset($_POST['btn']) and !empty($_POST['btn'])){
		//name vailidation
		if(is_null($name) or empty($name)){
			$name_error="* Name can not be empty";
		}else{
			echo $name."<br/>";
		}
	
	//mobile vailidation
	if(is_null($mobile) or empty($mobile)){
		$mobile_error="* Mobile Number can not be empty !";
	}else{
		echo $mobile;
	}
	//unset the button
	unset($_POST['btn']);
	
	//if any form control is empty then its break otherwise $i is incremented
	$i=0;
	foreach($_POST as $key => $value){
		
		if(empty($value)){
			break;
		}
		$i++;
	}
	
	//if $i is not equal to the zero then gloabal message is run
	if($i==count($_POST)){
		header("location:input.php?msg=registration-success");
	}
	
	}
}

?>	
	<html>
<head></head>
<body>
<h1>Form Handling</h1>
<?php show_errors(); ?>
<form action="<?php echo basename($_SERVER['PHP_SELF']); ?>" method="post">
<p>Enter the Name<br/><input type="text" class="input-box" name="name" value="<?php echo isset($name)?$name:""; ?>"/>
<?php echo $name_error; ?>
</p>
<p>Enter Mobile Number<br/><input type="text" class="input-box" name="mobile" value="<?php echo isset($mobile)?$mobile:""; ?>"/>
<?php echo $mobile_error; ?>
</p>
<input type="submit" name="btn"/>
</form>
</body>
</html>

