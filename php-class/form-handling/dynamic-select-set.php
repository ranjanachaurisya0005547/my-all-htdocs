<?php
#save dynamic-select-set.php
$city=$_POST['city'];
?>
<html>
<head></head>
<body>
   <h1>Form Handling with dynamic Select</h1>
   <form action="dynamic-select-set.php" method="post">
      <p>
	    Select City:<br/>
		<select name="city">
		  <!---static--->
		  <option value="<?php echo $city; ?>"><?php echo $city; ?></option>
		  <!----static->
		  <!---dynamic--->
		  <?php $city_arr=['Lucknow','Gorakhpur','kanpur','Jaipur'];?>
		  <?php foreach($city_arr as $index => $cityname){?>
		       
			   <?php if($city!=$cityname){?>
		        <option value="<?php echo $cityname; ?>">
				    <?php echo $cityname; ?>
				</option>
			   <?php }; ?>	
				
		  <?php }; ?>
		  <!---dynamic--->
		 
		</select>
	  </p>
   </form>
</body>
</html>