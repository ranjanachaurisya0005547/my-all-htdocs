<?php

?>
<html>
<head></head>
<body>
   <h1>Form Handling with dynamic Select</h1>
   <form action="dynamic-select-set.php" method="post">
      <p>
	    Select City:<br/>
		<select name="city">
		  <!---static--->
		  <option value="">Select</option>
		  <!----static->
		  <!---dynamic--->
		  <?php $city_arr=['Lucknow','Gorakhpur','kanpur','Jaipur'];?>
		  <?php foreach($city_arr as $index => $cityname){?>
		  
		        <option value="<?php echo $cityname; ?>">
				    <?php echo $cityname; ?>
				</option>
				
		  <?php }; ?>
		  <!---dynamic--->
		 
		</select>
	  </p>
	  <input type="submit" name="btn"/>
   </form>
</body>
</html>