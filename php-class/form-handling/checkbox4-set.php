<?php
#checkbox3-set.php
$name=$_POST['name'];
//echo $name;

$hobbies=$_POST['hobbies'];
$hobby=implode(',',$hobbies);
//+echo $hobby;

$hobby_arr=explode(',',$hobby);
/*explode():its  convert string to array .its opposite of implode function.
in_array:return true are false if particular value is present in array.
*/
?>
<html>
<head></head>
<body>
  <h1>Checkbox Handling In PHP Third page to set value page</h1>
  <form action="checkbox4-set.php" method="POST">
  <p>Name<input type="text" name="name"value="<?php echo $name;?>"></p>
   <fieldset>
      <legend>Hobbies</legend>
	  <?php $hobbies_arr=['singing','dancing','cooking','crying'];?>
      <?php foreach($hobbies_arr as $index => $value){?>
         <input type="checkbox" name="hobbies[]" value="<?php echo $value; ?>"
		 <?php in_array($value,$hobby_arr)?print('checked'):"";?>/>
		 <?php echo $value; ?>
     <?php }; ?>
   </fieldset>
     <input type="submit" name="btn"/>
  </form>
</body>
</html>