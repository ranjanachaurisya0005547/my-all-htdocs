<?php
#checkbox3-set.php
$name=$_POST['name'];
//echo $name;

$hobbies=$_POST['hobbies'];
$hobby=implode(',',$hobbies);
//+echo $hobby;

$hobby_arr=explode(',',$hobby);
/*explode():its  convert string to array .its opposite of implode function.
in_array:return true are false if particular value is present in array.
*/
?>
<html>
<head></head>
<body>
  <h1>Checkbox Handling In PHP Third page to set value page</h1>
  <form action="checkbox3-set.php" method="POST">
  <p>Name<input type="text" name="name"value="<?php echo $name;?>"></p>
   <fieldset>
      <legend>Hobbies</legend>
	  <input type="checkbox" name="hobbies[]" value="singing" 
	      <?php 
		  if(in_array('singing',$hobby_arr)){echo "checked";}
		?>
	  >Singing
	  <input type="checkbox" name="hobbies[]" value="dancing"
	      <?php 
		  if(in_array('dancing',$hobby_arr)){echo "checked";}
		?>
	  >Dancing
	  <input type="checkbox" name="hobbies[]" value="cooking"
	     <?php 
		  if(in_array('cooking',$hobby_arr)){echo "checked";}
		  #in_array('cooking',$hobby_arr)?print('checked'):"";
		?>
	  >Cooking
   </fieldset>
     <input type="submit" name="btn"/>
  </form>
</body>
</html>