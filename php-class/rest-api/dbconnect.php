<?php

class Database{
	
	private $host="127.0.0.1";
	private $user="root";
	private $pass="";
	private $dbname="rest_db";
	
	public function __construct(){
	try{
		$con=mysqli_connect($this->host,$this->user,$this->pass,$this->dbname);
		if(!$con){
			throw new Exception;
		}
	}catch(Exception $ex){
	     echo "Error ".mysqli_error($con);
		 echo $ex->getMessage();
	}
	}	
}