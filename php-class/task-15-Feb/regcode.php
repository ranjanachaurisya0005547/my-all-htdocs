<?php

include __DIR__."/config/functions.php";
session_start();
  $token=isset($_POST['_token'])?$_POST['_token']:NULL;
  $sess_token=$_SESSION['_token'];
if($token==$sess_token){
if($_SERVER['REQUEST_METHOD']=='POST'){
  if(isset($_POST['reg-btn']) and !empty($_POST['reg-btn'])){
  	$name=isset($_POST['name'])?$_POST['name']:NULL;
  	$mobile=isset($_POST['mobile'])?$_POST['mobile']:NULL;
  	$email=isset($_POST['email'])?$_POST['email']:NULL;
  	$pass=isset($_POST['password'])?$_POST['password']:NULL;
  	$cpass=isset($_POST['cpassword'])?$_POST['cpassword']:NULL;
  
  	if(is_null($name) or empty($name)){
       header("location:registration.php?msg=304");
       exit;
    }else{
      if(!(preg_match("/[a-zA-Z]+/",sanitise($name)))){
          header("location:registration.php?msg=305");
          exit;
      }
    }
      
    if(is_null($mobile) or empty($mobile)){
      header("location:registration.php?msg=306");
    }else{
         if(!(preg_match('/^[6-9]{1}[0-9]{9}$/',$mobile))){
            header("location:registration.php?msg=307");
            exit;
         }
    }    
  	
  }else{
      header("location:registration.php?msg=302");
  }
}else{
	header("location:registration.php?msg=301");
}
}else{
      header("location:registration.php?msg=303");
}