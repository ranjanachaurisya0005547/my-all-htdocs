<?php
include_once __DIR__."/csrf-preventation/csrf.php";
include_once __DIR__."/config/functions.php";
include_once __DIR__."/config/error.php";

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
    <h1><center>************Registration form With Text-captcha of PHP ************</center></h1>

    <form action="regcode.php" method="POST">
    	<input type="hidden" name="_token" value="<?php echo get_csrf_token(); ?>"/>
      <?php echo show_flash(); ?>
      <p>
      	Name<br/>
      	<input type="text" name="name"/>
        <br/><?php echo show_flash('name'); ?>
      </p>
      <p>
      	Mobile Number<br/>
      	<input type="text" name="mobile" />
        <br/><?php echo show_flash('mobile'); ?>
      </p>
      <p>
      	Enter Your Email<br/>
      	<input type="email" name="email"/>
      </p>
      <p>
      	Enter Your Password<br/>
      	<input type="password" name="password"/>
      </p>
      <p>
      	Enter The Confirm Password<br/>
      	<input type="password" name="cpass"/>
      </p>
      <input type="submit" name="reg-btn" value="Registration"/>
    </form>
</body>
</html>
