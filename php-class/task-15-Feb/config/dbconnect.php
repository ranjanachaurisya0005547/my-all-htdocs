<?php


$settings=include_once __DIR__.'/settings.php';


define('HOST',$settings['database']['host']);
define('USER_NAME',$settings['database']['user']);
define('PASSWORD',$settings['database']['password']);
define('DB_NAME',$settings['database']['name']);

try{
	$con=mysqli_connect(HOST,USER_NAME,PASSWORD,DB_NAME);

	if(!$con){
		throw new Exception("Connection Error",1);
	}
}catch(Exception $ex){

     echo "Error ".mysqli_error($con);
     echo $ex->getMessage();
     exit;

}