<?php

#use of list with associative array

$student = [
  'name'=>'ranjana',
  'class'=>'10th',
  'marks'=>'100',
];

list($a,$b,$c)=$student;
echo "a={$a},b={$b},c={$c}<br/>";


//Associative array with numeric key

$student = [
  0=>'ranjana',
  1=>'10th',
  2=>'100',
];

list($a,$b,$c)=$student;
echo "a={$a},b={$b},c={$c}<br/>";

#Associative array with mixed key

$student = [
  'name'=>'ranjana',
  1=>'10th',
  'marks'=>'100',
];

list($a,$b,$c)=$student;
echo "a={$a},b={$b},c={$c}<br/>";