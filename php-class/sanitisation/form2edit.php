<?php
if($_SERVER['REQUEST_METHOD']=='POST'){
	if(isset($_POST['btn']) and !empty($_POST['btn'])){
		$course_arr=['JQUERY','.NET','C++','Computer','C','PHP','JAVA','Android'];
        $name=isset($_POST['name'])?$_POST['name']:null;
        $mobile=isset($_POST['mobile'])?$_POST['mobile']:null;
        $gender=isset($_POST['gen'])?$_POST['gen']:null;
        $course=isset($_POST['course'])?$_POST['course']:null;
        $address=isset($_POST['address'])?$_POST['address']:null;
        
		
		if(is_null($name) or empty($name)){
			//name vailidation
			header("location:{$_REQUEST['/']}?action=name&msg=invailid-name");
		}elseif(is_null($mobile) or empty($mobile)){
			//mobile vailidation
			header("location:{$_REQUEST['/']}?action=mobile&msg=invailid-mobile");
		}elseif($course=='Select Course' or is_null($course) or empty($course)){
			//gender vailidation
			header("location:{$_REQUEST['/']}?action=gender&msg=invailid-gender");
		}
        
		
	}else{
		header("location:{$_REQUEST['/']}?msg=invailid-btn");
	}
}else{
	header("location:{$_REQUEST['/']}?msg=access-fail");
}

?>
<html>
<head></head>
<body>
 <form action="form2edit.php" method="post">
 <p>Enter Name:<br/> <input type="text" name="name" value="<?php echo $name; ?>"/> </p>
 <p> Enter Mobile Number<br/> <input type="text" name="mobile" value="<?php echo $mobile; ?>"/> </p>
 <p>Enter Gender<br/>
 Male<input type="radio" name="gen" value="male" <?php if($gender=='male'){echo "checked";}?>/> 
 Female<input type="radio" name="gen" value="female"<?php if($gender=='female'){echo "checked";}?>/>
 Other<input type="radio" name="gen" value="Other" <?php if($gender=='other'){echo "checked";}?>/>
 </p>
 <p>Select Course<br/>
    <select name="course">
	   <!-------static part------>
	   <option value="<?php echo $course1;?>"><?php echo $course;?></option>
	   <!--------Dynamic part---------------->
	   <?php foreach($course_arr as $key => $course1){?>
	      
         <?php if($course1!=course){?>		  
	      <option value="<?php echo $course1; ?>">
		      <?php echo $course1;?>
		  </option>
		 <?php }; ?> 
	   <?php }; ?>
	</select>
 </p>
 <p>Enter The Address<br/><textarea name="address" rows='6' cols='30'>
   <?php echo $address; ?>
 </textarea></p>
 <input type="submit" value="Show Preview"/>
 </form>
</body>
</html>