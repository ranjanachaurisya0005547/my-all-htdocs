<?php
$msg=[];
$ebranch=['Computer Science & Engg.','CIVIL Engg.','Information Technology','Machanical Engg.','Electrical Engg.','Electronics Engg.'];
if($_SERVER['REQUEST_METHOD']=='POST')
{
	if(isset($_POST['btn']) or !empty($_POST['btn']))
	{
		$fname=isset($_POST['fname'])?$_POST['fname']:NULL;
		$lname=isset($_POST['lname'])?$_POST['lname']:NULL;
		$email=isset($_POST['email'])?$_POST['email']:NULL;
		$mobile=isset($_POST['mobile'])?$_POST['mobile']:NULL;
		$branch=isset($_POST['branch'])?$_POST['branch']:NULL;
		$i=0;
		echo "<script>
		function remove(arg){document.getElementById(arg).style='display:none';}
		</script>";
		if(is_null($fname) or empty($fname))
		{
			$i++;
			$msg[0]="<span style='color:red;' id='fname'>*First Name should be required</span>
			<script>setTimeout('remove(\'fname\')',4000);</script>";
		}
			if(!preg_match("/^[A-Za-z ]+$/",trim($fname)) and !empty($fname))
			{
				$i++;
				$msg[6]="<span style='color:red;' id='fname1'>*Invalid First Name</span>
				<script>setTimeout('remove(\'fname1\')',4000);</script>";
			}
		if(is_null($lname) or empty($lname))
		{
			$i++;
			$msg[1]="<span style='color:red;' id='lname'>*Last Name should be required</span>
			<script>setTimeout('remove(\'lname\')',4000);</script>";
		}
			if(!preg_match("/^[A-Za-z ]+$/",trim($lname)) and !empty($lname))
			{
				$i++;
				$msg[7]="<span style='color:red;' id='lname1'>*Invalid Last Name</span>
				<script>setTimeout('remove(\'lname1\')',4000);</script>";
			}
		if(is_null($email) or empty($email))
		{
			$i++;
			$msg[2]="<span style='color:red;' id='email'>*Email should be required</span>
			<script>setTimeout('remove(\'email\')',4000);</script>";
		}
			if(!preg_match("/^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+\.[A-Za-z]+$/",trim($email)) and !empty($email))
			{
				$i++;
				$msg[5]="<span style='color:red;' id='email1'>*Invalid Email</span>
				<script>setTimeout('remove(\'email1\')',4000);</script>";
			}
		if(is_null($mobile) or empty($mobile))
		{
			$i++;
			$msg[3]="<span style='color:red;' id='mobile'>*Mobile should be required</span>
			<script>setTimeout('remove(\'mobile\')',4000);</script>";
		}
		   if(!preg_match("/^[6-9]{1}[0-9]{9}$/",trim($mobile)) and !empty($mobile))
			{
				$i++;
				$msg[4]="<span style='color:red;' id='mobile1'>*Invalid Mobile</span>
				<script>setTimeout('remove(\'mobile1\')',4000);</script>";
			}
		if(is_null($branch) or empty($branch))
		{
			$i++;
			$msg[8]="<span style='color:red;' id='branch'>*Please select any branch</span>
			<script>setTimeout('remove(\'branch\')',4000);</script>";
		}
		
		
		#echo count($msg);
		if(count($msg)==0)
			{
				echo "<script>alert('Registration Successfully....!');</script>";
				foreach($_POST as $emptykey => $emptyvalue)
				{
					$$emptykey='';
				}	
			}
	}	
}else{
	$url=isset($_REQUEST['/'])?$_REQUEST['/']:'';
	header("location:{$url}");
}
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h2>Set & Get Value Form</h2>
	<form action="editform1.php?/=<?php echo basename($_SERVER['PHP_SELF']); ?>" method="post">
		FIRST NAME : <input type="text" name="fname" value="<?php echo isset($fname)?$fname:''; ?>">
		       <?php echo isset($msg[0])?$msg[0]:'';
		             echo isset($msg[6])?$msg[6]:'';  
		       ?><br><br>
		LAST NAME : <input type="text" name="lname" value="<?php echo isset($lname)?$lname:''; ?>">
		       <?php echo isset($msg[1])?$msg[1]:'';
		             echo isset($msg[7])?$msg[7]:''; 
		       ?><br><br>
		
		E-MAIL : <input type="text" name="email" value="<?php echo isset($email)?$email:''; ?>">
		       <?php echo isset($msg[2])?$msg[2]:'';
 			         echo isset($msg[5])?$msg[5]:'';
		       ?><br><br>
		MOBILE : <input type="text" name="mobile" value="<?php echo isset($mobile)?$mobile:''; ?>">
		       <?php echo isset($msg[3])?$msg[3]:'';
			         echo isset($msg[4])?$msg[4]:'';
			   ?><br><br>
		BRANCH : <select name="branch" >
					    <option value=""> ----Select----</option>
					    <?php

					    	foreach($ebranch as $key => $value) {
					    		if($value!=$branch)
					    		   echo '<option value="'.$value.'">'.$value.'</option>';
					    		else{
					    			echo '<option value="'.$value.'" selected>'.$value.'</option>';
					    		}
					    	}
					     ?>
		         </select><?php echo isset($msg[8])?$msg[8]:''; ?>
		         <br><br>
		<input type="submit" name="btn">
	</form>
</body>
</html>