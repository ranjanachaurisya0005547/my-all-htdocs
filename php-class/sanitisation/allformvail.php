<?php
$fname_error="";
$lname_error="";
$mobile_error="";
$address_error="";
$mobile=isset($_POST['mobile'])?trim($_POST['mobile']):null;

if($_SERVER['REQUEST_METHOD']=='POST'){
	if(isset($_POST['btn']) and !empty($_POST['btn'])){
		$fname=isset($_POST['fname'])?trim($_POST['fname']):null;
		$lname=isset($_POST['lname'])?trim($_POST['lname']):null;
		$address=isset($_POST['address'])?$_POST['address']:null;
		
		//fname vailidation
		if(is_null($fname) and empty($fname)){
			$fname_error="* Name is can not be empty !";
		}else{
			if(!(preg_match("/^[a-zA-Z]+$/",$fname))){
				$fname_error="* Name is can not be empty !";
			}
		}
		//lname vailidation
		if(is_null($lname) and empty($lname)){
			$lname_error="* Name can not be empty !";
		}else{
			if(!(preg_match("/^[a-zA-Z]+$/",$lname))){
				$lname_error="* Name can not be empty !";
			}
		}
	   //mobile vailidation
	   if(is_null($mobile) and empty($mobile)){
			$mobile_error="* Name is can not be empty !";
		}else{
			if(!(preg_match("/^[6-9]{1}[0-9]{9}$/",$mobile))){
				$mobile_error="* Mobile Number can not be empty !";
			}
		}
		//adrress vailidation
		if($address=="" and $address==null){
			$address_error="* Address can not be empty !";
		}
		//
	//unset the btn
	unset($_POST['btn']);
	
	$i=0;
	foreach($_POST as $key => $value){
		
		if(empty($value)){
			break;
		}
		$i++;
	}
		if($i==count($_POST) and empty($fname_error) and empty($lname_error)){
		foreach($_POST as $k => $value){
		    $$k="";	
		}
		echo "Form Submitted";
	}
		
	}
}
?>
<html>
<head>
<style>
.error{color:red;font-size:20px;}
</style>
</head>
<body>
<h1>Form vailidation...</h1>
<form action="<?php echo basename($_SERVER["PHP_SELF"]); ?>" method="POST">
<p>Enter Your First Name:<br/>
   <input type="text" name="fname" value="<?php echo isset($fname)?$fname:"";?>"/>
   <span class="error"><?php echo $fname_error; ?></span>
</p>
<p>Enter Last Name:<br/>
<input type="text" name="lname" value="<?php echo isset($lname)?$lname:"";?>"/>
<span class="error"><?php echo $lname_error; ?></span>
</p>
<p>Enter Your Mobile<br/>
<input type="text" name="mobile" value="<?php echo isset($mobile)?$mobile:""; ?>"/>
<span class="error"><?php echo $mobile_error; ?></span>
</p>
<p>Select Your Gender:
<input type="radio" name="gen" value="male"/>Male
<input type="radio" name="gen" value="female"/>Female
</p>
<p>Select Your Course<br/>
<select name="course">
<!--------------static---------->
<option value="">Select Course</option>
<!--------------Dynamic----------------->
<?php $course=['PHP','JAVA','JQUERY','SQL'];?>
<?php foreach($course as $key => $value){?>
<option value="<?php echo $value;?>"><?php echo $value; ?></option>
<?php }; ?>
</select>
</p>
<p>Address<br/>
<textarea name="address"><?php echo isset($address)?$address:"";?></textarea>
<span class="error"><?php echo $address_error;?></span>
</p>
<input type="submit" name="btn"/>
</form>
</body>
</html>