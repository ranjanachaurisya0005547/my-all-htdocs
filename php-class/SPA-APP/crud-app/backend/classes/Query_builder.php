<?php

require_once __DIR__.'/Database.php';

class Query_builder extends Database{

  private $conn;
  private $sql;
  private $data;

  public function __construct(){
    parent::__construct();
  	 $this->conn=$this->con;
  }

   //Insert query
   public function insert($tablename,$form_data){

     $column_keys = implode(',',array_keys($form_data));
     $values = implode("','",array_values($form_data));
     
     //print_r($column_keys);
     //print_r($values);

     $sql = "INSERT INTO {$tablename}({$column_keys}) values('{$values}')";
     $this->sql=$sql;
   }

   //For see the last query
   public function last_query(){
      return $this->sql;
   }

   //Fire the Query
   public function run(){
     if(!is_null($this->sql)){
      
          $result=mysqli_query($this->conn,$this->sql);
          if($result==true){

            if(mysqli_affected_rows($this->conn)>0){
                  return true;
            }else{
                  return false;
            }

          }else{
             die("Data is Not inserted Successfuly !");
          }

     }else{
         die("Query is Null !");
     }
   }

   //Get the table all columns
   public function getcolumns($tablename){
        $sql="DESC {$tablename}";
        $this->sql=$sql;

        $result_set=mysqli_query($this->conn,$this->sql) or die("Query Fired Error ".mysqli_connect_error($this->conn));
        $count=mysqli_num_rows($result_set);

        if($count>0){
            while($row=mysqli_fetch_assoc($result_set)){
                $field[]=$row['Field'];
            }
            return $field;
             
        }else{
             return [];
        }
   }

   //Last inserted id
   public function last_id($tablename){
      $id_column=$this->getcolumns($tablename)[0];
      $sql="SELECT {$id_column} FROM {$tablename} ";
      $sql.="ORDER BY {$id_column} DESC LIMIT 1";
      $this->sql=$sql;
      return current(array_values($this->fetch_records()->first_row()));
   }


   //Function for firering the select query

   public function fetch_records($sql=NULL){
        if(!is_null($sql)){
            $this->sql=$sql;
        }
          $result_set=mysqli_query($this->conn,$this->sql) or die("Query Error ".mysqli_connect_error($this->conn));

         $count=mysqli_num_rows($result_set);

         if($count>0){
            while($row=mysqli_fetch_assoc($result_set)){
                  $data[]=$row;
            }
            $this->data=$data;
            return $this;

         }else{
            $this->data=[];
            return $this;
         }
        
   }

   //For Get The first Row
   public function first_row(){
      return current($this->data);
   }

   //For Get The last Row
    public function last_row(){
      return end($this->data);
   }

   //For Get The next Row
   public function next_row(){
      return next($this->data);
   }

   //To get All the rows

   public function all(){
        return $this->data;
   }

   //

}









