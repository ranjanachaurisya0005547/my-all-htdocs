<?php

class Database{

     private $host="localhost";
     private $user="root";
     private $pass="";

     private $dbname="spa_crud";

     #child class will be access
     protected $con;

     public function __construct(){
          try{
            $con=mysqli_connect($this->host,$this->user,$this->pass,$this->dbname);

            if(!$con){
               throw new Exception;
            }else{
            $this->con=$con;
            }

          }catch(Exception $ex){
              echo "Connection error ".mysqli_connect_error($con);
              echo $ex->getMessage();
              exit;
          }

     }


}
