//SPA JS

var base_url="http://localhost:8000/php-class/SPA-APP/app1/";

$(document).ready(function(){
    
});

function $hash(){
   return window.location.hash;
}

function load_view(pagename){
   
   switch(pagename){

   	case 'home':
   	  var response=home();
   	  sethash(pagename);
   	  return view(response);
   	  break;

   	case 'about':
   	var response=about();
   	sethash(pagename);
   	return view(response);
   	break;

   	case 'register':
   	var response=register();
   	sethash(pagename);
   	return view(response);
   	break;

   	case 'login':
   	var response=login();
   	sethash(pagename);
   	return view(response);
   	break;

   	default:
   	var response=home();
   	sethash(pagename);
   	return view(response);
   	break;
   }
}

function home(){
	return "<h1>This is Home View</h1>";
}

function about(){
	return "<h1>This is About us page</h1>";
}

function register(){
	return "<h1>This is Registration page</h1>";
}

function login(){
	return "<h1>This is login page</h1>";
}


function view(response){
	$("#outer").html(response);
}

function sethash(pagename){
	window.location.href=base_url+"#"+pagename;
}



