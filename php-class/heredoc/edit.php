<?php

$name="Ranjana";

#Heredoc default is processed string 
echo <<<FORM
      <form action="" method="GET">
	       <input type="text" value="{$name}"/>
		   <input type="submit">
	  </form>
FORM;

#Heredoc
echo <<<"FORM"
      <form action="" method="POST">
	      <input type="text" name="name" value="{$name}"/>
		  <input type="submit" value="SUBMIT"/>
	  </form>
FORM;



#Heredoc  unproceseed string Its not print the value of the php variables.
echo <<<'FORM'
      <input type="text" value="{$name}"/>
	  <input type="submit" />
FORM;



?>