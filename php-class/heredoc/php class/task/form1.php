<?php
$course=['JQUERY','.NET','C++','Computer','C','PHP','JAVA','Android'];
#include "init.php";
include 'errors.php';
include 'functions.php';
$name_error = "";
$mobile_error = "";
if($_SERVER['REQUEST_METHOD']=='POST'){
   if(isset($_POST['btn']) and !empty($_POST['btn'])){
	$name=isset($_POST['name'])?$_POST['name']:NULL;
	$mobile=isset($_POST['mobile'])?$_POST['mobile']:NULL;
	#echo $name;
	//Name
	if(is_null($name) or empty($name)){
     $name_error= "Name can not be empty";
	}else{
		if(!(preg_match("/^[a-zA-Z]+$/",$name))){
		header("location:{$_REQUEST['/']}?action=name&msg=invalid-name");
	}
	}
	else{
			echo $name;
		}
	//Mobile
	if(is_null($mobile) or empty($mobile)){
     $mobile_error= "Mobile can not be empty";
	}else{
		//Mobile is not blank-mobile
		if(!(preg_match("/^[6-9]{1}[0-9]{9}$/",$mobile))){
		header("location:{$_REQUEST['/']}?action=mobile&msg=invalid-mobile");
	}
	else{
			echo "<br/>{$mobile}";
		}
		
	
	unset($_POST['btn']);
	//Best Way
	$i=0;
	foreach($_POST as $key => $value){
		if(empty($value)){
			break;
		}
		$i++;
		if($i==count($_POST)){
			echo "Form is Submitted";
			header("location:input.php?msg=register-success");
		}
	}
}
}
   }
?>
<html>
<head>
<style>
.error{
	color:red;
}
</style>
</head>
<body>
<h2><?php show_errors();?></h2>
 <form action="form1.php?/=<?php echo basename($_SERVER['PHP_SELF']);?>" method="POST">
 Name:<input type="text" name="name" value="<?php echo isset($name)?$name:"";?>">
<span class="error"><?php echo $name_error;?></span><br/><br/>
Mobile:<input type="text" name="mobile" value="<?php echo isset($mobile)?$mobile:"";?>">
<span class="error"><?php echo $mobile_error;?></span><br/><br/>
 <p>Enter Gender<br/>
 Female<input type="radio" name="gen" value="female"/> 
 Male<input type="radio" name="gen" value="male"/>
 Other<input type="radio" name="gen" value="Other"/>
 </p>
 <p>Select Course<br/>
    <select name="course">
	   <!-------static part------>
	   <option value="">Select Course</option>
	   <!--------Dynamic part---------------->
	   <?php foreach($course as $key => $course){?>
	      <option value="<?php echo $course; ?>">
		      <?php echo $course;?>
		  </option>
		  
	   <?php }; ?>
	</select>
 </p>
 <p>Enter The Address<br/><textarea name="address" rows='6' cols='30'></textarea></p>
 <input type="submit" value="submit" name="btn"/>
 </form>
</body>
</html>