<?php

#validate.php
if($_SERVER['REQUEST_METHOD']=='POST'){
	if(isset($_POST['btn']) and !empty($_POST['btn'])){
	$fname = isset($_POST['fname'])?$_POST['fname']:NULL;
	$lname = isset($_POST['lname'])?$_POST['lname']:NULL;
	$mobile = isset($_POST['mobile'])?$_POST['mobile']:NULL;
	
	if(is_null($fname) or empty($fname)){
		header("location:{$_REQUEST['/']}?action=fname&msg=blank-fname");

	}else{
		//if(!(preg_match("/^[a-zA-Z]{8}[ ]{1}[a-zA-Z]{6}$/",$name))){
		if(!(preg_match("/^[a-zA-Z]+$/",$fname))){
		header("location:{$_REQUEST['/']}?action=fname&msg=invalid-fname");

		}else{
			//$res = trim($fname);
			echo $fname;
		}
	}
	
	if(is_null($lname) or empty($lname)){
		header("location:{$_REQUEST['/']}?action=lname&msg=blank-lname");

	}else{
		//if(!(preg_match("/^[a-zA-Z]{8}[ ]{1}[a-zA-Z]{6}$/",$name))){
		if(!(preg_match("/^[a-zA-Z]+$/",$lname))){
		header("location:{$_REQUEST['/']}?action=lname&msg=invalid-lname");

		}else{
			//$res = trim($fname);
			echo $lname;
		}
	}
	
	
    if(is_null($mobile) or empty($mobile)){
		//Blank validation
		header("location:{$_REQUEST['/']}?action=mobile&msg=blank-mobile");
	}else{
		//Mobile is not blank-mobile
		if(!(preg_match("/^[6-9]{1}[0-9]{9}$/",$mobile))){
		header("location:{$_REQUEST['/']}?action=mobile&msg=invalid-mobile");
		
		}else{
			echo "<br/>{$mobile}";
		}
	}	
	}else{
		header("location:{$_REQUEST['/']}");
	}
	
}else{
	header("location:{$_REQUEST['/']}");
}
	