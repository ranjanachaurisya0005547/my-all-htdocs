<?php
$json_str=file_get_contents("student.json");

$json_arr=json_decode($json_str);
var_dump($json_arr);

//classical way
//student_1=$json_arr[0];
//echo "student Information 1:<hr/>";
//echo "student_name :{$student_1->name}<br/>";
//echo "student class :{$student_1->class}<br/>";
//echo "student passed :{$student_1->passed}<br/>";
//echo "student marks :{$student_1->marks}<br/>"

//Developers approach 

//how to vailidate Json_string http://jsonviewer.stack.hu/ paste the code and try to parse 
//if any error it will tell.
//In Json Viewer dots(...) means interateable.

foreach($json_arr as $student):
echo "<hr/>";
echo "<b>Student Data:</b>";
echo "<hr/>";
echo "Student Name :{$student->name}<br/>";
echo "Student class :{$student->class}<br/>";
echo "Student passed :{$student->passed}<br/>";
echo "Student marks :{$student->marks}<br/>";
endforeach;
?>