<?php
include "functions.php";
include "errors.php";
if($_SERVER['REQUEST_METHOD']=='POST'){
	if(isset($_POST['btn']) and !empty($_POST['btn'])){
		$email=isset($_POST['email'])?$_POST['email']:null;
		$password=isset($_POST['password'])?$_POST['password']:null;
		$email=sanitise($email);
		$password=sanitise($password);
		$dbemail="student@gmail.com";
		$dbpass="student";
		
		//email empty vaildation	
		if(is_null($email) or empty($email)){
			header("location:{$_REQUEST['/']}?action=email&msg=303");
			exit;
		}
		
		//password empty vailidation
		if(is_null($password) or empty($password)){
			header("location:{$_REQUEST['/']}?action=password&msg=304");
			exit;
		}
		
		//when email and password not empty vailidation
		if(!empty($email) and !empty($password)){
			//check database email and password
			if($email==$dbemail and $password==$dbpass){
				
				//session created
				session_start();//timestamp for user_error
				
				//Initialize the session database
				$_SESSION['student']=array(
				  'name'=>'studentAdmin',
				  'email'=>$email,
				  'password'=>$password
				);
				
				header("location:dashboard.php?login_status=login-success");
			}else{
				header("location:{$_REQUEST['/']}?msg=305");
			}
		}
		
	}else{
		header("location:{$_REQUEST['/']}?msg=302");
	}
}else{
	header("location:{$_REQUEST['/']}?msg=301");
}

?>