<?php
//starting the session
session_start();

//access the session 
$session=$_SESSION['student'];

//secure the session
if(!isset($session)){
	if(is_null($session) or empty($session)){
		header("location:login.php?msg=307");
	}
}

//how to get the session
$email = $session['email'];
?>
<h1>Welcome to the dashboard !</h1>
<a href="logout.php">Logout</a>