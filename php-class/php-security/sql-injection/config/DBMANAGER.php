<?php

$settings=include_once __DIR__.'/settings.php';

define('USER_NAME',$settings['database']['user_name']);
define('PASSWORD',$settings['database']['password']);
define('DB_NAME',$settings['database']['name']);
define('HOST',$settings['database']['host']);

try{
    $con=mysqli_connect(HOST,USER_NAME,PASSWORD,DB_NAME);
   
    if(!$con){
    	throw new Exception;
    }

}catch(Exception $ex){
	echo "Error".mysqli_error($con);
	echo $ex->getMessage();
	exit;
}