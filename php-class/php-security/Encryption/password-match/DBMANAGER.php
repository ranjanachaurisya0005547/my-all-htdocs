<?php

$settings=include_once 'settings.php';

//print_r($settings);
//print_r($settings['database']);
//exit;

define('DB_HOST',$settings['database']['host'].":".$settings['database']['port']);
define('USER_NAME',$settings['database']['user']);
define('PASSWORD',$settings['database']['pass']);
define('DB_NAME',$settings['database']['name']);

try{

  $con=mysqli_connect(DB_HOST,USER_NAME,PASSWORD,DB_NAME);
  if(!$con){
  	throw new Exception("Connection Exception",1);
  }

}catch(Exception $ex){
	echo "Error".mysqli_error($con);
	echo $ex->getMessage();
	exit;
}