<?php

#without salt
$pass="1234";
$enc_pass=crypt($pass);

echo "Encrypted Password = {$enc_pass}";
echo "<br/>";                                        
echo "Length of Encrypted Password =".strlen($enc_pass);
echo "<hr/>";

//with salt
$pass="1234";
#$enc_pass=crypt($pass,"1234");
$enc_pass=crypt($pass,"ranjana");

echo "Encrypted Password = {$enc_pass}";
echo "<br/>";
echo "Length of Encrypted Password =".strlen($enc_pass);