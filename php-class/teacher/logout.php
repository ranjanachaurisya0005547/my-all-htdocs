<?php
//session start
session_start();
$session=$_SESSION['teacher'];

//logout logic
if(isset($session)){
	if(!is_null($session) and !empty($session)){
		
		//1.unset the session variable
		//2.destroy the session
		session_unset($_SESSION['teacher']);//unset the session key
		session_destroy();//destroy the timestap of the session
		header("location:login.php?msg=306");//redirect the page
	}
}
?>