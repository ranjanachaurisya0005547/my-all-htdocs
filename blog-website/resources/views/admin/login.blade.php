<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{@config('constants.site_name')}}</title>

    <!-- Bootstrap -->
    <link href="{{asset('admin_assets/css/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('admin_assets/css/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('admin_assets/css/vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{asset('admin_assets/css/vendors/animate.css/animate.min.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{asset('admin_assets/css/build/css/custom.min.css')}}" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form action="{{route('login')}}" method="POST">
              <h1>Login Form</h1>

              @if(session('success'))
                   <div class="alert alert-success">{{session('success')}}</div>
              @elseif(session('failed'))
                   <div class="alert alert-danger">{{session('failed')}}</div>
              @endif

              @csrf
              <div>
                <input type="email" class="form-control" name="email" placeholder="Username" required="" />
              </div>
              <div>
                <input type="password" name="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <div class="row">
                   <input type="submit" class="form-control mr-5 btn btn-primary" value="Log In"/>
                </div>   
              </div>

              

                <div>
                  <h1><i class="fa fa-paw"></i> {{@config('constants.site_name')}} !</h1>
                </div>
              </div>
            </form>
          </section>
        </div>

      </div>
    </div>
  </body>
</html>
