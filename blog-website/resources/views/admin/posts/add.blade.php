@extends('admin.layout.layout')

@section('page_title','Add Posts')

@section('container')
<div class="page-title">
      <div class="title_left ml-2">
        <h4 class="ml-2 mt-2">Add Posts</h4>
      </div>
</div>

@if(session('success'))
   <span class="success-color">{{session('success')}}</span>
@elseif(session('failed'))
   <span class="error-color">{{session('failed')}}</span>

@endif

<div class="col-md-12 col-sm-12"><div class="x_panel">
<div class="x_content">
	<br />
	<form action="{{route('post.send')}}" method="POST" id="demo-form2" enctype="multipart/form-data">

    @csrf
		<div class="item form-group">
			<label class="col-form-label col-md-3 col-sm-3 label-align" for="title"> 
			   Title <span class="required">*</span>
			</label>
			<div class="col-md-6 col-sm-6 ">
				<input type="text" id="title" name="title" class="form-control ">
				@error('title')
           <span class="error-color">{{$message}}</span>
				@enderror
			</div>
		</div>
		<div class="item form-group">
			<label class="col-form-label col-md-3 col-sm-3 label-align" for="short_desc">    Short Description <span class="required">*</span>
		   </label>
			<div class="col-md-6 col-sm-6 ">
			   <textarea name="short_desc" id="short_desc" class="form-control"></textarea>
			   @error('short_desc')
                  <span class="error-color">{{$message}}</span>
				@enderror
			</div>
		</div>
		<div class="item form-group">
			<label class="col-form-label col-md-3 col-sm-3 label-align" for="long_desc">    Long Description <span class="required">*</span>
		   </label>
			<div class="col-md-6 col-sm-6 ">
			   <textarea name="long_desc" id="long_desc" class="form-control"></textarea>
			   @error('long_desc')
                   <span class="error-color">{{$message}}</span>
				@enderror
			</div>
		</div>
		<div class="item form-group">
			<label class="col-form-label col-md-3 col-sm-3 label-align" for="file"> 
			   Image <span class="required">*</span>
			</label>
			<div class="col-md-6 col-sm-6 ">
				<input type="file" id="file" name="image" class="form-control ">
				@error('image')
                   <span class="error-color">{{$message}}</span>
 				@enderror
			</div>
		</div>
		<div class="item form-group">
			<label class="col-form-label col-md-3 col-sm-3 label-align" for="post_date"> 
			   Post Date <span class="required">*</span>
			</label>
			<div class="col-md-6 col-sm-6 ">
				<input type="date" id="post_date" name="blog_date" class="form-control ">
				@error('blog_date')
                   <span class="error-color">{{$message}}</span>
				@enderror
			</div>
		</div>
		<div class="item form-group">
			<div class="col-md-6 col-sm-6 offset-md-3">
				<input type="submit" class="btn btn-primary" name="submit-btn" value="Submit"/>
			</div>
		</div>
	</form>
</div>
</div>
</div>
					

@endsection
