<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use Validator;

class PostController extends Controller
{
   
   //create blog
    public function storePosts(Request $request){

      $rules=[
             'title'=>'required|regex:/^[A-Za-z ]+$/',
             'short_desc'=>'required|regex:/^[A-Za-z ]+$/',
             'long_desc'=>'required|regex:/^[A-Za-z ]+$/',
             'image'=>'required|mimes:jpeg,jpg,png',
             'blog_date'=>'required',
       ];    
      $validator=Validator::make($request->all(),$rules);

      if($validator->fails()){
          return redirect(route('post.add'))->withErrors($validator)->withInput();
      }else{
          try{
                   $data=$request->input();
                   $file_obj=$request->file('image');
                   $file_name=$file_obj->getClientOriginalName();
                   $file_obj->move(public_path('blog_image'),$file_name);

                   $post=Post::create([
                      'title'=>$data['title'],
                      'short_desc'=>$data['short_desc'],
                      'long_desc'=>$data['long_desc'],
                      'image'=>$file_name,
                      'blog_date'=>$data['blog_date'],
                   ]);
                   $status=$post->save();

                   if($status){
                        return redirect(route('post.add'))->with('success','Post Added Successfully !');
                   }else{
                        return redirect(route('post.add'))->with('failed','Post Not Added !');
                   }
          }catch(Exception $ex){
              return redirect(route('post.add'))->with('failed',$ex->getMessage());
          }
    }
  }  
    

    //Show Blog Posts
    public function showPosts(){
        try{

             $post=Post::all();

             if($post[0]->status==1){
                   return view('admin.posts.show',['post'=>$post]);
             }

        }catch(Exception $ex){
            return redirect(route('post.add'))->with('failed',$ex->getMessage());
        }
      }


    //post edit view
      public function editView($id){
           $allPost=Post::find($id);
           return view('admin.posts.postedit',['allPost'=>$allPost]);
      }

      
      //Post Update
      public function updatePost(Request $request){
           $post=$request->input();
           dd($post);

      }

      //delete post
      public function destroyPost($id){
           
          try{
                 $post=Post::find($id);
                 $status=0;
                 $newData=Post::where('id',$id)->update([
                  'status'=>$status,
                 ]);

          }catch(\Exception $ex){
               return redirect(route('post.add'))->with('failed',$ex->getMessage());
          }
                 
      }

}
