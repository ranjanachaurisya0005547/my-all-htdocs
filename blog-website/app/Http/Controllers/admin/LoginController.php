<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use DB;

class LoginController extends Controller
{

    public function login(Request $request){
        $email=$request->input('email');
        $password=$request->input('password');

        $data=User::where('email',$email)->where('password',$password)->get();

        if(isset($data[0]->id)){
            if($data[0]->status==1){
               session()->put('BLOG_USER_ID',$data[0]->email);
               return redirect(route('post.show'));
            }else{
                return redirect(route('login.view'))->with('failed','Account Deactivated !');
            }
        }else{
            return redirect(route('login'))->with('failed','User Not Available !');
        }
    }

    //logout 
    public function logout(Request $request){
          $request->session()->forget('BLOG_USER_ID');
          $request->session()->flush();
          return redirect('/')->with('success','Logout Successfully !');
    }
    
}
