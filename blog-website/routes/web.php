<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\admin\LoginController;
use App\Http\Controllers\admin\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){
    return view('admin.login');
});
Route::post('/login_submit',[LoginController::class,'login'])->name('login');

Route::prefix('admin/post')->group(function(){
    Route::middleware(['admin_auth'])->group(function(){
         Route::get('/show',[PostController::class,'showPosts'])->name('post.show');
         Route::view('/view','admin.posts.add')->name('post.add');
         Route::post('/add',[PostController::class,'storePosts'])->name('post.send');
         Route::get('/edit/{id}',[PostController::class,'editView'])->name('post.edit'); 
         Route::post('/update',[PostController::class,'updatePost'])->name('post.update');
         Route::get('/delete/{id}',[PostController::class,'destroyPost'])->name('post.delete');
         Route::get('/logout',[LoginController::class,'logout'])->name('logout'); 
    });
});
