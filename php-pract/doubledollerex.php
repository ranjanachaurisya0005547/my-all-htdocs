<?php

echo "<h1>Double doller($$) Example </h1>";

$var = 1234;
$$var="ranjana";

echo "\$var variable value =".$var."<br/> \$\$var varible value = ".$$var;

echo "<h1>Another Double doller($$) Example </h1>";

$var = 1234;
${$var} = "ranjana";

echo "\$var variable value =".$var."<br/> \$\$var varible value = ".$$var;

echo "<h1>Another Double doller($$) Example </h1>";

$name = "Cat";
${$name} = "Monkey";
${${$name}} = "Dog";

echo "\$name variable value =".$name."<br/> \$\$name varible value = ".${$name}."<br/> \$\$\$name value = ".${${$name}};

echo "<h1>Another Double doller($$) Example </h1>";

$name = "Cat";
${$name} = "Monkey";
${${$name}} = "Dog";

echo "\$name variable value =".$name."<br/> \${\$name} varible value = ".${$name}."<br/> \${\${\$name}} value = ".${${$name}};
