<?php

echo "<h1>__FUNCTION__ Magic Constant Example </h1>";

echo "__FUNCTION__ Magic constant is used find out the function name .its return the function name where it is used .";
echo "<br/>Example:<br/>";

class Test{

	public function show(){

		echo "<br/>This is ".__FUNCTION__." method <br/>";
	}

   public function __construct(){

      echo "<br/>This is ".__FUNCTION__." method";

   }
   
}

$obj = new Test();
$obj->show();