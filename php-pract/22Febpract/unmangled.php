<?php

$a=20;

echo "value of the script local varibale outside the function a={$a}<br/>";
function display(){
	$b=100;
	echo "Value of the local variable b={$b}<br/>";
	echo "value of script local variable a={$a}<br/>";
	//how to access the script local variable inside the function display
	//for acces the script local variable inside the function display first we need to make it global 
	//for make a script local variable to global we can use the global keyword before the variable name inside //the the function name
	global $a;
	echo "value of the script local variable inside the function display after making it global a={$a}<br/>"; 
}

display();

function show(){
	global $a;
	echo "value of the script local varible a={$a}";
	//always we need to make the variable global where we use the script local variable 
}

show();