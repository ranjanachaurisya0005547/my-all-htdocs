<?php

//Syntax
//define('Var_name','value',caseinsenstive)

define('Name','Ranjana');
define('age','10',true);
define('city','LKO',false);

echo Name."<br/>".age."<br/>".city."<br/>";
echo "<hr/>";

/*
echo name."<br/>".age."<br/>".CITY."<br/>";

output:
Warning: Use of undefined constant name - assumed 'name' (this will throw an Error in a future version of PHP) in C:\xampp\htdocs\php-pract\constantinphp.php on line 12

Warning: Use of undefined constant CITY - assumed 'CITY' (this will throw an Error in a future version of PHP) in C:\xampp\htdocs\php-pract\constantinphp.php on line 12
name
10
CITY

*/

echo Name."<br/>".AGE."<br/>".city."<br/>";
echo "<hr/>";

echo constant("Name")."<br/>";
echo constant('AGE')."<br/>";
echo constant("city");


