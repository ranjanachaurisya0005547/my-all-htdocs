<?php

require_once __DIR__."/vailidation/vailidate_page.php";
require_once __DIR__."/layout/top-header.php";
?>

		      
		<div class="row">
		<div class="col-sm-6 img-area">
			<div class="row picture">
				<div class="col-sm-12 form-rgba"></div>
			</div>
		</div>
        <div class="col-sm-6 form-area">
        	<h2 style="color:#007bff;font-family:Comic Sans MS;font-weight:bold;text-shadow:3px 3px 4px orange;">Registration Form........</h2>

        	<?php if(isset($_REQUEST['key'])?$_REQUEST['key']:null==200): ?>
        	<div class="p-1 w-100 my-2 bg-success text-white" id="show_error"><?php validate(); ?></div>
        	<?php else:?>
        		<div class="p-1 w-100 my-2 bg-danger text-white" id="show_error"><?php validate(); ?></div>
            <?php endif;?>

        	<form action="send_data.php?/=<?php echo basename($_SERVER['PHP_SELF']); ?>" method="POST" enctype="multipart/form-data">
        		<br/>
        		<div class="form-row">
				<div class="form-group col-md-6">
					<label for="fname">First Name</label>
					<input type="text" class="form-control" id="fname" name="name[]"/>
				</div>
				<div class="form-group col-md-6">
					<label for="lname">Last Name</label>
					<input type="text" class="form-control" id="lname" name="name[]"/>
				</div>
			</div>
				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" class="form-control" id="email" name="email"/>
				</div>
				<div class="form-group">
					<label for="mobile">Mobile Number</label>
					<input type="text" class="form-control" id="mobile" name="mobile"/>
				</div>

				<div class="form-group">
					<label for="address">Address</label>
					<textarea name="address" id="address" class="form-control"></textarea>
				</div>
				<div class="form-row">
                <div class="form-group col-sm-6">
					<label for="city">City</label>
					<input type="text" class="form-control" id="city" name="city"/>
				</div>
				<div class="form-group col-sm-6">
					<label for="state">State</label>
					<input type="text" class="form-control" id="state" name="state"/>
				</div>
			</div>
				<div class="form-group">
					<label for="country">Country</label>
					<?php $country=['India','Austrila','America','Izariel','Londan','Russia'];?>
					<select name="country" id="country" class="form-control">
						<?php foreach($country as $key => $value):?>

						<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
				    <?php endforeach; ?>
					</select>
				</div>
                <div class="form-group">
					<label for="photo">Photo</label>
					<input type="file" class="form-control" id="photo" name="photo"/>
				</div>

				Gender<br/>					
				<label class="radio-inline"><input type="radio" name="gender" value="male"/>&nbsp;&nbsp;Male</label>
				<label class="radio-inline"><input type="radio" name="gender" value="female"/>&nbsp;&nbsp;Female</label>

				<br/>Subjects<br/>

				<?php $subject=['Hindi','English','Mathematics','Science','Geography','Drawing','Computer','Social Science'];?>
				<?php foreach($subject as $key => $value):?>
				<label class="checkbox-inline"><input type="checkbox" name="sub[]" value="<?php echo $value; ?>">&nbsp;<?php echo $value; ?></label>
                <?php endforeach; ?>

                <br/><br/>
                <input type="submit" name="sub-btn" value="Registration" class="btn btn-primary w-75 ml-5"/>

            </form>

        </div>
    </div>
    
<?php
require_once __DIR__."/layout/footer.php";
?>
