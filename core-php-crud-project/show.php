<?php

require_once __DIR__."/database/setting.php";
require_once __DIR__."/vailidation/vailidate_page.php";
require_once __DIR__."/layout/top-header.php";
?>
<?php if(isset($_REQUEST['key'])?$_REQUEST['key']:null==205): ?>
        	<div class="p-1 w-100 my-2 bg-success text-white" id="show_error"><?php validate(); ?></div>
        	<?php else:?>
        		<div class="p-1 w-100 my-2 bg-danger text-white" id="show_error"><?php validate(); ?></div>
            <?php endif;?>

<div class="row show_data">
	<h2 style="color:blue;">User Information</h2>
	<div class="col-sm-12 colo-md-5 table-area">
		<table class="table table-hover table-responsive table-sm">
			<thead class="thead-dark">
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Mobile Number</th>
				<th>Address</th>
				<th>Gender</th>
				<th>Subjects</th>
				<th>Delete</th>
				<th>Edit</th>
			</tr>
		</thead>
		<?php
             
            $select_query = "select * from registration";
            $rs = mysqli_query($conn,$select_query) or die("Query error ".mysqli_error($conn));
            $row_count = mysqli_num_rows($rs);

            if($row_count > 0):
            while($arr = mysqli_fetch_assoc($rs)):

        ?>
           <tr>
           	<td><?php echo $arr['name']?></td>
           	<td><?php echo $arr['email']?></td>
           	<td><?php echo $arr['mobile']?></td>
           	<td><?php echo $arr['address']?></td>
           	<td><?php echo $arr['gender']?></td>
           	<td><?php echo $arr['subjects']?></td>
           	<td><a href="delete.php?id=<?php echo $arr['user_id']; ?>"><i class="fa fa-trash" style="color:red;"></i></a></td>
           	<td><a href="edit.php?id=<?php echo $arr['user_id']; ?>"><i class="fa fa-edit"></i></a></td>
           </tr>

        <?php
            endwhile;
            else:
            	header("location:{$_SERVER['PHP_SELF']}?key=204");
            endif;


		 ?>		
		</table>
	</div>
</div>

<?php
require_once __DIR__."/layout/footer.php";
?>
