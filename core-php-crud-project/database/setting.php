<?php
define("DB_USER","root");
define("DB_PASSWORD","");
define("DB_HOST","localhost");
define("DB_NAME","crud_project");

$conn = null;

try{
	$conn = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);

	if(!$conn){
		throw new Exception;
	}

}catch(Exception $ex){
   
   echo "Error ".$ex;
}