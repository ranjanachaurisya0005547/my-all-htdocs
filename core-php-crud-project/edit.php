<?php

require_once __DIR__."/vailidation/vailidate_page.php";
require_once __DIR__."/layout/top-header.php";
require_once __DIR__."/database/setting.php";


$user_id = isset($_REQUEST['id'])?$_REQUEST['id']:"";
$all_data = "select * from registration where user_id = $user_id";

$rs = mysqli_query($conn,$all_data) or die("Query Error ".mysqli_error($conn));
$count = mysqli_num_rows($rs);
if($count >0){
	if($row = mysqli_fetch_assoc($rs)){
		extract($row);
	}
}
$name_arr = explode(' ',$name);
$sub_arr = explode(',',$subjects);


?>

		      
		<div class="row">
		<div class="col-sm-6 img-area">
			<div class="row picture">
				<div class="col-sm-12 form-rgba"></div>
			</div>
		</div>
        <div class="col-sm-6 form-area">
        	<h2 style="color:#007bff;font-family:Comic Sans MS;font-weight:bold;text-shadow:3px 3px 4px orange;">Edit User Here........</h2>


        	<form action="<?php echo basename($_SERVER['PHP_SELF']); ?>" method="POST" enctype="multipart/form-data">
        		<br/>
        		<div class="form-row">
				<div class="form-group col-md-6">
					<label for="fname">First Name</label>
					<input type="text" class="form-control" id="fname" name="name[]" value="<?php echo $name_arr[0]; ?>"/>
				</div>
				<div class="form-group col-md-6">
					<label for="lname">Last Name</label>
					<input type="text" class="form-control" id="lname" name="name[]" value="<?php echo $name_arr[1]; ?>"/>
				</div>
			</div>
				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" class="form-control" id="email" name="email" value="<?php echo $email; ?>"/>
				</div>
				<div class="form-group">
					<label for="mobile">Mobile Number</label>
					<input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo $mobile; ?>"/>
				</div>

				<div class="form-group">
					<label for="address">Address</label>
					<textarea name="address" id="address" class="form-control">va <?php echo $address; ?>"</textarea>
				</div>
				<div class="form-row">
                <div class="form-group col-sm-6">
					<label for="city">City</label>
					<input type="text" class="form-control" id="city" name="city" value="<?php echo $city; ?>"/>
				</div>
				<div class="form-group col-sm-6">
					<label for="state">State</label>
					<input type="text" class="form-control" id="state" name="state" value="<?php echo $state; ?>"/>
				</div>
			</div>
				<div class="form-group">
					<label for="country">Country</label>
					<?php $country1=['India','Austrila','America','Izariel','Londan','Russia'];?>
					<select name="country" id="country" class="form-control">
                        <option>----Select Country------</option>
						<?php foreach($country1 as $key => $value): ?>
						<option value="<?php echo $value; ?>" 
						<?php ($value==$country)?print("selected"):"" ?> ><?php echo $value; ?>
						</option>				    <?php endforeach; ?>
					</select>
				</div>
				    <div class="form-group">
					<label for="photo">Photo</label><br/>
					<img src="<?php echo "uploded_photoes/$photo"; ?>" alt="image not found" class="img-responsive" height="150px"/>

					<input type="file" class="form-control" id="photo" name="photo" value="<?php echo $photo; ?>"/>
				</div>

				Gender<br/>					
				<label class="radio-inline"><input type="radio" name="gender" value="male" 
                   <?php ("male"==$gender)?print("checked"):""; ?>
					/>&nbsp;&nbsp;Male</label>
				<label class="radio-inline"><input type="radio" name="gender" value="female"
                      <?php ("female"==$gender)?print("checked"):""; ?>
					/>&nbsp;&nbsp;Female</label>

				<br/>Subjects<br/>

				<?php $subject=['Hindi','English','Mathematics','Science','Geography','Drawing','Computer','Social Science'];?>
				<?php foreach($subject as $key => $sub_value):?>
				 <label class="checkbox-inline">
				 	<input type="checkbox" name="sub[]" value="<?php echo $sub_value; ?>" 
                      <?php in_array($sub_value,$sub_arr)?print("checked"):""; ?>
					>
					&nbsp;<?php echo $sub_value; ?>
				</label>
                <?php 
                  endforeach;
                ?>
                 
                <br/><br/>
                <input type="submit" name="sub-btn" value="Registration" class="btn btn-primary w-75 ml-5"/>

            </form>

        </div>
    </div>
    
<?php
require_once __DIR__."/layout/footer.php";
?>
