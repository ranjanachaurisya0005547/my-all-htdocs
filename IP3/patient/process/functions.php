<?php

include("dbconnection.php");
//print_r($con);
class PatientFunctions
{
    public $con;
    function __construct($con)
    {
       $this->con=$con;
       unset($con);
    }
    function getMaxId()
    {
        $maxId=$this->con->query("SELECT max(id) as id FROM tbl_patient_details")->fetch_assoc()['id'];
        unset($this->con);
        return $maxId;
    }
}
$dbObject=new DBconnection();
$con=$dbObject->createConnection();
$patientFunctionObj=new PatientFunctions($con);
unset($con);


?>