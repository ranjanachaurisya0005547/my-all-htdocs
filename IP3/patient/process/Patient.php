<?php

class Patient
{
   //variable declaration
   public $msg="";
   public $patientName;
   public $patientNumber; 
   public $patientDob; 
   public $patientRegDate;   
   public $patientEmail; 
   public $patientGender; 
   public $patientYear; 
   public $patientEmrName; 
   public $patientEmrPhone; 
   public $patientEmrEmail; 
   public $patientRel; 
   public $patientCountry; 
   public $patientState; 
   public $patientCity; 
   public $patientDistrict; 
   public $patientTahshil; 
   public $patientPermanentCountry; 
   public $patientPermanentState; 
   public $patientPermanentCity; 
   public $patientPermanentDistrict; 
   public $patientPermanentTahshil; 
   public $patientIdentityType; 
   public $patientIdentityNumber; 
   public $insuranceReq; 
   public $serviceProvider; 


    // senetize function
    function sanitizeData($data,$con)
    {
        $this->patientName=$con->real_escape_string($_POST['patientName']);
        $this->patientNumber=filter_var($con->real_escape_string($_POST['patientNumber']),FILTER_SANITIZE_NUMBER_INT); 
        $this->patientDob=$con->real_escape_string($_POST['patientDob']); 
        $this->patientRegDate=$con->real_escape_string($_POST['patientRegDate']);   
        $this->patientEmail=filter_var($con->real_escape_string($_POST['patientEmail']),FILTER_SANITIZE_EMAIL); 
        $this->patientGender=$con->real_escape_string($_POST['patientGender']); 
        $this->patientYear=$con->real_escape_string($_POST['patientYear']); 
        $this->patientEmrName=$con->real_escape_string($_POST['patientEmrName']); 
        $this->patientEmrPhone=filter_var($con->real_escape_string($_POST['patientEmrPhone']),FILTER_SANITIZE_NUMBER_INT); 
        $this->patientEmrEmail=filter_var($con->real_escape_string($_POST['patientEmrEmail']),FILTER_SANITIZE_EMAIL); 
        $this->patientRel=$con->real_escape_string($_POST['patientRel']); 
        $this->patientCountry=$con->real_escape_string($_POST['patientCountry']); 
        $this->patientState=$con->real_escape_string($_POST['patientState']); 
        $this->patientCity=$con->real_escape_string($_POST['patientCity']); 
        $this->patientDistrict=$con->real_escape_string($_POST['patientDistrict']); 
        $this->patientTahshil=$con->real_escape_string($_POST['patientTahshil']); 
        $this->patientPermanentCountry=$con->real_escape_string($_POST['patientPermanentCountry']); 
        $this->patientPermanentState=$con->real_escape_string($_POST['patientPermanentState']); 
        $this->patientPermanentCity=$con->real_escape_string($_POST['patientPermanentCity']); 
        $this->patientPermanentDistrict=$con->real_escape_string($_POST['patientPermanentDistrict']); 
        $this->patientPermanentTahshil=$con->real_escape_string($_POST['patientPermanentTahshil']); 
        $this->patientIdentityType=$con->real_escape_string($_POST['patientIdentityType']); 
        $this->patientIdentityNumber=$con->real_escape_string($_POST['patientIdentityNumber']); 
        $this->patientMaritalStatus=$con->real_escape_string($_POST['patientMaritalStatus']);
        $this->insuranceReq=$con->real_escape_string($_POST['insuranceReq']); 
        $this->serviceProvider=$con->real_escape_string($_POST['serviceProvider']);

        //server side pattern match
        if(!preg_match("/^([a-zA-Z' ]+)$/", $this->patientName))
        {
              return $this->msg="patientName";
        }
        if(!preg_match("/^([6789][0-9]{9})$/", $this->patientNumber))
        {
              return $this->msg="patientNumber";
        }
        if(!preg_match("/(\d{4})\-(\d{2})\-(\d{2})$/", $this->patientDob))
        {
              return $this->msg="patientDob";
        }
        if(!preg_match("/(\d{4})\-(\d{2})\-(\d{2})$/", $this->patientRegDate))
        {
              return $this->msg="patientRegDate";
        }
        if(!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $this->patientEmail))
        {
              return $this->msg="patientEmail";
        }
        if(!preg_match("/^([a-zA-Z]+)$/", $this->patientGender))
        {
              return $this->msg="patientGender";
        }
        if(!preg_match("/^([1-9][0-9]{0,2})$/", $this->patientYear))
        {
              return $this->msg="patientYear";
        }
        else
        {
           if($this->patientYear>200) 
           {
            return $this->msg="patientYear";
           }
        }
        if(!preg_match("/^([a-zA-Z' ]+)$/", $this->patientEmrName))
        {
              return $this->msg="patientEmrName";
        }
        if(!preg_match("/^([6789][0-9]{9})$/", $this->patientEmrPhone))
        {
              return $this->msg="patientEmrPhone";
        }
        if(!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $this->patientEmrEmail))
        {
              return $this->msg="patientName";
        }
        if(!preg_match("/^([a-zA-Z ]+)$/", $this->patientCountry))
        {
              return $this->msg="patientCounrty";
        }
        if(!preg_match("/^([a-zA-Z ]+)$/", $this->patientState))
        {
              return $this->msg="patientState";
        }
        if(!preg_match("/^([a-zA-Z ]+)$/", $this->patientCity))
        {
              return $this->msg="patientCity";
        }
        if(!preg_match("/^([a-zA-Z ]+)$/", $this->patientDistrict))
        {
              return $this->msg="patientDistrict";
        }
        if(!preg_match("/^([a-zA-Z0-9 ]+)$/", $this->patientTahshil))
        {
              return $this->msg="patientTahshil";
        }
        if(!preg_match("/^([a-zA-Z ]+)$/", $this->patientPermanentCountry))
        {
              return $this->msg="patientPermanentCountry";
        }
        if(!preg_match("/^([a-zA-Z ]+)$/", $this->patientPermanentState))
        {
              return $this->msg="patientPermanentState";
        }
        if(!preg_match("/^([a-zA-Z ]+)$/", $this->patientPermanentCity))
        {
              return $this->msg="patientPertmanentCity";
        }
        if(!preg_match("/^([a-zA-Z ]+)$/", $this->patientPermanentDistrict))
        {
              return $this->msg="patientPermanentDistrict";
        }
        if(!preg_match("/^([a-zA-Z0-9 ]+)$/", $this->patientPermanentTahshil))
        {
              return $this->msg="patientPermanentTahshil";
        }
        if(!preg_match("/^([a-zA-Z ]+)$/", $this->patientRel))
        {
              return $this->msg="patientRel";
        }
        if(!preg_match("/^([a-zA-Z_ ]+)$/", $this->patientIdentityType))
        {
              return $this->msg="patientIdentityType";
        }
        if(!preg_match("/^([a-zA-Z\-0-9 ]+)$/", $this->patientIdentityNumber))
        {
              return $this->msg="patientIdentityNumber";
        }
        if(!preg_match("/^([a-zA-Z]+)$/", $this->patientMaritalStatus))
        {
              return $this->msg="patientMaritalStatus";
        }
        if(!preg_match("/^([01])$/", $this->insuranceReq))
        {
              return $this->msg="insuranceReq";
        }
        if($this->serviceProvider!="")
        {
            if(!preg_match("/^([a-zA-Z0-9 ]+)$/", $this->serviceProvider))
        {
              return $this->msg="serviceProvider";
        }

        }
        
        return true;        

    }

    // Check Patient Data is empty or not
    function checkPatientDetails()
    {
        if($this->patientName != ""  && $this->patientNumber != ""  && $this->patientDob != ""  && $this->patientRegDate != ""  && $this->patientGender != ""  && $this->patientEmail != ""  && $this->patientYear != ""  && $this->patientEmrName != ""  && $this->patientEmrPhone != ""  && $this->patientEmrEmail != ""  && $this->patientRel != ""  && $this->patientCountry != ""  && $this->patientCity != ""  && $this->patientState != ""  && $this->patientDistrict != ""  && $this->patientTahshil != ""  && $this->patientPermanentCountry != ""  && $this->patientPermanentCity != ""  && $this->patientPermanentState != ""  && $this->patientPermanentDistrict != ""  && $this->patientPermanentTahshil != ""  && $this->patientIdentityType != ""  && $this->patientIdentityNumber != ""  && $this->patientMaritalStatus != "")
        {
            if($this->insuranceReq==1)
            {
                if($this->serviceProvider!="")
                {
                   return true;
                }
                else
                {
                    return false;
                }
            }
            
            return true;
        }
        else
        {
            return false;
        }
    }

    //find patient Details
    function findPatientDetails($con)
    {
        $query_num_rows=$con->query("SELECT * FROM `tbl_patient_details` WHERE `patient_email`='".$this->patientEmail."' || `patient_contact`='".$this->patientNumber."'")->num_rows;
        if($query_num_rows>0)
        {
            return false;
        }
        return true;
    }


    //find Id of current patient Details
    function findPatientId($con)
    {
        $query=$con->query("SELECT id FROM `tbl_patient_details` WHERE `patient_email`='".$this->patientEmail."' || `patient_contact`='".$this->patientNumber."'");
        if($query->num_rows>0)
        {
           $patientId=$query->fetch_assoc()['id'];
           return $patientId;
        }
        return false;
    }




    // Insert Patient Details
    function insertPatientDetails($con)
    {
        
                $query=$con->query("INSERT INTO `tbl_patient_details` (`patient_name`, `patient_contact`, `patient_dob`, `patient_reg_date`, `patient_email`, `patient_gender`, `patient_age`, `patient_country`, `patient_city`, `patient_state`, `patient_district`,  `patient_tahshil`, `patient_permanent_country`, `patient_permanent_city`, `patient_permanent_state`, `patient_permanent_distirict`,  `patient_permanent_tahshil`, `patient_identity_type`, `patient_identity_no`, `marital_status`, `insurance_req`, `insurance_provider`) VALUES ('".$this->patientName."', '".$this->patientNumber."', '".$this->patientDob."', '".$this->patientRegDate."', '".$this->patientEmail."', '".$this->patientGender."', '".$this->patientYear."', '".$this->patientCountry."', '".$this->patientCity."', '".$this->patientState."', '".$this->patientDistrict."', '".$this->patientTahshil."', '".$this->patientPermanentCountry."', '".$this->patientPermanentCity."', '".$this->patientPermanentState."', '".$this->patientPermanentDistrict."', '".$this->patientPermanentTahshil."', '".$this->patientIdentityType."', '".$this->patientIdentityNumber."', '".$this->patientMaritalStatus."', '".$this->insuranceReq."', '".$this->serviceProvider."')");
                if($query)
                {
                    //get id current inserted data
                    if($patientId=$this->findPatientId($con))
                    {
                        $query=$con->query("INSERT INTO `tbl_patient_emergency_contact` (`patient_id`, `patient_name`, `patient_email`, `patient_contact`, `patient_relationship`) VALUES ('".$patientId."', '".$this->patientEmrName."', '".$this->patientEmrEmail."',  '".$this->patientEmrPhone."', '".$this->patientRel."')");
                        if($query)
                        {
                            return true;
                        }
                    }   
                    
                    
                }
                else
                {
                    $this->msg="Patient details has been not inserted";
                    return $this->msg;
                }
           
     
    }
   
}

?>