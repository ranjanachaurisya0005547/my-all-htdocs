<?php
class DBConnection
{

    private $hostName="localhost";
    private $rootName="root";
    private $password="";
    private $dbName="patient";

    //define connection function
    public function createConnection()
    {
        try {

            $dbObject=new mysqli($this->hostName,$this->rootName,$this->password,$this->dbName);
            return $dbObject;
          } 
          catch (mysqli_sql_exception  $e) {

            throw $e;
            die;
          }
          
    }

}


?>