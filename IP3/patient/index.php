<!DOCTYPE html>
<html>
    <head>
        <title>Patient | Registration</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="preload" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" as="css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" >
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link rel="preload" href="assets/css/style.css" as="css">
        <link rel="stylesheet" href="assets/css/style.css">
        <script type="text/javascript" src="assets/js/validate.js"></script> 
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11" async></script>
    </head>
    <body>
        <!-- header section start-->
        <div class="header d-xl-none d-lg-none d-xs-none d-sm-block bg-info">
            <a href="#"><img src="assets/img/Sparkle.png" class=" logo" /></a>
            <a  href="#"><i class="fa fa-medkit sidebar-icon" aria-hidden="true"></i><span class="sr-only">(current)</span></a>
            <a  href="#"><i class="fa fa-user-md sidebar-icon" aria-hidden="true"></i></a>
            <a  href="#"> <i class="fa fa-calendar sidebar-icon" aria-hidden="true"></i></a>
            <a  href="#"><i class="fa fa-bed sidebar-icon" aria-hidden="true"></i></a>
            <a  href="#"><i class="fa fa-bed sidebar-icon" aria-hidden="true"></i></a>
            <a  href="#"><i class="fa fa-bed sidebar-icon" aria-hidden="true"></i></a>
            <a  href="#"><i class="fa fa-question-circle sidebar-icon" aria-hidden="true"></i></a>
            <a  href="#"><i class="fa fa-wheelchair sidebar-icon" aria-hidden="true"></i></a>
            <a  href="#"><i class="fa fa-address-book-o sidebar-icon" aria-hidden="true"></i></a>
            <div class="header-right">
             <a href="#"><i class="fa fa-bell pr-2"aria-hidden="true"></i></a>
             <a href="#"><i class="fa fa-windows pr-2"aria-hidden="true"></i></a>
             <a href="#"><i class="fa fa-user-circle-o pr-2"aria-hidden="true"></i></a>
            </div>
          </div>
        <nav class="navbar navbar-expand-lg navbar-light bg-info w-100 ">
            <a class="navbar-brand" href="#"><img src="assets/img/Sparkle.png" class=" logo" /></a>
            <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class=" active">
                    <a class="nav-link" href="#"><i class="fa fa-medkit sidebar-icon" aria-hidden="true"></i><span class="sr-only">(current)</span></a>
                  </li>
                  <li class="">
                    <a class="nav-link" href="#"><i class="fa fa-user-md sidebar-icon" aria-hidden="true"></i></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#"> <i class="fa fa-calendar sidebar-icon" aria-hidden="true"></i></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-bed sidebar-icon" aria-hidden="true"></i></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-bed sidebar-icon" aria-hidden="true"></i></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-bed sidebar-icon" aria-hidden="true"></i></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-question-circle sidebar-icon" aria-hidden="true"></i></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-wheelchair sidebar-icon" aria-hidden="true"></i></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-address-book-o sidebar-icon"aria-hidden="true"></i></a>
                  </li>
              </ul>
              <ul class="navbar-nav" >
                  <li class="nav-item">
                    <i class="fa fa-bell pr-2"aria-hidden="true"></i>
                  </li>
                  <li class="nav-item">
                      <i class="fa fa-windows pr-4 pl-3"aria-hidden="true"></i>

                  </li>
                  <li class="nav-item">
                    <i class="fa fa-user-circle-o"aria-hidden="true"></i>
                  </li>
              </ul>
              
        
            
        
            
            </div>
          </nav>


        <!--- header section End -->
        <section class="mt-5">
            
            <div class="container-fluid">
                <div class="row">
                    <!--- Sidebar start -->
                    <div class="side mt-lg-2 mt-xl-2 mt-md-2 pl-0 pr-0 text-secondary border border-light border-top-0 ">
                          <ul class="pl-4">
                              <li>
                                 <i class="fa fa-list sidebar-icon" aria-hidden="true"></i>
                              </li>
                              <div class="dropdown-divider"></div>
                              <li>
                                 <i class="fa fa-clock-o sidebar-icon" aria-hidden="true"></i>
                              </li>
                              <li>
                                 <i class="fa fa-calendar-o sidebar-icon" aria-hidden="true"></i>
                              </li>
                              <li>
                                <i class="fa fa-plane sidebar-icon" aria-hidden="true"></i>
                              </li>
                              <li>
                                 <i class="fa fa-calendar-check-o sidebar-icon" aria-hidden="true"></i>
                              </li>
                              <li>
                                 <i class="fa fa-calendar sidebar-icon" aria-hidden="true"></i>
                              </li>
                              <li>
                                <i class="fa fa-question-circle-o sidebar-icon" aria-hidden="true"></i>
                              </li>
                              <li>
                                  <i class="fa fa-hourglass-o sidebar-icon" aria-hidden="true"></i>
                              </li>
                          </ul>
                    </div>
                    <!-- Sidebar End -->
                    <!-- Main section Start-->
                    <div class="main mt-lg-2 mt-xl-2 mt-md-2 pl-4">
                        
                            <div class="row pt-3 border border-light border-top-0">
                                <div class="col-xs-12 pt-md-2">
                                    <label class="head-label text-secondary float-left">Patient Registration</label>
                                
                                    <i class="fa fa-times float-right text-dark float-right" aria-hidden="true"></i>
                                </div>
                            </div>
                             <div class="row hidden-xs">
                                 <div class="col-10">
                                    <div class="row pt-4 pl-4">
                                        <div class="col-3">
                                            <span class="hidden-lg hidden-xl hidden-md h6 text-dark text-bold  process-text pr-0">Add Patient</span>
                                            <div class="row">
                                                <div class="col-1 col-xs-1 pt-3 pl-0 pr-0">
                                                   <span class="process-circle  bg-primary   border border-secondary  rounded-circle p-3">1</span> 
                                                </div>
                                                <span class="col-5 col-xs-6 hidden-sm pt-1 h6 text-dark text-bold pl-3 process-text pr-0">Add Patient</span>
                                                <span class="col-5 col-xs-5  h-0 pt-3 pl-0 pr-0 "><hr class="process-line ml-sm-5"/></span>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <span class="hidden-lg hidden-xl hidden-md  pt-1 h6 text-dark   process-text pr-0">Reg. Charge</span>
                                            <div class="row">
                                                <div class="col-1 col-xs-1  pt-3 pl-0 pr-0">
                                                   <span class=" rounded-circle  text-center border border-secondary p-3">2</span>
                                                </div> 
                                                <span class="col-4 col-xs-6 hidden-sm pt-1 h6 text-dark   process-text pr-0">Reg. Charge</span>
                                                <span class="col-6 col-xs-5  h-0 pt-3 pl-0 pr-0"><hr class="process-line ml-sm-5"/></span>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <span class="hidden-lg hidden-xl hidden-md pt-1 h6 text-dark   process-text pr-0">Payment</span>
                                            <div class="row">
                                                <div class="col-1 col-xs-1  pt-3 pl-0 pr-0">
                                                <span class=" rounded-circle  text-center border border-secondary p-3 ">3</span> 
                                                </div>
                                                <span class="col-4 col-xs-6 hidden-sm  pt-1 h6 text-dark   process-text pr-0">Payment</span>
                                                <span class="col-6 col-xs-5  h-0 pt-3 pl-0 pr-0"><hr class="process-line ml-sm-5"/></span>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <span class="hidden-xl hidden-lg hidden-md pt-1 h6 text-dark   process-text  pr-0">Payment Receipt</span>
                                            <div class="row">
                                                <div class="col-1 col-xs-1  pt-3 pl-0 pr-0">
                                                <span class="rounded-circle  text-center border border-secondary p-3 ">4</span> 
                                            </div>
                                                <span class="col-5 col-xs-6 hidden-sm pt-1 h6 text-dark   process-text  pr-0">Payment Receipt</span>
                                                <span class="col-5 col-xs-5  h-0 pt-3 pl-0 pr-0"><hr class="process-line ml-sm-5"/></span>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                 </div>
                                 <div class="col-2">
                                     
                                     <div class="row pt-4 pl-4">
                                        <div class="col-12">
                                            <span class="hidden-lg hidden-xl hidden-md text-dark text-bold pr-0 process-text">Appointmet</span>
                                            <div class="row">
                                                <div class="col-1 col-xs-1  pt-3 pl-0 pr-0">
                                                <span class=" rounded-circle  text-center border border-secondary p-3">5</span> 
                                            </div>
                                                <span class="col-6 hidden-sm hidden-xs pt-1 h6 text-dark text-bold pr-0 process-text">Appointmet</span>
                                            </div>
                                        </div>
                                     </div>
                                 </div>
                             </div>
                            
                            <div class="clearfix"></div>
                            <div class="row pt-5">
                                <div class="col-md-12 col-lg-10 ol-xl-10 col-sm-12 col-xs-12 custom-text">
                                    <form>
                                    <!---Patient Personal Details-->
                                    <div class="row">
                                        <div class="col-md-2 col-xs-12 col-sm-12 pt-5 text-center">
                                            <img src="https://i.pinimg.com/originals/6b/aa/98/6baa98cc1c3f4d76e989701746e322dd.png " class="profile-img"/>
                                        </div>
                                        <div class="col-md-3 col-xs-12 col-sm-12 pt-5 ">
                                            <div class="row">
                                                <?php 
                                                include("process/functions.php");
                                                $id=$patientFunctionObj->getMaxId()+1;
                                                ?>
                                                <div class="col-5 p-2"><span class="text-danger">*</span>&nbsp;<label>Patient Id :</label></div>
                                                <div class="col-7 p-2"><input type="text" name="patientId" class="form-control" placeholder="Patient Code Number" value="PATREG<?php echo $id;?>" readonly/></div>
                                                <div class="col-12"><span class="patientNumber float-right"></span></div>
                                                <div class="col-5 p-2">&nbsp;<span class="text-danger">*</span>&nbsp;<label>Phone No:</label></div>
                                                <div class="col-7 p-2"><input type="text" name="patientNumber"id="patientNumber" class="form-control"  placeholder="Phone Number" maxlength="10" /></div>
                                                <div class="col-12"><span class="patientName float-right"></span></div>
                                                <div class="col-5 p-2"><span class="text-danger">*</span>&nbsp;<label>Name :</label></div>
                                                <div class="col-7 p-2"> <input type="text" name="patientName" id="patientName" class="form-control" placeholder="Patient Name" /></div>
                                                <div class="col-12"><span class="patientDob float-right"></span></div>
                                                <div class="col-5 p-2"><span class="text-danger">*</span>&nbsp;<label>DOB :</label></div>
                                                
                                                <div class="col-7 p-2"> <input type="date" name="patientDob" id="patientDob" class="form-control" placeholder="DD/MM/YYY"  min='1899-01-01' max='<?php echo date('Y-m-d');?>'/></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-12 col-sm-12 pt-5 ">
                                            <div class="row">
                                                <div class="col-12"><span class="patientRegDate float-right"></span></div>
                                                <div class="col-5 p-2"><span class="text-danger">*</span><label>Reg. Date :</label></div>
                                                <div class="col-7 p-2"><input type="date" name="patientRegDate" id="patientRegDate" class="form-control" placeholder="Registration Date" /></div>
                                                <div class="col-12"><span class="patientEmail float-right"></span></div>
                                                <div class="col-5 p-2"><span class="text-danger">*</span>&nbsp;<label>Email</label></div>
                                                <div class="col-7 p-2"><input type="text" name="patientEmail" id="patientEmail" class="form-control" placeholder="Email ID" /></div>
                                                <div class="col-12"><span class="patientGender float-right"></span></div>
                                                <div class="col-5 p-2"><span class="text-danger">*</span>&nbsp;<label>Gender:</label></div>
                                                <div class="col-7 p-2"><select name="patientGender" id="patientGender" class="form-control" >
                                                    <option value="">Select Gender</option>
                                                    <option value="male">Male</option>
                                                    <option value="female">Female</option>
                                                    <option value="other">Other</option>
                                                </select></div>
                                                <div class="col-12"><span class="patientYear float-right"></span></div>
                                                <div class="col-5 p-2"><span class=" "></span>&nbsp;<span class="text-danger">*</span>&nbsp;<label>Age(Year):</label></div>
                                                <div class="col-7 p-2"><input type="text" name="patientYear" id="patientYear"  class="form-control" placeholder="0" maxlength="4" /></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-12 col-sm-12 p-5">
                                            <h4>Emergency Contact</h4>
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-6 p-2 pr-3">
                                                <span class="text-danger">*</span><label>Name</label><span class="patientEmrName float-right"></span>
                                                    <input type="text" class="form-control" name="patientEmrName" id="patientEmrName" placeholder="Name" />
                                                </div>
                                                <div class=" col-md-6 col-sm-6 col-xs-6 p-2 pr-3">
                                                <span class="text-danger">*</span><label>Email</label><span class="patientEmrEmail float-right"></span>
                                                    <input type="email" name="patientEmrEmail"class="form-control" id="patientEmrEmail" placeholder="Email" />
                                                </div>   
                                            </div>
                                            <div class="row">
                                                <div class=" col-md-12 col-xs-5 col-sm-5  p-0 pt-2 pr-3"><span class="text-danger">*</span><label>Phone No.</label><span class="patientEmrPhone float-right"></span>&nbsp;</div>
                                                <div class="col-md-12  col-xs-7 col-sm-7 p-0 pt-2"><input type="number" class="form-control" name="patientEmrPhone" id="patientEmrPhone" placeholder="Phone Number" maxlength="10" /></div>
                                                <div class="  col-xs-5 col-sm-12  col-lg-12 p-0 pt-2 pr-3"><span class="text-danger">*</span><label>Relationship</label><span class="patientRel float-right"></span>&nbsp;</div>
                                                <div class="  col-xs-7 col-sm-12  col-lg-12 p-0 pt-2">
                                                    <select name="patientRel" id="patientRel" class="form-control" >
                                                    <option value="">Please Select Relationship</option>
                                                    <option value="father">Father</option>
                                                    </select>
                                                </div>
                                            </div>   
                                        </div>
                                     </div>
                                    
                                        <div class="row  custom-text">
                                
                                            <div class="col-xs-12 col-sm-6 col-md-4  pt-5">
                                                <h5>Present Address</h5>
                                                <div class="row pt-3 ">
                                                <div class="col-12 pr-5"><span class="patientCountry float-right"></span></div>
                                                    <div class="col-4 pr-0  ">
                                                    <span class="text-danger">*</span>&nbsp;<label>Country :</label>
                                                    </div>
                                                    <div class="col-8 pr-5">
                                                        <select name="patientCountry" id="patientCountry" class="form-control" >
                                                            <option value="">Select Country</option>
                                                            <option value="india">India</option>
                                                            <option value="us">US</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-12 pr-5"><span class="patientState float-right"></span></div>
                                                    <div class="col-4 pr-0 mt-3">
                                                    <span class="text-danger">*</span>&nbsp;<label>State :</label>
                                                    </div>
                                                    <div class="col-8 mt-3 pr-5">
                                                        <select name="patientState" id="patientState" class="form-control" >
                                                        <option value="">Select State</option>
                                                        <option value="Andhra Pradesh">Andhra Pradesh</option>
                                                        <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                                        <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                                        <option value="Assam">Assam</option>
                                                        <option value="Bihar">Bihar</option>
                                                        <option value="Chandigarh">Chandigarh</option>
                                                        <option value="Chhattisgarh">Chhattisgarh</option>
                                                        <option value="Dadar and Nagar Haveli">Dadar and Nagar Haveli</option>
                                                        <option value="Daman and Diu">Daman and Diu</option>
                                                        <option value="Delhi">Delhi</option>
                                                        <option value="Lakshadweep">Lakshadweep</option>
                                                        <option value="Puducherry">Puducherry</option>
                                                        <option value="Goa">Goa</option>
                                                        <option value="Gujarat">Gujarat</option>
                                                        <option value="Haryana">Haryana</option>
                                                        <option value="Himachal Pradesh">Himachal Pradesh</option>
                                                        <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                                                        <option value="Jharkhand">Jharkhand</option>
                                                        <option value="Karnataka">Karnataka</option>
                                                        <option value="Kerala">Kerala</option>
                                                        <option value="Madhya Pradesh">Madhya Pradesh</option>
                                                        <option value="Maharashtra">Maharashtra</option>
                                                        <option value="Manipur">Manipur</option>
                                                        <option value="Meghalaya">Meghalaya</option>
                                                        <option value="Mizoram">Mizoram</option>
                                                        <option value="Nagaland">Nagaland</option>
                                                        <option value="Odisha">Odisha</option>
                                                        <option value="Punjab">Punjab</option>
                                                        <option value="Rajasthan">Rajasthan</option>
                                                        <option value="Sikkim">Sikkim</option>
                                                        <option value="Tamil Nadu">Tamil Nadu</option>
                                                        <option value="Telangana">Telangana</option>
                                                        <option value="Tripura">Tripura</option>
                                                        <option value="Uttar Pradesh">Uttar Pradesh</option>
                                                        <option value="Uttarakhand">Uttarakhand</option>
                                                        <option value="West Bengal">West Bengal</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-12 pr-5"><span class="patientDistrict float-right"></span></div>
                                                    <div class="col-4 pr-0 mt-3">
                                                    <span class="text-danger">*</span>&nbsp;<label>District :</label>
                                                    </div>
                                                    <div class="col-8 mt-3 pr-5">
                                                        <select name="patientDistrict" id="patientDistrict" class="form-control" >
                                                            <option value=""> Select Distritct</option>
                                                            <option value="allahabad">Allahabad</option>
                                                            <option value="mirzapur">Mirzapur</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-12 pr-5"><span class="patientCity float-right"></span></div>
                                                    <div class="col-4 pr-0 mt-3">
                                                    <span class="text-danger">*</span>&nbsp;<label>City:</label>
                                                    </div>
                                                    <div class="col-8 mt-3 pr-5" >
                                                        <input type="text" name="patientCity" id="patientCity" class="form-control" placeholder="City/Town/Village" />
                                                    </div>
                                                    <div class="col-12 pr-5"><span class="patientTahshil float-right"></span></div>
                                                    <div class="col-4 pr-0 mt-3">
                                                    <span class="text-danger">*</span>&nbsp;<label>Tahshil:</label>
                                                    </div>
                                                    <div class="col-8 mt-3 pr-5">
                                                        <input type="text" name="patientTahshil" id="patientTahshil" class="form-control" placeholder="Tahshil" />
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4 col-sm-6 pt-5">
                                                <h5><input type="checkbox" name="patientPermanentAdd"/>&emsp;Permanent Address</h5>
                                                <div class="row pt-3">
                                                   <div class="col-12 pr-5"><span class="patientPermanentCounrty float-right"></span></div>
                                                    <div class="col-4 pr-0 ">
                                                    <span class="text-danger">*</span>&nbsp;<label>Country :</label>
                                                    </div>
                                                    <div class="col-8 pr-5">
                                                        <select name="patientPermanentCountry" id="patientPermanentCounrty"class="form-control" >
                                                           <option value="">Select Country</option>
                                                            <option value="india">India</option>
                                                            <option value="us">US</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-12 pr-5"><span class="patientPermanentState float-right"></span></div>
                                                    <div class="col-4 pr-0 mt-3">
                                                    <span class="text-danger">*</span>&nbsp;<label>State :</label>
                                                    </div>
                                                    <div class="col-8 mt-3 pr-5">
                                                        <select name="patientPermanentState" id="patientPermanentState"class="form-control" >
                                                        <option value="">Select State</option>
                                                        <option value="Andhra Pradesh">Andhra Pradesh</option>
                                                        <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                                        <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                                        <option value="Assam">Assam</option>
                                                        <option value="Bihar">Bihar</option>
                                                        <option value="Chandigarh">Chandigarh</option>
                                                        <option value="Chhattisgarh">Chhattisgarh</option>
                                                        <option value="Dadar and Nagar Haveli">Dadar and Nagar Haveli</option>
                                                        <option value="Daman and Diu">Daman and Diu</option>
                                                        <option value="Delhi">Delhi</option>
                                                        <option value="Lakshadweep">Lakshadweep</option>
                                                        <option value="Puducherry">Puducherry</option>
                                                        <option value="Goa">Goa</option>
                                                        <option value="Gujarat">Gujarat</option>
                                                        <option value="Haryana">Haryana</option>
                                                        <option value="Himachal Pradesh">Himachal Pradesh</option>
                                                        <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                                                        <option value="Jharkhand">Jharkhand</option>
                                                        <option value="Karnataka">Karnataka</option>
                                                        <option value="Kerala">Kerala</option>
                                                        <option value="Madhya Pradesh">Madhya Pradesh</option>
                                                        <option value="Maharashtra">Maharashtra</option>
                                                        <option value="Manipur">Manipur</option>
                                                        <option value="Meghalaya">Meghalaya</option>
                                                        <option value="Mizoram">Mizoram</option>
                                                        <option value="Nagaland">Nagaland</option>
                                                        <option value="Odisha">Odisha</option>
                                                        <option value="Punjab">Punjab</option>
                                                        <option value="Rajasthan">Rajasthan</option>
                                                        <option value="Sikkim">Sikkim</option>
                                                        <option value="Tamil Nadu">Tamil Nadu</option>
                                                        <option value="Telangana">Telangana</option>
                                                        <option value="Tripura">Tripura</option>
                                                        <option value="Uttar Pradesh">Uttar Pradesh</option>
                                                        <option value="Uttarakhand">Uttarakhand</option>
                                                        <option value="West Bengal">West Bengal</option>
                                                            
                                                        </select>
                                                    </div>
                                                    <div class="col-12 pr-5"><span class="patientPermanentDistrict float-right"></span></div>
                                                    <div class="col-4 pr-0 mt-3">
                                                    <span class="text-danger">*</span>&nbsp;<label>District :</label>
                                                    </div>
                                                    <div class="col-8 mt-3 pr-5">
                                                        <select name="patientPermanentDistrict" id="patientPermanentDistrict" class="form-control" >
                                                            <option value="">Select District</option>
                                                            <option value="allahabad">Allahabad</option>
                                                            <option value="mirzapur">Mirzapur</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-12 pr-5"><span class="patientPermanentCity float-right"></span></div>
                                                    <div class="col-4 pr-0 mt-3">
                                                    <span class="text-danger">*</span>&nbsp;<label>City:</label>
                                                    </div>
                                                    <div class="col-8 mt-3 pr-5">
                                                        <input type="text" name="patientPermanentCity" id="patientPermanentCity"class="form-control" placeholder="City/Town/Village" />
                                                    </div>
                                                    <div class="col-12 pr-5"><span class="patientPermanentTahshil float-right"></span></div>
                                                    <div class="col-4 pr-0 mt-3">
                                                    <span class="text-danger">*</span>&nbsp;<label>Tahshil:</label>
                                                    </div>
                                                    <div class="col-8 mt-3 pr-5">
                                                        <input type="text" name="patientPermanentTahshil" id="patientPermanentTahshil" class="form-control" placeholder="Tahshil" />
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4 col-sm-6 pt-5">
                                                <h5>Identity Details</h5>
                                                <div class="row pt-3">
                                                <div class="col-12 pr-5"><span class="patientIdentityType float-right"></span></div>
                                                    <div class="col-5 pr-0 ">
                                                    <span class="text-danger">*</span>&nbsp;<label>Identity Type :</label>
                                                    </div>
                                                    <div class="col-7 ">
                                                        <select name="patientIdentityType" id="patientIdentityType" class="form-control" >
                                                            <option value="">Identity Type</option>
                                                            <option value="adhar_card">ADHAR CARD</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-12 pr-5"><span class="patientIdentityNumber float-right"></span></div>
                                                    <div class="col-5 pr-0 mt-3">
                                                    <span class="text-danger">*</span>&nbsp;<label>Identity No. :</label>
                                                    </div>
                                                    <div class="col-7 mt-3 ">
                                                        <input type="number" class="form-control" name="patientIdentityNumber" id="patientIdentityNumber" placeholder="Identity Card Number" />
                                                    </div>
                                                    <div class="col-12 pr-5"><span class="patientMaritalStatus float-right"></span></div>
                                                    <div class="col-5 pr-0 mt-3">
                                                    <span class="text-danger">*</span>&nbsp;<label>Marital Status :</label>
                                                    </div>
                                                    <div class="col-7 mt-3 ">
                                                        <select name="patientMaritalStatus" id="patientMaritalStatus" class="form-control" >
                                                            <option value="">Select Gender</option>
                                                            <option value="single">Single</option>
                                                            <option value="married">Married</option>
                                                        </select>
                                                    </div>
                                                    
                                                    <div class="col-5 pr-0 mt-3">
                                                        <span class="text-danger">*</span>&nbsp;<label>Is Insurance Req. :</label>
                                                    </div>
                                                    <div class="col-7 mt-3 ">
                                                        Yes: <input type="radio" name="insuranceReq"  value="1"/>
                                                        No: <input type="radio" name="insuranceReq" value="0" checked/>
                                                    </div>
                                                    <div class="col-12 pr-5"><span class="serviceProvider float-right"></span></div>
                                                    <div class="  col-5   pr-0 mt-3">
                                                    <label>Insurance Providers:</label>
                                                    </div>
                                                    <div class=" col-7 mt-3 ">
                                                        <select name="serviceProvider" id="serviceProvider" class="form-control">
                                                            <option value="">Select Insurance Service Provider</option>
                                                            <option value="1">Option 1</option>
                                                            <option value="2">Option 2</option>
                                                            <option value="3">Option 3</option>
                                                        </select>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-12 p-5">
                                                <input type="submit" class="btn-sm btn-info text-white patientFormBtn p-3 pl-5 pr-5 float-right" value="submit" name="submit"/>
                                            </div>
                                        </div>
                                    </form>

                                    </div>
                                    <!---->
                                    <!--Patient Address Details-->
                                    
                                    <!----->
                               
                                <div class="col-lg-2 pt-5">
                                    <div class="card">
                                        <div class="card-header">
                                           <span>NearestMatch</span><i class="fa fa-thumb-tack float-right text-dark" aria-hidden="true"></i>
                                        </div>
                                        <div class="card-body">
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                           <span></span><i class="fa fa-pin"></i>
                                        </div>
                                        <div class="card-body text-center p-3">
                                             <span>No Data</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            
                        
                    </div>
                    <!-- main section end-->
                </div>
            </div>
        </section>
    </body>
   
    
</html>
