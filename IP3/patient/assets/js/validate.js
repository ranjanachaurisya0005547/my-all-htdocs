
    $(document).ready(function(){
        if(window.matchMedia('(min-width:768px)').matches || window.matchMedia('(min-width:320px)').matches || !window.matchMedia('(max-width:1024px)').matches){
            $('nav').addClass('position-absolute');
            $('nav').css('z-index','2');
            $('section').addClass('position-absolute');
            $('section').css('top','3.5rem');
            $('section').css('z-index','1');
        }
    });
    /* validate input phone number*/
   
    $(document).ready(function () {
        $("#patientNumber").keypress(function (e) {
            var keyCode = e.keyCode || e.which;
            //Regex for Validate number start with 6,7,8,9.
            var regex =/^\d+$/;
 
            //Validate TextBox value against the Regex.
            var isValid = regex.test(String.fromCharCode(keyCode));
            if (!isValid) {
                $('.patientNumber').html('<i class="fa fa-times-circle-o text-danger"></i>');
            }
            else
            {
                $('.patientNumber').html('<i class="fa fa-check-circle-o text-success"></i>')
            }
            setTimeout(function(){
               $('.patientNumber').html('');
            },3000);
            return isValid;
        });
        
      /* validate name */
        $("#patientName").keypress(function (e) {
            var keyCode = e.keyCode || e.which;
             //Regex for Valid Characters i.e. Alphabets.
            var regex = /^[a-zA-Z ]+$/;
 
            //Validate TextBox value against the Regex.
            var isValid = regex.test(String.fromCharCode(keyCode));
            if (!isValid) {
                $('.patientName').html('<i class="fa fa-times-circle-o text-danger"></i>');
            }
            else
            {
                $('.patientName').html('<i class="fa fa-check-circle-o text-success"></i>')
            }
            setTimeout(function(){
               $('.patientName').html('');
            },3000);
            return isValid;
        });

        /* validate year*/
        $("#patientYear").keypress(function (e) {
            var keyCode = e.keyCode || e.which;
            //Regex for Valid Characters i.e. year not start from 0.
            var regex = /^[1-9]+$/;
 
            //Validate TextBox value against the Regex.
            var isValid = regex.test(String.fromCharCode(keyCode));
            if (!isValid) {
                $('.patientYear').html('<i class="fa fa-times-circle-o text-danger"></i>');
            }
            else
            {
                $('.patientYear').html('<i class="fa fa-check-circle-o text-success"></i>')
            }
            setTimeout(function(){
               $('.patientYear').html('');
            },3000);
            return isValid;
        });

        /* validate emergency name*/
        $("#patientEmrName").keypress(function (e) {
            var keyCode = e.keyCode || e.which;
            //Regex for Valid Characters i.e. Alphabets.
            var regex = /^[a-zA-Z ]+$/;
 
            //Validate TextBox value against the Regex.
            var isValid = regex.test(String.fromCharCode(keyCode));
            if (!isValid) {
                $('.patientEmrName').html('<i class="fa fa-times-circle-o text-danger"></i>');
            }
            else
            {
                $('.patientEmrName').html('<i class="fa fa-check-circle-o text-success"></i>')
            }
            setTimeout(function(){
               $('.patientEmrName').html('');
            },3000);
            return isValid;
        });

       
        /* validate mail*/
      //   $("#patientEmail").keypress(function (e) {
      //       var keyCode = e.keyCode || e.which;
      //       //Regex for Valid Characters i.e. Alphabets.
      //       var regex =/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
 
      //       //Validate TextBox value against the Regex.
      //       var isValid = regex.test(String.fromCharCode(keyCode));
      //       if (!isValid) {
      //           $('.patientEmail').html('<i class="fa fa-times-circle-o text-danger"></i>');
               
      //       }
      //       else
      //       {
      //           $('.patientEmail').html('<i class="fa fa-check-circle-o text-success"></i>')
      //       }
      //       return isValid;
      //   });

        /* validate emergency mail*/
        // $("#patientEmrEmail").keypress(function (e) {
        //     var keyCode = e.keyCode || e.which;
        //     //Regex for Valid Characters i.e. Alphabets.
        //     var regex =  /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
 
        //     //Validate TextBox value against the Regex.
        //     var isValid = regex.test(String.fromCharCode(keyCode));
        //     if (!isValid) {
        //         $('.patientEmrEmail').html('<i class="fa fa-times-circle-o text-danger"></i>');
        //     }
        //     else
        //     {
        //         $('.patientEmrEmail').html('<i class="fa fa-check-circle-o text-success"></i>')
        //     }
        //     return isValid;
        // });

        /* validate emergency phone number*/
        $("#patientEmrPhone").keypress(function (e) {
            var keyCode = e.keyCode || e.which;
            //Regex for Validate number i.e. start from 6,7,8,9 .
            var regex = /^[0-9]+$/;
 
            //Validate TextBox value against the Regex.
            var isValid = regex.test(String.fromCharCode(keyCode));
            if (!isValid) {
                $('.patientEmrPhone').html('<i class="fa fa-times-circle-o text-danger"></i>');
            }
            else
            {
                $('.patientEmrPhone').html('<i class="fa fa-check-circle-o text-success"></i>')
            }
            setTimeout(function(){
               $('.patientEmrPhone').html('');
            },3000);
            return isValid;
        });

        //present address and permanent address both are same.
        $('input[name="patientPermanentAdd"]').click(function(){
           if($('input[name="patientPermanentAdd"]').is(':checked'))
           {
               var t=$('select[name="patientCountry"]').find(':selected').val();
               $('select[name="patientPermanentCountry"] option[value="'+t+'"]').attr("selected", "selected");
               var t=$('select[name="patientState"]').find(':selected').val();
               $('select[name="patientPermanentState"] option[value="'+t+'"]').attr("selected", "selected");
               var t=$('select[name="patientDistrict"]').find(':selected').val();
               $('select[name="patientPermanentDistrict"] option[value="'+t+'"]').attr("selected", "selected");
               $('input[name="patientPermanentCity"]').val($('input[name="patientCity"]').val());
               $('input[name="patientPermanentTahshil"]').val($('input[name="patientTahshil"]').val());
           }
           else{
            var t=$('select[name="patientCountry"]').find(':selected').val();
            $('select[name="patientPermanentCountry"] option[value="'+t+'"]').attr("selected", false);
            var t=$('select[name="patientState"]').find(':selected').val();
            $('select[name="patientPermanentState"] option[value="'+t+'"]').attr("selected", false);
            var t=$('select[name="patientDistrict"]').find(':selected').val();
            $('select[name="patientPermanentDistrict"] option[value="'+t+'"]').attr("selected", false);
            $('input[name="patientPermanentCity"]').val("");
            $('input[name="patientPermanentTahshil"]').val("");
           }
        });
    });

    $(document).ready(function(){
      $('form').on('submit', function (e) {

         e.preventDefault();
            var valid=true;
            if($('#patientName').val()!="")
            {
              $('.patientName').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientName').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientName').html('');
            },3000);
            if($('#patientNumber').val()!="")
            {
               $('.patientNumber').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientNumber').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientNumber').html('');
            },3000);
            if($('#patientDob').val()!="")
            {
               $('.patientDob').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientDob').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientDob').html('');
            },3000);
            if($('#patientRegDate').val()!="")
            {
               //  alert($('#patientRegDate').val());
               $('.patientRegDate').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientRegDate').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientRegDate').html('');
            },3000);
            if($('#patientEmail').val()!="")
            {
               $('.patientEmail').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientEmail').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientEmail').html('');
            },3000);
            if($('#patientGender').val()!="")
            {
               $('.patientGender').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientGender').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientGender').html('');
            },3000);
            if($('#patientYear').val()!="")
            {
               $('.patientYear').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientYear').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientYear').html('');
            },3000);
            if($('#patientEmrEmail').val()!="")
            {
               $('.patientEmrEmail').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientEmrEmail').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientEmrEmail').html('');
            },3000);
            if($('#patientEmrName').val()!="")
            {
               $('.patientEmrName').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientEmrName').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientEmrName').html('');
            },3000);
            if($('#patientEmrPhone').val()!="")
            {
               $('.patientEmrPhone').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientEmrPhone').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientEmrPhone').html('');
            },3000);
            if($('#patientRel').val()!="")
            {
               $('.patientRel').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientRel').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientRel').html('');
            },3000);
            if($('#patientCountry').val()!="")
            {
               $('.patientCountry').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientCountry').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientCountry').html('');
            },3000);
            if($('#patientState').val()!="")
            {
               $('.patientState').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientState').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientState').html('');
            },3000);
            if($('#patientDistrict').val()!="")
            {
               $('.patientDistrict').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientDistrict').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientDistrict').html('');
            },3000);
            if($('#patientTahshil').val()!="")
            {
               $('.patientTahshil').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientTahshil').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientTahshil').html('');
            },3000);
            if($('#patientCity').val()!="")
            {
               $('.patientCity').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientCity').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientCity').html('');
            },3000);
            if($('#patientPermanentCounrty').val()!="")
            {
               $('.patientPermanentCounrty').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientPermanentCounrty').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientPermanentCounrty').html('');
            },3000);
            if($('#patientPermanentState').val()!="")
            {
               $('.patientPermanentState').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientPermanentState').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientPermanentState').html('');
            },3000);
            if($('#patientPermanentCity').val()!="")
            {
               $('.patientPermanentCity').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientPermanentCity').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientPermanentCity').html('');
            },3000);
            if($('#patientPermanentDistrict').val()!="")
            {
               $('.patientPermanentDistrict').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientPermanentDistrict').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientPermanentDistrict').html('');
            },3000);
            if($('#patientPermanentTahshil').val()!="")
            {
               $('.patientPermanentTahshil').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientPermanentTahshil').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientPermanentTahshil').html('');
            },3000);
            if($('#patientIdentityType').val()!="")
            {
               $('.patientIdentityType').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientIdentityType').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientIdentityType').html('');
            },3000);
            if($('#patientIdentityNumber').val()!="")
            {
               $('.patientIdnetityNumber').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientIdentityNumber').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientIdentityNumber').html('');
            },3000);
            if($('#patientMaritalStatus').val()!="")
            {
               $('.patientMaritalStatus').html('<i class="fa fa-check-circle-o text-success"></i>');
            }
            else
            {
               $('.patientMaritalStatus').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
            }
            setTimeout(function(){
               $('.patientMaritalStatus').html('');
            },3000);
            if($('input[name="insuranceReq"]:checked').val()=="1")
            {
              if($('#serviceProvider').val()!="")
              {
               $('.serviceProvider').html('<i class="fa fa-check-circle-o text-success"></i>');
              }
              else
              {
               $('.serviceProvider').html('<i class="fa fa-times-circle-o text-danger"></i>');
               valid=false;
              }
              setTimeout(function(){
               $('.serviceProvider').html('');
            },3000);
            }
            else
            {
               $('.serviceProvider').html('');
            }
             //submit patient Form
             if(valid)
             {
               // $('input').prop('readonly',true);
               // $('select').prop('disabled',true);
               $('.patientFormBtn').html('processing..');
               $('.patientFormBtn').prop('disabled',true);
               $.ajax({
                   method:"post",
                   url:"process/patient-registration.php",
                   data:$('form').serialize(),
                   success:function(data){
                       if(data=="success")
                       {
                           
                           Swal.fire({
                           position: 'top',
                           icon: 'success',
                           title: 'Patient Details has been saved successfully',
                           showConfirmButton: false,
                           timer: 1500
                           });
                           setTimeout(function(){
                             location.reload();
                           },3000);
                           
                           
                       }
                       else
                       {
                           
                           if(data=="Patient Details Already Exists" || data=="Please Fill All Fields"){
                              Swal.fire({
                                 position: 'top',
                                 icon: 'error',
                                 title: data,
                                 showConfirmButton: false,
                                 timer:1500
                                 });
                           }
                           else
                           {
                                 $('.'+data).html('<p class="text-danger">Invalid Data !</p>');
                                 setTimeout(function(){
                                    $('.'+data).html('');
                                 },3000);
                           }
                          
                
                       }
                       // $('select').prop('disabled',false);
                       // $('input').prop('disabled',false);
                       $('.patientFormBtn').prop('disabled',false);
                       $('.patientFormBtn').html('submit');
                      
                   }
               }); 
             }
              
           });
    });

    // check Field
    

