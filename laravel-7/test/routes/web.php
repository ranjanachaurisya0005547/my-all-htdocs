<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
use App\Http\Middleware\LoginMiddleware;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/testcomponent',function(){
	return view('testcomponent');
});

//Route::view('/routename','bladefilename')
Route::view('/test','testcomponent');

Route::get('/usercontroller',[UserController::class,'index']);

Route::get('/session_set',[LoginController::class,'session_set']);
Route::get('/session_get',[LoginController::class,'session_get']);
Route::get('/session_remove',[LoginController::class,'session_remove']);
Route::get('/session_check',[LoginController::class,'session_check']);

//Login with session
Route::get('/login',[LoginController::class,'login_form']);
Route::post('/login_auth',[LoginController::class,'login_auth']);
// Route::get('/dashboard',[LoginController::class,'dashboard']);
Route::get('/logout',[LoginController::class,'logout']);
//Middleware Login
Route::group(['middleware'=>['LoginMiddleware']],function(){
    Route::get('/dashboard',[LoginController::class,'dashboard']);
    Route::view('/details','details');

});