<html>
	<head>
		<title>Login Here...</title>
		<style>
			form{
				background:#ddd;;
				width:30%;
				height:300px;
				margin:3% 24%;
				border:2px solid gray;
				border-radius:10px;
				padding:6% 8%;
			}
			input[type=email],input[type=password],input[type=submit]{
				width:100%;
				height:42px;
				border-radius: 3px;
				outline:none;
				padding-left:10px;
				font-size:19px;
			}
			input[type=email]:focus,input[type=password]:focus,input[type=submit]:focus{
				border:2px solid orange;
			}
			input[type=submit]:hover{
				background:orange;
				border:2px solid orange;
				color:white;
			}

           .error_msg{
           	 color:red;
           	 font-size:19px;
           }
		</style>
	</head>
	<body>
		<center><h1 style="font-family:verdana;text-shadow:3px 3px 4px orange;color:black;">Login Form...</h1></center>
	
		<form method="POST" action="login_auth">
			@csrf
			<span class="error_msg">
				{{session('msg')}}
			</span>
			<p>
				Enter Email<br/>
				<input type="email" name="email" placeholder="ex-ranjana@gmail.com" />
				<span class="error_msg">

				  @error('email')
				      {{$message}}
				  @enderror

			</span>
			</p>
			<p>
				Enter Password<br/>
				<input type="password" name="password" placeholder="ex-ran1234@%WOW" />

				<span class="error_msg">

				  @error('password')
				      {{$message}}
				  @enderror
				  
			</span>

			</p>
			<input type="submit" value="LOGIN" name="lg-btn"/>
		</form>

	</body>
</html>