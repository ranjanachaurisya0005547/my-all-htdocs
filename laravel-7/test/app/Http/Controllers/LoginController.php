<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class LoginController extends Controller
{
    //
    public function index(){
    	echo "login controller !";
    }

    public function session_set(Request $request){

        $request->session()->put('name','ranjana');

    }

    public function session_get(Request $request){

    	$value = $request->session()->get('name');
    	echo $value;

    }

    public function session_remove(Request $request){

         $request->session()->forget('name');
    }

    public function session_check(Request $request){

          if(!$request->session()->has('name')){
          	   echo "Session expired !";
          }else{
          	    echo $request->session()->get('name');
          }

    }

   public function login_form(){

    return view('login');

   } 

   public function login_auth(Request $request){

   	   $request->validate([
             'email' => ['required','email'],
             'password' => ['required',
            'string',
            'min:8',             // must be at least 10 characters in length
            'regex:/[a-z]/',      // must contain at least one lowercase letter
            'regex:/[A-Z]/',      // must contain at least one uppercase letter
            'regex:/[0-9]/',      // must contain at least one digit
            'regex:/[@$!%*#?&]/'],
   	   ]);

      $email = $request->get('email');
      $password =$request->get('password');

      if($email=="ranjana@gmail.com" && $password=='ran1234#WOW'){

      	$request->session()->put('email','ranjana@gmail.com');
      	if($request->session()->has('email')){
           
           return redirect('dashboard');

      	}else{
      		 $request->session()->flash('msg','Session Expired !');
             return redirect('login');
      	}
      
      }else{

        $request->session()->flash('msg','Invailid Email id or password');
        return redirect('login');

      }

   }

   public function dashboard(Request $request){
      return view('dashboard');
   }

   public function logout(Request $request){
   	   $request->session()->forget('email');
   	   $request->session()->flush();
   	   $request->session()->flash('msg','Logout Successfully !');
   	   return redirect('login');
   }

}
