<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
           if(!$request->session()->has('email')){
             $request->session()->flash('msg','Sorry Access Denied !');
             return redirect('login');
           }

        return $next($request);
    }
}
