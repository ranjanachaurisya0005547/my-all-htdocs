<?php
require_once __DIR__."/../dbconnect/settings.php";
require_once __DIR__."/../functions/functions.php";
require_once __DIR__."/../functions/errors.php";

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>User Registration......</title>
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css"/>

     <link rel="stylesheet" type="text/css" href="../css/userhandling.css"/>

</head>
<body>
<div class="container">
	<div class="row ml-1">
		<span class="spn-error">
		<?php
            echo show_flash();
		?>
	</span>
	</div>
	<div class="row reg-txt">
		<p>&nbsp;Candidate Registration...............</p>
	</div>
	<div class="col-md-12 form-area">
		<form action="send_reg_data.php?/=<?php echo basename($_SERVER['PHP_SELF']); ?>" method="POST">
			<div class="form-group">
				<label for="name">Name:</label>
				<input type="text" style="text-transform:capitalize;" autocomplete="off" class="form-control" id="name" name="uname" placeholder="Enter Your Name"/>
				<span class="spn-error">
					<?php
                        echo show_flash('name');
					?>
				</span>
			</div>
			<div class="form-group">
				<label for="email">Username</label>
				<input type="email" id="email" autocomplete="off" name="email" placeholder="Enter Your Email" class="form-control"/>
				<span class="spn-error">
					<?php
                        echo show_flash('email');
					?>
				</span>

			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" id="password" autocomplete="off"  name="password" class="form-control" placeholder="Enter Your Password" />
				<span class="spn-error">
					<?php
                        echo show_flash('password');
					?>
				</span>

			</div>
			<div class="form-group">
				<label for="cpassword">Confirm Password</label>
				<input type="password" id="cpassword" autocomplete="off"  name="cpassword" class="form-control" placeholder="Enter Your Confoim Password" />
				<span class="spn-error">
					<?php
                        echo show_flash('cpassword');
					?>
				</span>

			</div><br/>
			<input type="submit" name="reg_btn" value="Register" class="btn btn-outline-primary form-control">
		</form>
	</div>
</div>
</body>
</html>