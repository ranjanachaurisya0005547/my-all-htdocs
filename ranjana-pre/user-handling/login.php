<?php
require_once __DIR__."/../functions/functions.php";
require_once __DIR__."/../functions/errors.php";

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Login Here............</title>
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css"/>

     <link rel="stylesheet" type="text/css" href="../css/userhandling.css"/>

</head>
<body>
<div class="container">
	<span class="spn-error">
		<?php
		   echo show_flash();
		?>
	</span>
	<div class="row reg-txt">
		<p>&nbsp;Login Here...............</p>
	</div>
	<div class="col-md-12 form-area">
		<form action="userlogin.php?/=<?php echo basename($_SERVER['PHP_SELF']); ?>" method="POST">
			<div class="form-group">
				<label for="email">Username</label>
				<input type="email" id="email" autocomplete="off" name="email" placeholder="Enter Your Email" class="form-control"/>
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" id="password" autocomplete="off"  name="password" class="form-control" placeholder="Enter Your Password" />
			</div><br/>
			<input type="submit" name="reg_btn" value="Register" class="btn btn-outline-primary form-control">
		</form>
	</div>
</div>
</body>
</html>