<?php
require_once __DIR__."/dbconnect/settings.php";
require_once __DIR__."/../functions/functions.php";
require_once __DIR__."/../functions/errors.php";

if($_SERVER['REQUEST_METHOD']=="POST"){
	if(isset($_POST['reg_btn']) and !empty($_POST['reg_btn'])){

           $name=isset($_POST['uname'])?$_POST['uname']:NULL;
           $email=isset($_POST['email'])?$_POST['email']:NULL;
           $password=isset($_POST['password'])?$_POST['password']:NULL;
           $cpassword=isset($_POST['cpassword'])?$_POST['cpassword']:NULL;


           if(check_empty($name)){
           	 header("location:{$_REQUEST['/']}?msg=305&action=uname");
           }elseif(check_vailid_name($name)){
             header("location:{$_REQUEST['/']}?msg=306&action=uname");
           }elseif(check_empty($email)){
           	 header("location:{$_REQUEST['/']}?msg=311&action=email");
           }elseif(check_vailid_email($email)){
           	 header("location:{$_REQUEST['/']}?msg=312&action=email");
           }elseif(check_empty($password)){
           	  header("location:{$_REQUEST['/']}?msg=313&action=password");
           }elseif(check_vailid_password($password)){
           	header("location:{$_REQUEST['/']}?msg=314&action=password");
           }elseif(check_empty($cpassword)){
           	  header("location:{$_REQUEST['/']}?msg=313&action=cpassword");
           }elseif(check_vailid_password($cpassword)){
           	header("location:{$_REQUEST['/']}?msg=314action=cpassword");
           }

           $i=0;
           foreach($_POST as $key=>$value){
           	  if(empty($value)){
           	  	break;
           	  }
           	  $i++;
           }

           if($i==count($_POST)){
           	  if($password==$cpassword){
                 $encypted_password=password_hash($password,PASSWORD_DEFAULT);

                 $data=[
                       "name"=>$name,
                       "email"=>$email,
                       "password"=>$encypted_password
                 ];

                 $obj=new Connection();
                 $result=$obj->insert_data("tbl_registration",$data);

                 if($result==true){
                   header("location:{$_REQUEST['/']}?msg=200");
                 }elseif($result==false){
                    header("location:{$_REQUEST['/']}?msg=201");
                 }

           	  }else{
           	  echo "<script>";
           	  echo "alert('password and confirm password not matched !');";
           	  echo "window.location.href='registration.php';";
           	  echo "</script>";

           	  }

           }else{
           	  echo "<script>";
           	  echo "alert('Please Fill all the fields ,then submit the Form !');";
           	  echo "window.location.href='registration.php';";
           	  echo "</script>";
           }


	}else{
		header("location:{$_REQUEST['/']}?msg=302");
	}
 
 }else{
	header("location:{$_REQUEST['/']}?msg=301");
}