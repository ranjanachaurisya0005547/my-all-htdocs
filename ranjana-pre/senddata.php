<?php

require_once __DIR__.'/dbconnect/settings.php';
include __DIR__.'/functions/functions.php';
include __DIR__.'/functions/errors.php';

if($_SERVER['REQUEST_METHOD']=='POST'){
	if(isset($_POST['reg_btn']) and !empty($_POST['reg_btn'])){

    $patient_id = isset($_POST['patient_id'])?$_POST['patient_id']:NULL;
    $patient_id = sanitise($patient_id);
	$phone_no = isset($_POST['phone_no'])?$_POST['phone_no']:NUll;
	$phone_no=sanitise($phone_no);
	$name = isset($_POST['name'])?$_POST['name']:NULL;
	$name=sanitise($name);
	$dob = isset($_POST['dob'])?$_POST['dob']:NULL;
	$regdate = isset($_POST['regdate'])?$_POST['regdate']:NULL;
	$email =isset($_POST['email'])?$_POST['email']:NULL;
	$email=sanitise($email);
	$gender = isset($_POST['gender'])?$_POST['gender']:NULL;
	$age = isset($_POST['age'])?$_POST['age']:NULL;
	$age=sanitise($age);
	$fname = isset($_POST['fname'])?$_POST['fname']:NULL;
	$fname=sanitise($fname);

	$nemail = isset($_POST['nemail'])?$_POST['nemail']:NULL;
	$nemail=sanitise($nemail);
	$country_code = isset($_POST['mob_no'])?$_POST['mob_no']:NULL;
	$mob_no = isset($_POST['mob_no1'])?$_POST['mob_no1']:NULL; 
	$mob_no=sanitise($mob_no);

	$rel_ship = isset($_POST['rel_ship'])?$_POST['rel_ship']:NULL;
	$country_name = isset($_POST['country_name'])?$_POST['country_name']:NULL;
	$state_name = isset($_POST['state_name'])?$_POST['state_name']:NULL;
	$distt_name = isset($_POST['distt_name'])?$_POST['distt_name']:NULL;
	$city_name = isset($_POST['city_name'])?$_POST['city_name']:NULL;
	$city_name=sanitise($city_name);

	$tahsil = isset($_POST['tahsil'])?$_POST['tahsil']:NULL;
	$tahsil=sanitise($tahsil);

	$country_name1 = isset($_POST['contry_name1'])?$_POST['contry_name1']:NULL;
	$state_name1 = isset($_POST['state_name1'])?$_POST['state_name1']:NULL;
	$distt_name1 = isset($_POST['distt_name1'])?$_POST['distt_name1']:NULL;
	$city_name1 = isset($_POST['city_name1'])?$_POST['city_name1']:NULL;
	$city_name1=sanitise($city_name1);

	$tahsil1 = isset($_POST['tahsil1'])?$_POST['tahsil1']:NULL;
	$tahsil1=sanitise($tahsil1);

	$id_type = isset($_POST['id_type'])?$_POST['id_type']:NULL;
	$card_no = isset($_POST['card_no'])?$_POST['card_no']:NULL;
	$card_no=sanitise($card_no);
	$mstatus = isset($_POST['mstatus'])?$_POST['mstatus']:NULL;
	$insurance = isset($_POST['insurance'])?$_POST['insurance']:NULL;
	$ins_pro = isset($_POST['ins_pro'])?$_POST['ins_pro']:NULL;


/************************** SERVER SIDE VAILIDATION *****************************/
       if(check_empty($phone_no)){
         	header("Location:{$_REQUEST['/']}?msg=303&action=phone_no");
         }elseif(check_vailid_mobile($phone_no)){
         	    header("Location:{$_REQUEST['/']}?msg=304&action=phone_no");
         }elseif(check_empty($name)){
             header("Location:{$_REQUEST['/']}?msg=305&action=name");
      	}elseif(check_vailid_name($name)){
             	header("Location:{$_REQUEST['/']}?msg=306&action=name");
         }elseif(check_empty($fname)){
                header("Location:{$_REQUEST['/']}?msg=305&action=fname");
      	}elseif(check_vailid_name($fname)){
             	header("Location:{$_REQUEST['/']}?msg=306&action=fname");
         } elseif(check_empty($mob_no)){
         	header("Location:{$_REQUEST['/']}?msg=303&action=mob_no1");
         }elseif(check_vailid_mobile($mob_no)){
         	    header("Location:{$_REQUEST['/']}?msg=304&action=mob_no1");
         }elseif(check_empty($city_name)){
                header("Location:{$_REQUEST['/']}?msg=307&action=city_name");
      	}elseif(check_vailid_name($city_name)){
             	header("Location:{$_REQUEST['/']}?msg=308&action=city_name");
         }elseif(check_empty($tahsil)){
                header("Location:{$_REQUEST['/']}?msg=309&action=tahsil");
      	}elseif(check_vailid_name($tahsil)){
             	header("Location:{$_REQUEST['/']}?msg=310&action=tahsil");
         }elseif(check_empty($city_name1)){
                header("Location:{$_REQUEST['/']}?msg=307&action=city_name1");
      	}elseif(check_vailid_name($city_name1)){
             	header("Location:{$_REQUEST['/']}?msg=308&action=city_name1");
         }elseif(check_empty($tahsil1)){
                header("Location:{$_REQUEST['/']}?msg=309&action=tahsil1");
      	}elseif(check_vailid_name($tahsil1)){
             	header("Location:{$_REQUEST['/']}?msg=310&action=tahsil1");
         }elseif(check_empty($email)){
              header("Location:{$_REQUEST['/']}?msg=311&action=nemail");
         } 

    unset($_POST['reg_btn']);

    $i=0;
    foreach($_POST as $key => $value){
      if(empty($value))
        break;
      $i++;
    }

    /****************************** DATA INSERTION **************************/

     if($i==count($_POST)){  
     	$data=[
    	 'patient_id'=>"$patient_id",
        'phone_no'=>"$phone_no",
         'name'=>"$name",
         'dob'=>"$dob",
         'reg_date'=>"$regdate",
         'email'=>"$email",
         'gender'=>"$gender",
         'age'=>"$age",
         'fname'=>"$fname",
         'emergency_email'=>"$nemail",
         'country_code'=>"$country_code",
         'emergency_phone'=>"$mob_no",
         'relationship'=>"$rel_ship",
         'present_country'=>"$country_name",
         'present_state'=>"$state_name",
         'present_district'=>"$distt_name",
         'present_city'=>"$city_name",
         'present_tahasil'=>"$tahsil",
         'permanent_country'=>"$country_name1",
         'permanent_state'=>"$state_name1",
         'permanent_district'=>"$distt_name1",
         'permanent_city'=>"$city_name1",
         'permanent_tahasil'=>"$tahsil1",
         'identity_type'=>"$id_type",
         'card_no'=>"$card_no",
         'marital_status'=>"$mstatus",
         'insurance_status'=>"$insurance",
         'insurance_provider'=>"$ins_pro"

    ];
	  
	  $tablename='tbl_patient';
	$query = new Connection();
	$status=$query->insert_data($tablename,$data);

	if($status==true){
		echo "<script>";
		echo "alert('Data inserted successfully !');";
		echo "window.location.href='ip2.php';";
		echo "</script>";

	}elseif($status==false){
              echo "<script>";
		echo "alert('Data Not Inserted !');";
		echo "window.location.href='ip2.php';";
		echo "</script>";

	}

      
	}else{
		echo "<script>";
		echo "alert('Please Fill The All Neccessary Fields,Then Submit The Form !');";
		echo "window.location.href='ip2.php';";
		echo "</script>";
	}

	}else{
		header("Location:{$_REQUEST['/']}?msg=302&action=reg_btn");
	}

}else{
	header("Location:{$_REQUEST['/']}?msg=301");
}

?>
