<?php
require_once __DIR__.'/dbconnect/settings.php';

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Patient Registration Data..........</title>

	<script src="js/jquery.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
	<link rel="stylesheet" href="font-awesome/css/all.css"/>

</head>
<body>
<div class="container-fluid">

	<!----------------------------user personal details--------------------->
	<div class="row ml-1 mt-3">
		<h4>User Personal Details..........</h4>
	</div>
<table class="table table-striped mb-2">
	<tr class=" bg-dark text-white">
		<td>ID</td>
		<td>Mobile NO.</td>
		<td>Name</td>
		<td>DOB</td>
		<td>Reg.Date</td>
		<td>Email</td>
		<td>Gender</td>
		<td>Age</td>
	</tr>
	<?php  
	       $dbobj = new connection;
  $data_arr=$dbobj->get_data('tbl_patient');
  //echo "<pre>";
  //print_r($data_arr);
  // $phone_arr=$dbobj->get_column('tbl_patient','phone_no');
  // // // echo "<pre>";
  // // // print_r($value);
  // // foreach($value as $key){
  // //   $mobile_arr[]=$key['phone_no']; 
  // // }
  // echo "<pre>";
  // print_r($phone_arr);

  // $email_arr=$dbobj->get_column('tbl_patient','email');
  // // echo "<pre>";
  // // print_r($value);
  // // foreach($value as $key){
  // //   $email_arr[]=$key['email']; 
  // // }
  // echo "<pre>";
  // print_r($email_arr);

// $phone_arr=$dbobj->get_column('tbl_patient','phone_no');
//           foreach($phone_arr as $key){
//             $phone[]=$key['phone_no']; 
//           }
//           echo "<pre>";
//           print_r($phone);


foreach($data_arr as $key):

  ?>
  <tr>
		<td><?php echo $key['fname']; ?></td>
		<td><?php echo $key['phone_no']; ?></td>
		<td><?php echo $key['name']; ?></td>
		<td><?php echo $key['dob']; ?></td>
		<td><?php echo $key['reg_date']; ?></td>
		<td><?php echo $key['email']; ?></td>
		<td><?php echo $key['gender']; ?></td>
		<td><?php echo $key['age']; ?></td>

	</tr>
	<?php
      endforeach;
	?>
  	
</table>

	<!----------------------------Emergency details--------------------->
	<div class="row ml-1 mt-3">
		<h4>User Emergency Contact..........</h4>
	</div>
<table class="table table-striped mb-2">
	<tr class=" bg-dark text-white">
		<td>ID</td>
		<td>Mobile NO.</td>
		<td>Name</td>
		<td>DOB</td>
		<td>Reg.Date</td>
		<td>Email</td>
		<td>Gender</td>
		<td>Age</td>
	</tr>
	<?php  
	       $dbobj = new connection;
  $data_arr=$dbobj->get_data('tbl_patient');
  //echo "<pre>";
  //print_r($data_arr);
  
foreach($data_arr as $key):

  ?>
  <tr>
		<td><?php echo $key['patient_id']; ?></td>
		<td><?php echo $key['phone_no']; ?></td>
		<td><?php echo $key['name']; ?></td>
		<td><?php echo $key['dob']; ?></td>
		<td><?php echo $key['reg_date']; ?></td>
		<td><?php echo $key['email']; ?></td>
		<td><?php echo $key['gender']; ?></td>
		<td><?php echo $key['age']; ?></td>

	</tr>
	<?php
      endforeach;
	?>
  	
</table>
</div>
</body>
</html>