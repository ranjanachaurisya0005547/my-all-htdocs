<?php


//sanitisation
function sanitise($arg){
$arg = strip_tags($arg);
$arg = htmlentities($arg);
$arg = htmlspecialchars($arg);
$arg = trim($arg);
return $arg;
}

//For Showing Error
function show_flash($action='')
{ 
	global $msg;
	$keyname = @$_REQUEST['msg'];
	$error=@$msg[$keyname];
	$request_action = @$_REQUEST['action'];
	
	if($action==$request_action and !empty($action)){
		echo "<span style='color:red' id='spn-error'>$error</span>";//Main Varible		
		echo "<script>";
		echo "setTimeout(function(){
		document.getElementById('spn-error').style='display:none;';
		},6000);";
		echo "</script>";
	}//action compare
	if($action=='' and $request_action==NULL){	
		echo "<span style='color:red' id='spn-error'>$error</span>";//Main Varible
		echo "<script>";
		echo "setTimeout(function(){
		document.getElementById('spn-error').style='display:none;';
		},6000);";
		echo "</script>";
	}
	
}

/*************************SERVER SIDE VAILIDATION FUNCTION********************/
function check_empty($value){
	if(is_null($value) or empty($value)){
		return true;
	}
}

function check_vailid_name($value){
	if(!(preg_match('/^[A-Za-z ]+$/',$value))){
		return true;
	}
}

function check_vailid_email($email_value){
	$email_regex='/[A-Za-z0-9._]{3,}@[A-Za-z]{3,}[.]{1}[A-Za-z.]{2,3}/';
	if(!(preg_match($email_regex,$email_value))){
		return true;
	}

}

function check_vailid_mobile($value){
	if(!preg_match('/^[6-9]{1}[0-9]{9}$/',$value)){
		return true;
	}
}

function check_vailid_password($value){
	$password_regex='/^[A-Z]{1,}[a-z]{1,}[0-9]{1,}[\.*@#\[\]%$!=_]{1,}$/';
	if(!preg_match($password_regex,$value)){
		return true;
	}
}

?>
