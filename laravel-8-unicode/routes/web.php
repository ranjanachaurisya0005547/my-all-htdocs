<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestController;
use App\Http\Controllers\crudController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\AjaxController;
use App\Http\Controllers\NewEmployeeController;
use App\Http\Controllers\comman\CommanController;
use App\Http\Controllers\UserRegistrationController;
use App\Http\Controllers\MyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/user_form', function () {
    return view('form');
});

Route::get('/test',[TestController::class,"hello"]);
Route::get('/user',[TestController::class,'index']);

//laravel crud routes
Route::get('/registration',[crudController::class,'show_form']);
Route::post('/insert',[crudController::class,'insert_data']);

//Employee Registration
Route::get('/employee_registration',function(){
    return view('employee.add');
});

Route::post('/insert_data',[EmployeeController::class,'insert_data']);
// Route::view('/employee_information','employee_data');
Route::get('/employee_information',[EmployeeController::class,'show_data']);
Route::get('/edit/{id}',[EmployeeController::class,'edit'])->where('id','[0-9]+');
Route::post('/update_data',[EmployeeController::class,'update']);
Route::get('/delete/{id}',[EmployeeController::class,'delete'])->where('id','[0-9]+');


// //Employee Registration
// Route::get('/employee_registration1',function(){
//     return view('employee_registration');
// });

// Route::post('/insert_data',[EmployeeController::class,'insert_data']);
// // Route::view('/employee_information','employee_data');
// Route::get('/employee_information',[EmployeeController::class,'show_data']);
// Route::get('/edit/{id}',[EmployeeController::class,'edit']);
// Route::post('/update_data',[EmployeeController::class,'update']);
// Route::get('/delete/{id}',[EmployeeController::class,'delete']);


//Ajax Insertion
Route::view('/ajax_form','ajax.add');
Route::post('/ajax_insertion',[AjaxController::class,'insert_data']);

//Multiple routes 
Route::match(['get','post'],'/test_route',function(){
    return view('test');
});

Route::any('/check_it',function(){
    return "hello Dear Jindagi !";
});

Route::get('/redirection_first_way',function(){
    return redirect('/check_it');
});

//redirect directly from one uri to another uri 
//Temporary redirection
Route::redirect("/redirection_second_way",'/');
//Permanent redirection
Route::redirect("/redirection_second_way_statuscode",'/',301);
//permanent redirection using below method
Route::permanentRedirect('/redirection_by_301_statuscode','/check_it');

//get user id based on the regex vailidation 
Route::get('/regex_route_id/{id}',function($id){
    return $id;
})->where('id','[0-9]+');


Route::get('/regex_route_name/{name}',function($name){
    return $name;
})->where('name','[A-Za-z ]+');

Route::get('regex_name_id_route/{id}/{name}',function($id,$name){
    return "User id = ".$id."<br/>name = ".$name;
})->where('id','[0-9]+','name','[A-Za-z ]+');

//Global regex Vailidation for a particular variable
Route::get('/check_global_regex/{id}',function($id){
    return "This is Global Regex Route !"." <br/>User Id =".$id;
});

//Regex validation by some predefined function of laravel 8
Route::get('/regex_route_by_predefined_function/{id}/{name}',function($id,$name){
    return "User id = ".$id." and User Name = ".$name;
})->whereNumber('id')->whereAlpha('name');

Route::get('/regex_with_name_validation/{name}',function($name){
      return "User name is = ".$name;
})->whereAlphaNumeric('name');

//Not Understant it whereUuid('id') method
// Route::get('/regex_with_uniqueid/{id}',function($id){
//     return "User id = ".$id;
// })->whereUuid('id');

//optional Parameter route
Route::get('/optional_parameter_example/{name?}',function($name="This passed as Default value !"){
    return "This optional parameter Example ! <br/> User name is =".$name;
})->whereAlpha('name');

//Multiple required parameter
Route::get('/multiple_parameter/user/{id}/name/{name}',function($id,$name){
    return "User Id =".$id."<br/>Name =".$name;
});

//Encoded Forward slases
// Route::get('/search/{search}',function($search){
//     return " Search convert into = ".$search;
// })->where("search".".*");

//Named route
Route::get('/Named_route/Example',function(){

    $url=url('example');
    $route_path=route('example');
    return "Url =".$url."<br/> route = ".$route_path;
})->name('example');

Route::get('/named_route/id/{id}/name/{name}',function($id,$name){
    $url=route('named_route',['id'=>'100','name'=>'ranjana']);
    return "Route ".$url;
})->name('named_route');

Route::get('student/details',function()  
{  
  $url=route('student.details'); 
  // return $url; 
 return view('student');  
})->name('student.details'); 

//New Employee Implimentation

Route::view('/employee/registration','employee.add');
Route::post('/employee/registration/data',[NewEmployeeController::class,'insertEmployee']);
Route::get('/show_employees/{offset?}',[NewEmployeeController::class,'showEmployee']);
Route::get('/delete_employees/{id}',[NewEmployeeController::class,'deleteEmployee']);
Route::get('/employee_edit_form/{id}',[NewEmployeeController::class,'editForm']);
Route::post('/update_employee/{id?}',[NewEmployeeController::class,'updateEmployee'])->name('update');

//csv import
Route::get('/importExportView', [MyController::class, 'importExportView']);
Route::get('/csv', [MyController::class, 'exportCSV'])->name('csv');
Route::get('/excel', [MyController::class, 'exportExcel'])->name('excel');
Route::post('/import', [MyController::class, 'import'])->name('import');
Route::get('/employee/pdf_show',[MyController::class, 'viewPDF'])->name('viewpdf');
Route::get('/employee/pdf',[MyController::class, 'createPDF']);



//Middleware Example
//1.Global Middleware
// Route::get('/user_status/{age?}',function(){
//     return view("middleware.success");
// });

// Route::view('/failed',"edit");

Route::get("/controller_subfolder_access",[CommanController::class,"index"]);

//-------------
Route::view("/header_area","header.header");

//Global Middleware Example
// Route::view("/global_route","welcome");
// Route::get("/global_middleware",function(){
//     return "wow ";
// });

//route middleware
Route::view("/route_middle","welcome")->middleware('checkgender');
Route::get("/route_middleware",function(){
    return " wow ";
});

//group middleware
Route::group([],function(){
    Route::get('/ranjana-1',function(){
        return "Ranjana -1";
    });
    Route::get('/ranjana-2',function(){
        return "Ranjana-2";
    })->middleware('checkgender');
    Route::get("/ranjana-3",function(){
        return "Ranjana-3";
    });
});

//global session helper Practice
//session(['key' => 'value']);
Route::get("/session_get_all",function(Request $request){
    return session()->all();
    /*
    output:
{"_token":"N99NqzvxJ2t4r9yUYY1gfBbs5Nw9j0AQbjfhbovx","_previous":{"url":"http:\/\/localhost:7000\/session"},"_flash":{"old":[],"new":[]}}     
    */
});

Route::get("/session_get_particular_value",function(){
    return session()->get("_token");

/*
    output:
    N99NqzvxJ2t4r9yUYY1gfBbs5Nw9j0AQbjfhbovx
*/    
});

Route::get("/session_get_particular_value_2",function(){
    return session()->get('_flash');

    /*
      output:
      {"old":[],"new":[]}
    */
});

Route::get("/seeeion_set_own_key",function(){
    return session("name","amazing");
    /*
     output:amazing
    */
});

Route::get("/session_push",function(){
    session()->push('_previous:name','ranjana chaurasiya');
    return session()->all();
    /*
    Output:
                    {"_token":"N99NqzvxJ2t4r9yUYY1gfBbs5Nw9j0AQbjfhbovx","_previous":{"url":"http:\/\/localhost:7000\/session_get"},"_flash":{"old":[],"new":[]},"_previous:name":["ranjana chaurasiya","ranjana chaurasiya","ranjana chaurasiya","ranjana chaurasiya"],"name":"laravel tutorial"}
    */

});

Route::get("/session_put",function(){
   session()->put("name","laravel tutorial");
   return session()->all();
   /*
     output:
                    {"_token":"N99NqzvxJ2t4r9yUYY1gfBbs5Nw9j0AQbjfhbovx","_previous":{"url":"http:\/\/localhost:7000\/session_push"},"_flash":{"old":[],"new":[]},"_previous:name":["ranjana chaurasiya","ranjana chaurasiya","ranjana chaurasiya","ranjana chaurasiya"],"name":"laravel tutorial"}
   */
});

Route::get("/session_keep",function(){
    session()->keep(["username","status",""]);
    return session()->all();
    /*
     output:
                    {"_token":"N99NqzvxJ2t4r9yUYY1gfBbs5Nw9j0AQbjfhbovx","_previous":{"url":"http:\/\/localhost:7000\/session_put"},"_flash":{"old":[],"new":["username","status",""]},"_previous:name":["ranjana chaurasiya","ranjana chaurasiya","ranjana chaurasiya","ranjana chaurasiya"],"name":"laravel tutorial"}
    */
});

Route::get("/session_get",function(){

    return session("_previous:name");

    /*
     Output:
                    ["ranjana chaurasiya","ranjana chaurasiya","ranjana chaurasiya","ranjana chaurasiya"]
    */
});


// //Session Request Instance 
// Route::get("/session_by_request",function(Request $request){
//    $request->session()->put("admin","Rohit shetty");
//    return $request->session()->get("admin");
// });


//Session Practice
Route::prefix('user')->group(function(){
    Route::get("/registration",[UserRegistrationController::class,"index"]);
    Route::post('/registration_data',[UserRegistrationController::class,"store"])->name('send');
});