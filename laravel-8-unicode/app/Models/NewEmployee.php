<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewEmployee extends Model{

    public $table='newemployees';
	protected $fillable=['name','dob','gender','address','contact','email','pincode','date_of_joining'];
    public $timestamps=false;

	
    //Insert Employee Data
	public function insertData($request){
             $employee=self::create([
                        "name"=>$request->name,
                        "dob"=>$request->dob,
                        "gender"=>$request->gender,
                        "address"=>$request->address,
                        "contact"=>$request->contact,
                        "email"=>$request->email,
                        "pincode"=>$request->pincode,
                        "date_of_joining"=>$request->date_of_join
              ]);
             return $employee->save();
    }



	//Show Employee Data According To Pagination Requirement
	public function showData($start){
			 $data=self::offset($start)->limit(5)->get();
			 return $data;
	}

		

	//Count The Total Employee Available In Database
	public function totalDataCount(){
			$count=self::all()->count();
            return $count;
	}


	//Delete Employee Data
	public function deleteData($employee_id){
			$data=self::select('employee_id')->where('employee_id','=',$employee_id);
			return $data->delete();
	}


    //Find A Single Employee
	public function uniqueUser($id){
			$single_user=self::get()->where('employee_id',$id);
			return $single_user;
	}


	//Update An Employee Data
	public function updateData($request){
			$id=$request->emp_id;
			$result=self::where('employee_id',$id)->update([
                        "name"=>$request->name,
                        "dob"=>$request->dob,
                        "gender"=>$request->gender,
                        "address"=>$request->address,
                        "contact"=>$request->contact,
                        "email"=>$request->email,
                        "pincode"=>$request->pincode,
                        "date_of_joining"=>$request->date_of_join
			 ]);
           
            return $result;
	}


}