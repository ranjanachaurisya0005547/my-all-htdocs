<?php 

namespace App\Http\Middleware;

use Closer;
use Illuminate\Http\Request;

class CheckAge
{

	   public function handle(Request $request,Closer $next){

	   	   if(!$request->age){
	   	   	  return redirect('/middleware');
	   	   }
	   	  return $next($request);
	   }
}