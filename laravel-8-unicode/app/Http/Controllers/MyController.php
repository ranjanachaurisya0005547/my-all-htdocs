<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\NewEmployeesExport;
use App\Imports\NewEmployeesImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\NewEmployee;
use PDF;

class MyController extends Controller
{
     
     public function importExportView()
    {
       return view('employee.import');
    }


    public function exportCSV() 
    {
        return Excel::download(new NewEmployeesExport, 'newemployees.csv');
    }

    public function exportExcel() 
    {
        try{
            return Excel::download(new NewEmployeesExport, 'excelemployee.xlsx');
        }catch(\Exception $ex){
            return redirect('/show_employees')->with('failed',$ex->getMessage());
        }
    }


    public function import() 
    {
        try{
             Excel::import(new NewEmployeesImport,request()->file('file'));  
             return back()->with('success','File Imported Success Fully !');
        }catch(\Exception $ex){
            return back()->with('failed',$ex->getMessage());
        }
    }

    public function viewPDF(){
        $employee_data = NewEmployee::offset(1)->limit(5)->get();
        return view('employee.pdf_view',['employee_data'=>$employee_data]);
    }

     
    public function createPDF() {
      $employee_data = NewEmployee::offset(1)->limit(5)->get();
      $pdf = PDF::loadView('employee.pdf_view',['employee_data'=>$employee_data]);
      return $pdf->download('pdf_file.pdf');
      //dd($employee_data);
    }



}
