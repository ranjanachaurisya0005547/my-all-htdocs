<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employees;


class EmployeeController extends Controller{

	public function insert_data(Request $request){


    // $data= new Employees();
    // dd($data->user());
           $validated=$request->validate([
                'name' => 'bail|required|max:50',
                'dob' => 'required',
                'gender' =>'required',
                'address' => 'required',
                'contact' => 'required|unique.employees|min:10|max:12',
                'email' => 'required|email|unique:employees',
                'pincode' => 'required|max:6',
                'date_of_join' => 'required'

           ]);

           try{

                    $employee=Employees::create([
                        "name"=>$request->name,
                        "dob"=>$request->dob,
                        "gender"=>$request->gender,
                        "address"=>$request->address,
                        "contact"=>$request->contact,
                        "email"=>$request->email,
                        "pincode"=>$request->pincode,
                        "date_of_joining"=>$request->date_of_join
                    ]);

                    $employee->save();
                    return redirect('employee_registration')->with('status',"Insert successfully");

                }catch (\Exception $exception) {
                 return redirect('employee_registration')->with('failed',"Something went wrong !");
                }

            }

//show Employees
  public function show_data(){

    try{

      $employee_data=Employees::all();
      return view('employee_data',['employee_data'=>$employee_data]);
    }catch(\Exception $ex){
        return redirect('employee_registration')->with('failed',"Somthing went wrong !");
    }
      
}


//show a employee in edit form
public function edit($id){
   $specific_employee=Employees::get()->where('id',$id);
  // $specific_employee=Employees::find($id);
   
   return view('edit',['employee'=>$specific_employee]);
}


//update a particular employee
public function update(Request $request){

  $validated=$request->validate([
                'name' => 'bail|required|max:50',
                'dob' => 'required',
                'gender' =>'required',
                'address' => 'required',
                'contact' => 'required|unique.employees|min:10|max:12',
                'email' => 'required|email|unique:employees',
                'pincode' => 'required|max:6',
                'date_of_join' => 'required'

           ]);

   try{

     $emp_id=$request->emp_id;

     $emp_updated_data=Employees::where('id',$emp_id)->update([
        "name"=>$request->name,
        "dob"=>$request->dob,
        "gender"=>$request->gender,
        "address"=>$request->address,
        "contact"=>$request->contact,
        "email"=>$request->email,
        "pincode"=>$request->pincode,
        "date_of_joining"=>$request->date_of_join
     ]);

     if($emp_updated_data==true){     
      return redirect('employee_information')->with('status',"Updated successfully");
    }else{
      return redirect('employee_information')->with('failed',"updation failed");
    }

      }catch(Exception $exception) {
          return redirect('employee_information')->with('failed',"Somthing Went Wrong !");
      }
  }

//delete the employee
  public function delete($id){
         $employee_id=Employees::find($id);
         $status=$employee_id->delete();

    if($status==true){     
      return redirect('employee_information')->with('status',"Deleted successfully");
    }else{
      return redirect('employee_information')->with('failed',"Deletion failed");
    }

    }

}