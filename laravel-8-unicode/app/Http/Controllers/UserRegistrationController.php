<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserRegistration;

class UserRegistrationController extends Controller{

	public function index(){
		return view('session.form');
	}

	public function store(Request $request){
		$data=$request->input();
		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";

     /*  
	   $value = $request->session()->put('email',$data['email']);
	   var_dump($value);
	   output:Null

	   */
	   /*
	   $request->session()->put('email',$data['email']);
       $value=$request->session()->all();
       return $value;
       output:
       				{"_token":"B6QOMQ2dnq5l0b8HklplBFxddUGvA9glD6PEUTfj","_previous":{"url":"http:\/\/localhost:7000\/user\/registration"},"_flash":{"old":[],"new":[]},"email":"ranjan@gmail.com"}
	   */
       /*
	   $request->session()->put('email',$data['email']);
       $value=$request->session()->get('email');
       return $value;
       output:ranjan@gmail.com
       */

       /*
       //Global Session
       session(['user'=>$data['name']]);
       $result=session('user');
       echo $result;
       output:abhishek
       */

       /*
       //Retriving All session Data
       session(['user1'=>"anshul"]);
       session(['user2'=>'gulshan']);
       return $request->session()->all();

       output:
       				{"_token":"B6QOMQ2dnq5l0b8HklplBFxddUGvA9glD6PEUTfj","_previous":{"url":"http:\/\/localhost:7000\/user\/registration"},"_flash":{"old":[],"new":[]},"email":"ranjan@gmail.com","user":"abhishek","user1":"anshul","user2":"gulshan"}

       */

       /*
       //Deleting A particular session value
       $request->session()->forget('user2');
        return $request->session()->all();	

        Output:
        				{"_token":"B6QOMQ2dnq5l0b8HklplBFxddUGvA9glD6PEUTfj","_previous":{"url":"http:\/\/localhost:7000\/user\/registration"},"_flash":{"old":[],"new":[]},"email":"ranjan@gmail.com","user":"abhishek","user1":"anshul"}	

        */

       /*
        //Deleting All the session value
        $request->session()->flush();

        return $request->session()->all();
        Output:	[]

        */

       /*
        //Flash Data
        $request->session()->flash("success","User registration done Successfully !");
                           OR
        session()->flash("success","User registration done Successfully !");
        return $request->session()->get('success');

        Output:User registration done Successfully !
        */

	}
}