<?php 

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\NewEmployee;
use Validator;

class NewEmployeeController extends Controller{


  //insert employee data
  public function insertEmployee(Request $request){
		$validated=$request->validate([
                'name' => 'required|max:50|regex:/^[A-Za-z ]+$/',
                'dob' => 'required',
                'gender' =>'required',
                'address' => 'required',
                'contact' => 'required|unique:newemployees|min:10|max:12|regex:/^[6-9]{1}[0-9]{9}$/',
                'email' => 'required|email|unique:newemployees',
                'pincode' => 'required|max:6',
                'date_of_join' => 'required'
        ]);
	    try{
		    $employee_object=new NewEmployee();

		   if($employee_object->insertData($request)){
			      return redirect('show_employees')->with('status',"Data Insert successfully !");
		   }else{
			      return redirect('employee_registration')->with('failed',"Data Not Inserted !");
		   }
	    }catch(\Exception $ex){
		    return back()->with('failed',$ex->getMessage())->withInput();	
	    }
  }



  //show employee data
  public function showEmployee($start=0){
		$employee_object=new NewEmployee();
		$employee_data=$employee_object->showData($start);
        $total_employee=$employee_object->totalDataCount();
      
         $per_page=5;
         $record=$total_employee;
         $pagination_link=ceil($record/$per_page);
         $start=0;

		return view('employee.show',['employee_data'=>$employee_data,'start'=>$start,'pagination'=>$pagination_link]);      
  }



  //delete employee data
  public function deleteEmployee($id){
        $employee_object=new NewEmployee();
        try{
             if( $employee_object->deleteData($id)){
         	     return redirect('show_employees')->with('status',"Data Deleted successfully !");
             }else{
         	     return redirect("show_employees")->with('failed',"Data Not Deleted !");
             }
        }catch(\Exception $ex){
    	    return back()->with('failed',$ex->getMessage())->withInput();
        }
  }



  //Edit Employee form
  public function editForm($id){
    	$employee_object=new NewEmployee();
    	$single_employee=$employee_object->uniqueUser($id);

        return view('employee.edit',['employee'=>$single_employee]);
  }




  //update employee data
  public function updateEmployee(Request $request,$id){
       $rule=[
                'name'=>'required|max:50',
                'dob'=>'required',
                "gender"=>"required",
                'address'=>'required',
                'contact'=>'required|min:10|max:12',
                'email'=>'required|email',
                'pincode'=>'required|min:6|max:6',
                'date_of_joining'=>'required'
             ];
        $validator=Validator::make($request->all(),$rule);

        if($validator->fails()){
            return redirect("employee_edit_form/{$id}")->withErrors($validator)->withInput();
        }else{
            try{
                 $employee_object=new NewEmployee();
        
                 if($employee_object->updateData($request)){
                    return redirect("/show_employees")->with("status","Data Updated Successfully !");
                 }else{
                    return redirect("/show_employees")->with('failed',"Data Not Updated !");
                 } 

            }catch(\Exception $ex){
                 return redirect('/show_employees')->with("failed",$ex->getMessage())->withInput();
            }
        }      
  }


}