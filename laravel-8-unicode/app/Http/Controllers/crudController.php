<?php

    namespace App\Http\Controllers;
    use Illuminate\Http\Request;
    use App\Models\cruds;
    use Illuminate\Support\Facades\Validator;
   
	class crudController extends Controller{

		public function show_form(){
			return view('form');
		}

		public function insert_data(Request $request){

                  $rule=[
                         "name"=>"required|max:50",
                         "email"=>"bail|unique:cruds|email",
                         "address"=>"required"
                  ];
			      $validator=Validator::make($request->all(),$rule);

			      if($validator->fails()){
                       return redirect('/registration')->withErrors($validator)->withInput();
			      }else{
                     $name=$request->name;
                     $email=$request->email;
                     $address=$request->address;
                     //echo $name." ".$email." ".$address;

                     $insert = new cruds();
                     $insert->name=$name;
                     $insert->email=$email;
                     $insert->address=$address;

                     $insert->save();
                     return redirect('/registration')->with('status','Data Inserted Successfully !');			      	
			      }


		}

	}