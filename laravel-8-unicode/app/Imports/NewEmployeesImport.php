<?php

namespace App\Imports;

use App\Models\NewEmployee;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class NewEmployeesImport implements ToModel, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function model(array $row)
    {
        return new NewEmployee([
              'name'=>$row['name'],
              'dob'=>$row['dob'],
              'gender'=>$row['gender'],
              'address'=>$row['address'],
              'contact'=>$row['contact'],
              'email'=>$row['email'],
              'pincode'=>$row['pincode'],
              'date_of_joining'=>$row['date_of_joining'],
        ]);
    }
}
