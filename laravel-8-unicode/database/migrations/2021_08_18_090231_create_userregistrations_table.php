<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserregistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userregistrations', function (Blueprint $table) {
            $table->integer('user_id',true);
            $table->string('name',70);
            $table->string('email',100)->unique();
            $table->string('password',12)->default('admin');
            $table->string('token');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userregistrations');
    }
}
