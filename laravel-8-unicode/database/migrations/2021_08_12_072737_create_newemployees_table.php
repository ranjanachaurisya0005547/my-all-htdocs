<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewemployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newemployees', function (Blueprint $table) {
            $table->integer('employee_id',true);
            $table->string('name',100)->nullable();
            $table->string('dob',70);
            $table->string('gender',50);
            $table->text('address');
            $table->string('contact',12)->unique();
            $table->string('email',100)->unique();
            $table->integer('pincode',false)->length(6);
            $table->string('date_of_joining',70);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newemployees');
    }
}
