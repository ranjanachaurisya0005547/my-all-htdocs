<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Employee...........</title>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/css/employee_css.css')}}"/>
	
	<script src="{{asset('assets/js/jquery.js')}}"></script>
	<script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/font-awesome/css/all.css')}}"/>


	<style type="text/css">
		
	</style>
</head>
<body>
	<!------------------------------------Menu Area--------------------------------------->
	
<nav class="navbar navbar-expand-lg navbar-light bg-primary">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="{{url('/employee/registration')}}">Add Employee <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{url('show_employees')}}">Show Employee Details</a>
      </li>
  </div>
</nav>
<!------------------------------------ End Menu Area ------------------------------------------>