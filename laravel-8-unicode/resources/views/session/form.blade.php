<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Registration......</title>
	<link rel="stylesheet" type="text/css" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{asset('assets/bootstrap/css/bootstrap.css')}}" />

    <script type="text/javascript" src="{{asset('assets/js/jquery.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
				<h1>User Registration</h1>
				<form action="{{route('send')}}" method="POST">
					@csrf
					<div class="form-group">
						<lable for="name">Enter Your Name</lable>
						<input type="text" name="name" id="name" autocomplete="off" placeholder="Enter Your name Here..." class="form-control"/>
					</div>
					<div class="form-group">
						<label for="email">Enter Your Email</label>
						<input type="email" name="email" id="email" autocomplete="off" placeholder="Enter Your Email Here..." class="form-control"/>
					</div>
					<div class="form-group">
						<label for="password">Enter Your Password</label>
						<input type="password" name="password" id="passwordd" placeholder="Enter Your password Here...." class="form-control" />
					</div>
					<input type="submit" name="sub-btn" value="Save" class="btn btn-primary"/> 
				</form>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>
</body>
</html>