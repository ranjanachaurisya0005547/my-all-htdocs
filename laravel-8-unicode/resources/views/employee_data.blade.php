<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Employees Informations.................</title>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/font-awesome/css/all.css')}}"/>
	
	<script src="{{asset('assets/js/jquery.js')}}"></script>
	<script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>

	<style type="text/css">
		.emp_header{
			min-height: 60px;
			margin-top: 10px;
			margin-left: 10px;
			font-size: 20px;
		    font-weight: bold;
		    text-shadow: 3px 3px 2px orange;
		    }
		    .emp_header p:first-letter{
		    	font-size: 35px;
		    	font-style: italic;
		    	border-bottom: 2px solid orange;
		    }
		    .trash-icon{
		    	color: red;
		    }
	</style>

</head>
<body>
<div class="container-fluid">
	<div class="row emp_header">
		<p>Employee Information</p>
		<a href="{{url('/employee_registration')}}">Add New Employee</a>
	</div>
	<div class="row emp_info">
					@if (session('status'))
<div class="alert alert-success" role="alert">
	<button type="button" class="close" data-dismiss="alert">×</button>
	{{ session('status') }}
</div>
@elseif(session('failed'))
<div class="alert alert-danger" role="alert">
	<button type="button" class="close" data-dismiss="alert">×</button>
	{{ session('failed') }}
</div>
@endif
		<table class="table">
			<tr class="bg-dark text-white">
				<th>Id</th>
				<th>Name</th>
				<th>DOB</th>
				<th>Gender</th>
				<th>Address</th>
				<th>Contact</th>
				<th>Email</th>
				<th>Pincode</th>
				<th>Joining Date</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
			@foreach($employee_data as $key)
			<tr>
				<td>{{$key->id}}</td>
				<td>{{$key->name}}</td>
				<td>{{$key->dob}}</td>
				<td>{{$key->gender}}</td>
				<td>{{$key->address}}</td>
				<td>{{$key->contact}}</td>
				<td>{{$key->email}}</td>
				<td>{{$key->pincode}}</td>
				<td>{{$key->date_of_joining}}</td>
				<td><a href="edit/{{$key->id}}"><span class="fa fa-edit"></span></a></td>
				<td><a href="delete/{{$key->id}}"><span class="fa fa-trash trash-icon"></span></a></td>
			</tr>
			@endforeach
		</table>
	</div>
</div>
</body>
</html>