<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Employee PDf</title>
	
	<link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/css/employee_css.css')}}"/>
	
	<script src="{{asset('assets/js/jquery.js')}}"></script>
	<script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>

</head>
<body>
		<table class="table table-striped">
			<tr class="bg-dark text-white">
				<th>Id</th>
				<th>Name</th>
				<th>DOB</th>
				<th>Gender</th>
				<th>Address</th>
				<th>Contact</th>
				<th>Email</th>
				<th>Pincode</th>
				<th>Joining Date</th>
			</tr>
			@foreach($employee_data as $key)
			<tr>
				<td>{{$key->employee_id}}</td>
				<td>{{$key->name}}</td>
				<td>{{$key->dob}}</td>
				<td>{{$key->gender}}</td>
				<td>{{$key->address}}</td>
				<td>{{$key->contact}}</td>
				<td>{{$key->email}}</td>
				<td>{{$key->pincode}}</td>
				<td>{{$key->date_of_joining}}</td>
			</tr>
			@endforeach
		</table>

</body>
</html>