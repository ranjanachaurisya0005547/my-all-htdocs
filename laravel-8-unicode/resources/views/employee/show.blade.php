@include('comman.header')

<div class="container-fluid mt-3 mb-2">
	<div class="row emp_info">
	@if (session('status'))
<div class="col-sm-12 alert alert-success" role="alert">
	<button type="button" class="close" data-dismiss="alert">×</button>
	{{ session('status') }}
</div>
@elseif(session('failed'))
<div class="col-sm-12 alert alert-danger" role="alert">
	<button type="button" class="close" data-dismiss="alert">×</button>
	{{ session('failed') }}
</div>
@endif

<button class="btn btn-warning mt-2 mb-2 ml-3"><a href="{{url('/csv')}}">CSV</a></button>
<button class="btn btn-info mt-2 mb-2 ml-3"><a href="{{url('/importExportView')}}" style="color:white;">import</a></button>
<button class="btn btn-primary mt-2 mb-2 ml-3"><a href="{{url('/excel')}}" style="color:white;">excel</a></button>
<button class="btn btn-success mt-2 mb-2 ml-3">
	<a href="{{url('/employee/pdf')}}" style="color:white;">Export to PDF</a>
</button>


		<table class="table table-striped">
			<tr class="bg-dark text-white">
				<th>Id</th>
				<th>Name</th>
				<th>DOB</th>
				<th>Gender</th>
				<th>Address</th>
				<th>Contact</th>
				<th>Email</th>
				<th>Pincode</th>
				<th>Joining Date</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
			@foreach($employee_data as $key)
			<tr>
				<td>{{$key->employee_id}}</td>
				<td>{{$key->name}}</td>
				<td>{{$key->dob}}</td>
				<td>{{$key->gender}}</td>
				<td>{{$key->address}}</td>
				<td>{{$key->contact}}</td>
				<td>{{$key->email}}</td>
				<td>{{$key->pincode}}</td>
				<td>{{$key->date_of_joining}}</td>
				<td><a href="{{url('employee_edit_form',$key->employee_id)}}"><span class="fa fa-edit"></span></a></td>
				<td><a href="{{url('delete_employees',$key->employee_id)}}" onclick="return confirm('Are you sure?')"><span class="fa fa-trash trash-icon"></span></a></td>
			</tr>
						@endforeach
		</table>
		</div>

		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-6 pagination-area">
            <ul class="pagination">
				      <?php
					    
                    for($i=1;$i<=$pagination;$i++):
                    	
                  ?>
                  <li>
                  	<a href="{{url('/show_employees/')}}/{{$start}}" class="page-link">
                  		<?php echo $i; ?>
                  	</a>
                  </li>
                <?php
                   $start=$start+5;
                   endfor;
                ?>
            </ul> 
			</div>
			<div class="col-sm-3"></div>
		</div>
</div>

@include('comman.footer')