<!DOCTYPE html>
<html>
<head>
    <title>Employee Import Export Excel to database</title>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/employee_css.css')}}"/>
    
    <script src="{{asset('assets/js/jquery.js')}}"></script>
    <script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>

</head>
<body>
     
<div class="container">

      
      <div class="col-sm-12">
          @if(session('success'))
             <div class="alert alert-success mt-3">{{session('success')}}</div>
          @elseif(session('failed'))
             <div class="alert alert-danger mt-3">{{session('failed')}}</div>
          @endif
      </div>
    

    <div class="card bg-light mt-3">
        <div class="card-header">
            Employee Import Export Excel to database 
        </div>
        <div class="card-body">
            <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" name="file" class="form-control">
                <br>
                <button class="btn btn-success">Import User Data</button>
            </form>
        </div>
    </div>
</div>
   
</body>
</html>
