<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Add Data With Ajax..............</title>
	<link type="text/css" rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
	<link type="text/css" rel="stylesheet" href="bootstrap/css/bootstrap.css" />
	<link type="text/css" rel="stylesheet" href="css/add_css.css"/>

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid outer">
	<div class="row ">
		<div class="col-sm-3"></div>
		<div class="col-sm-6 top_header"><h2>PHP WITH AJAX</h2></div>
		<div class="col-sm-3"></div>
     </div>
		<div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-4 btn_area">
				<button class="btn-lg bg-primary form-control">Load Data</button>
			</div>
			<div class="col-sm-4"></div>
		</div>

<!-----------------------------------DATA SHOW HERE-------------------------------------------->
		<div class="row data-area">
			<div class="col-sm-3"></div>
			<div class="col-sm-6">
				<span id="table_data"></span>
			</div>
			<div class="col-sm-3"></div>
		</div>
     </div>

<script type="text/javascript">
	$(document).ready(function(){
		$("button").on('click',function(e){
			  $.ajax({
			  	url:'fetch_data.php',
			  	type:"POST",
			  	success:function(data){
                  $("#table_data").html(data);
			  	}
			  });
		});
	});
</script>
</body>
</html>