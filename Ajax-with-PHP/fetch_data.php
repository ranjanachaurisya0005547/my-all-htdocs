<?php 

	$conn=mysqli_connect('localhost','root','',"rest_db") or die("Connect Error ".mysqli_connect_error());

	$query="SELECT * FROM tbl_users";

	$result_set=mysqli_query($conn,$query) or die("SQL Query Failed !");
	$row_count=mysqli_num_rows($result_set);

	if($row_count>0){
		$output='<table class="table table-bordered table-striped">
        <tr class="bg-dark text-white">
			<th>ID</th>
			<th>Name</th>
			<th>Email</th>
			<th>Mobile</th>
			<th>Delete</th>
			<th>EDIT</th>
		</tr>';
    
         while($row=mysqli_fetch_assoc($result_set)){
         	$output.="<tr>
               <td>{$row['id']}</td>
               <td>{$row['name']}</td>
               <td>{$row['email']}</td>
               <td>{$row['mobile']}</td>
               <td><button type='button' class='btn btn-danger delete-btn' data-id='{$row['id']}'>DELETE</button></td>
               <td>
                  <button type='button' class='btn btn-primary edit-btn' data-eid='{$row['id']}' data-toggle='modal' data-target='#exampleModalCenter'>EDIT
                  </button>
               </td>
         	</tr>";
         }
         $output.="</table>";
         echo $output;

	}else{
		header("location:add.php?msg=201");
	}
