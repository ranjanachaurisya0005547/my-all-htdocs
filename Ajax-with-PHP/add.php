<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Add Data With Ajax..............</title>
	<link type="text/css" rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
	<link type="text/css" rel="stylesheet" href="bootstrap/css/bootstrap.css" />
	<link type="text/css" rel="stylesheet" href="css/add_css.css"/>
	<link type="text/css" rel="stylesheet" href="font-awesome/css/all.css"/>

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid outer">
	<div class="row ">
		<div class="col-sm-3"></div>
		<div class="col-sm-6 top_header"><h2>PHP WITH AJAX FOR INSERT RECORDS</h2></div>
		<div class="col-sm-3"></div>
     </div>

<!-----------------------------------INSRT DATA HERE-------------------------------------------->
		<div class="row data-area">
			<div class="col-sm-3"></div>
			<div class="col-sm-6">
				<span id="error-message"></span>
	            <span id="success-message"></span><br/><br/>
				<div class="form-group">
				   <label>Enter Name</label>
				   <input type="text" name="name" id="name" class="form-control"/>
                </div>
                <div class="form-group">
				   <label>Enter Email</label>
				   <input type="email" name="email" id="email" class="form-control"/>
                </div>
                <div class="form-group">
				   <label>Enter Mobile Number</label>
				   <input type="text" name="mobile" id="mobile" class="form-control"/>
                </div>
                <input type="submit" name="save_btn" id="save_btn" class="form-control bg-primary text-white"/>
			</div>
			<div class="col-sm-3"></div>
		</div>

	<!-----------------------------------DATA SHOW HERE---------------------------------------->
	

		<div class="row data-area mt-4">
			<div class="col-sm-3"></div>
			<div class="col-sm-6">
				<span id="table_data"></span>
			</div>
			<div class="col-sm-3"></div>
		</div>

    <!------------------------------EDIT USER----------------------------->
    <!-- Button trigger modal -->
<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#exampleModalCenter'>EDIT
                  </button>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      </div>
  </div>
</div>

</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#save_btn").on('click',function(e){
            e.preventDefault();
            
            var uname=$("#name").val();
            var uemail=$("#email").val();
            var umobile=$("#mobile").val();
             
  
			$.ajax({
			  	url : 'insert_data.php',
			  	type : "POST",
			  	data : {name:uname,email:uemail,mobile:umobile},

			  	success : function(data){
			  		if(data==true){
			  			show_data();
			  		    $('#success-message').html("** DATA INSERTED SUCCESSFULLY.").css({"color":"green","font-weight":"bold"}).slideDown();
                        $('#error-message').slideUp();
			  	    }else if(data==false){
			  	    	show_data();
			  		    $('#error-message').html("DATA NOT INSERTED.").css({"color":"red","font-weight":"bold"}).slideDown();
                        $('#success-message').slideUp();
			  	    }else{
			  		    $('#error-message').html("SOMTHING WENT WRONG !").css("color","red").slideDown();
                        $('#success-message').slideUp();
			  	    }
			  	}
			});
		});

		//show data
		function show_data(){
			$.ajax({
			  	url:'fetch_data.php',
			  	type:"POST",
			  	success:function(data){
                  $("#table_data").html(data);
			  	}
			  });

		}
		show_data();

		//delete data
		$(document).on('click','.delete-btn',function(){
			 var user_id=$(this).data('id');
             var element=this;

             if(confirm("Really Do You want to delete this record ?")){
			 $.ajax({
                  url:'delete.php',
                  type:'POST',
                  data:{id:user_id},
                  success:function(data){
                  	if(data==true){
                        $(element).closest('tr').fadeOut();
                        $('#success-message').html("** DATA DELETED SUCCESSFULLY.").css("color","green").slideDown();
                        $('#error-message').slideUp();
                  	}else{
                         $('#error-message').html("DATA NOT DELETED.").css("color","red").slideDown();
                        $('#success-message').slideUp();
                  	}
                  	
                  }
			 });
		}else{
			 $('#error-message').html("RECORD NOT DELETED !").css("color","red").slideDown();
             $('#success-message').slideUp();

		}
		});

		//edit data
		$(document).on('click','.edit-btn',function(){
			 var user_id=$(this).data('eid');
             var element=this;

             alert(user_id);

   		});

	});
</script>
</body>
</html>