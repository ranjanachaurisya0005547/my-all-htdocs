<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use PDO;

class pdoController extends Controller
{
    private $PDO;
    /**
    * Construct create PDO instance
    */
    public function __construct()
    {
    $this->PDO = DB::connection()->getPdo();
    }
    public function query_check()
    {
        $myPDO = new PDO('pgsql:host=167.179.99.134;
        dbname=test-data', 'postgres', 'dna#123');

        $latlng = 21.4843706;
        $radius=50.37005070667;
        $lng=39.1152482;
        //dd($latlng);
        // $result = $myPDO->query("SELECT * FROM public.v2_boundries")->fetchAll();
        // dd($result);

        // $result = $myPDO->query("SELECT sum(tpop) + (( (sum(tpop) * avg(pg_rate) ) / 100)/365) * (current_date - '2015-12-31'::date) Tot,sum(tmale) Male,sum(tfemale) Female, sum(areakm) areakm, sum(income) income,sum(houses) houses , sum(case sec when 'Poor' then 1 else 0 end) poor, sum(case sec when 'Rich' then 1 else 0 end) rich,sum(case sec when 'Mid Class' then 1 else 0 end) midclass,sum(case sec when 'New Area' then 1 else 0 end) newarea, sum(ag1_m) ag1_m, sum(ag2_m) ag2_m, sum(ag3_m) ag3_m, sum(ag4_m) ag4_m, sum(ag5_m) ag5_m, sum(ag6_m) ag6_m, sum(ag7_m) ag7_m, sum(ag8_m) ag8_m, sum(ag9_m) ag9_m, sum(ag10_m) ag10_m, sum(ag11_m) ag11_m, sum(ag12_m) ag12_m, sum(ag13_m) ag13_m, sum(ag14_m) ag14_m, sum(ag15_m) ag15_m, sum(ag16_m) ag16_m, sum(ag17_m) ag17_m, sum(ag1_f) ag1_f, sum(ag2_f) ag2_f, sum(ag3_f) ag3_f, sum(ag4_f) ag4_f, sum(ag5_f) ag5_f, sum(ag6_f) ag6_f, sum(ag7_f) ag7_f, sum(ag8_f) ag8_f, sum(ag9_f) ag9_f, sum(ag10_f) ag10_f, sum(ag11_f) ag11_f, sum(ag12_f) ag12_f, sum(ag13_f) ag13_f, sum(ag14_f) ag14_f, sum(ag15_f) ag15_f, sum(ag16_f) ag16_f, sum(ag17_f) ag17_f, sum(case location when 'Residential' then 1 else 0 end) res_val, sum(case location when 'Industrial' then 1 else 0 end) indus_val, sum(case location when 'Commercial' then 1 else 0 end) comm_val,Avg(pg_rate) gratio,avg(hh_size) p_hh FROM public.v2_population");

        $result = $myPDO->query("SELECT sum(tpop) + (( (sum(tpop) * avg(pg_rate) ) / 100)/365) * (current_date - '2015-12-31'::date) Tot,sum(tmale) Male,sum(tfemale) Female, sum(areakm) areakm, sum(income) income,sum(houses) houses , sum(case sec when 'Poor' then 1 else 0 end) poor, sum(case sec when 'Rich' then 1 else 0 end) rich,sum(case sec when 'Mid Class' then 1 else 0 end) midclass,sum(case sec when 'New Area' then 1 else 0 end) newarea, sum(ag1_m) ag1_m, sum(ag2_m) ag2_m, sum(ag3_m) ag3_m, sum(ag4_m) ag4_m, sum(ag5_m) ag5_m, sum(ag6_m) ag6_m, sum(ag7_m) ag7_m, sum(ag8_m) ag8_m, sum(ag9_m) ag9_m, sum(ag10_m) ag10_m, sum(ag11_m) ag11_m, sum(ag12_m) ag12_m, sum(ag13_m) ag13_m, sum(ag14_m) ag14_m, sum(ag15_m) ag15_m, sum(ag16_m) ag16_m, sum(ag17_m) ag17_m, sum(ag1_f) ag1_f, sum(ag2_f) ag2_f, sum(ag3_f) ag3_f, sum(ag4_f) ag4_f, sum(ag5_f) ag5_f, sum(ag6_f) ag6_f, sum(ag7_f) ag7_f, sum(ag8_f) ag8_f, sum(ag9_f) ag9_f, sum(ag10_f) ag10_f, sum(ag11_f) ag11_f, sum(ag12_f) ag12_f, sum(ag13_f) ag13_f, sum(ag14_f) ag14_f, sum(ag15_f) ag15_f, sum(ag16_f) ag16_f, sum(ag17_f) ag17_f, sum(case location when 'Residential' then 1 else 0 end) res_val, sum(case location when 'Industrial' then 1 else 0 end) indus_val, sum(case location when 'Commercial' then 1 else 0 end) comm_val,Avg(pg_rate) gratio,avg(hh_size) p_hh FROM public.v2_population where ST_Intersects(st_setsrid(ST_GeomFromText(' ".$latlng." '),4326),st_setsrid(geom,4326)) and tpop > 0 and city in (SELECT distinct boundryname FROM public.v2_boundries where ST_Intersects(st_setsrid(ST_GeomFromText(' ".$latlng." '),4326),st_setsrid(geom,4326)) and levelid='3')");

            return $result->fetch();
            //dd($result->fetchAll());
            return $result->fetchAll(PDO::FETCH_NUM);
    }
}
