<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Http\Request as Request;
define('SITE_PATH','http://localhost:8000/laravel/xyz/');

Route::get('/', function () {
    return view('welcome');
});

#Creating our routes
Route::get('/home',function() {
     return "<h1>Welome to the Home</h1>";	
});

Route::get('/student-edit/{id}',function($id){

	$student=[
        [
             'id'=>10,
             'name'=>'ranjana',
             'class'=>'12th',
             'marks'=>'100'
        ],
        [
           'id'=>11,
           'name'=>'rekha',
           'class'=>'10th',
           'marks'=>'88'
        ]
	];

	// echo "<pre>";
	// print_r($student);

     $match=0;
	foreach($student as $i => $value){

           if($id==$student[$i]['id']){
           	    $std_id=$student[$i]['id'];
           	    $std_name=$student[$i]['name'];
           	    $std_class=$student[$i]['class'];
           	    $std_marks=$student[$i]['marks'];
           	    
           	    $match++;
           	    break;
           }
           

	}

	//Heredoc

   if($match>0):
	echo <<<"FORM"
   	 <form action="update" method="POST">
         ID:<input type="text" name="id" value="{$std_id}"/><br/>
         Name:<input type="text" name="name" value="{$std_name}"/><br/>
         CLASS:<input type="text" name="class" value="{$std_class}"/><br/>
         MARKS:<input type="text" name="marks" value="{$std_marks}"/><br/>
    </form>  
FORM;
    else:
    	return "No Record Found";
    endif;	
});

/*****************************************************************************************/
Route::get('/blade-edit/{id}',function($id){
  
  //return view("edit",$data);
  $student="ranjana";
  $class="12th";

  //return view("edit",['id'=>$id,'student'=>$student,'class'=>$class])
  return view("edit",compact(['id','student','class']));
  // return view("edit",compact('id','student','class'));
});

//************************************************************************************

Route::get('/practice1',function(){
  return view('pracice1');
});

/*************************************************************************************/
Route::get('/contact-us',function(){
  $url=SITE_PATH;
  echo "<h1>Contact us Page </h1>";

  echo <<<"FORM"
    <form action="{$url}contactcode" method="GET">
       <p>Enter Name<input type="text" name="name"/></p>
       <p>Enter Mobile Number<input tyep="text" name="mobile"/></p>
       <input type="submit" value="SUBMIT" name="submit">
    </form>
  FORM;
});

Route::get('/contactcode',function(Request $request){
  
  $name=$request->get('name');
  $mobile=$request->get('mobile');

  echo $name,$mobile;
});

//**********************************Addition of two numbers routes**************************************
Route::get('/calculate-math',function(){
echo "<h1>Addition of two Numbers </h1>";
$url=SITE_PATH;
   echo <<<"FORM"
    <form action="{$url}calculation" method="GET">
       <p>Enter First Number<input type="text" name="n1"/></p>
       <p>Enter Second Number<input tyep="text" name="n2"/></p>
       <input type="submit" value="SUBMIT" name="submit">
    </form>
  FORM;
});

Route::get('/calculation',function(Request $request){
  $no1=$request->get('n1');
  $no2=$request->get('n2');

  $res=$no1+$no2;
  echo "Addition Of Two Number =".$res;
});

//******************************************************
Route::get('/add',function(){

   $csrf_token=csrf_token();
   echo <<<"FORM"
    <form action="add-res" method="POST">
    <input type="hidden" name="_token" value="{$csrf_token}">
       <p>Enter First Number<input type="text" name="n1"/></p>
       <p>Enter Second Number<input tyep="text" name="n2"/></p>
       <input type="submit" value="SUBMIT" name="submit">
    </form>
  FORM;
});

Route::post('/add-res',function(Request $request){
        echo "Welcome to new post request";
 });

//working with put,patch and delete request
//*****************PUT REQUEST ******************************
Route::get('/get_request',function(Request $request){

   $csrf_token=csrf_token();
   echo <<<"FORM"
    <form action="put_request" method="POST">
    <input type="hidden" name="_token" value="{$csrf_token}">
    <input type="hidden" name="_method" value="PUT">
    <input type="submit" value="SUBMIT" name="submit">
    </form>
  FORM;
});

Route::put('/put_request',function(Request $request){
  echo "<center><h1>Put request Loaded </h1></center>";
});
//****************************DELETE REQUEST********************************
Route::get('/get_request_delete',function(Request $request){

   $csrf_token=csrf_token();
   echo <<<"FORM"
    <form action="delete_request" method="POST">
    <input type="hidden" name="_token" value="{$csrf_token}">
    <input type="hidden" name="_method" value="DELETE"/>
    <input type="submit" value="SUBMIT" name="submit">
    </form>
  FORM;
});

Route::delete('/delete_request',function(Request $request){
  echo "<center><h1>Delete request Loaded</h1></center>";
});
//*****************PATCH REQUEST**************************
Route::get('/get_request_patch',function(Request $request){

   $csrf_token=csrf_token();
   echo <<<"FORM"
    <form action="patch_request" method="POST">
    <input type="hidden" name="_token" value="$csrf_token">
    <input type="hidden" name="_method" value="PATCH"/>
    <input type="submit" value="SUBMIT" name="submit">
    </form>
  FORM;
});

Route::patch('/patch_request',function(Request $request){
  echo "<center><h1>PATCH request Loaded</h1></center>";
});









