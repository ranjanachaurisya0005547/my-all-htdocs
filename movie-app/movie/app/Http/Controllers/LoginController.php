<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Login;

class LoginController extends Controller
{
    //

    public function login(Request $request){
         
            $myerrors = array();
            foreach($request->all() as $field => $value){
            	if(is_null($value) or empty($value)){
            		$myerrors[$field] = "{$field} is required !";
            	}
            }
        //prx($errors)

       if(count($myerrors)>0){
       	   return view('login',compact('myerrors'));
       }else{
            $login_id = $request->get('login_id');
            $password = $request->get('password');

            $login_count = Login::where('login_id',$login_id)
                            ->where('password',$password)->count();

            if($login_count>0){
              $request->session()->put('admin',array(
                  
                 'login_id'=>$login_id

              ));

            	return redirect()->to(route('admin.dashboard'));           
            	 }else{
       	  $myerrors['login_error']="Invaild login id or Password !";

       	  return view('login',compact('myerrors'));
       }

       }

    }

    //end login function

    public function admindashboard(){
      check_session('admin');
      return view('admin.dashboard');
    }

    //end admindashboard()

    Public function logout(Request $request){
        $request->session()->forget('admin');
        $request->session()->flush();
        return redirect()->to(url('admin'));
    }
}
