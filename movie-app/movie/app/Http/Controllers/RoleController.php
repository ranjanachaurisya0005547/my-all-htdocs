<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Roles;

class RoleController extends Controller
{
    //
    public function insertform(){
    	return view('admin.role.add');
    }

    public function createRole(Request $request){

         $myerrors=array();
         $msg = array();
         foreach($request->all() as $field => $value){
         	if(is_null($value) or empty($value)){
         		$myerrors[$field]="{$field} is required";
         	}
         }

         if(count($myerrors)>0){
               return view('admin.role.add',compact('myerrors'));
         }else{
         	     $role = new Roles;

                 $role->role = $request->role;
                 $role->save();
                 
                 $msg['success']="Data inserted Succeefully !";
                  return view('admin.role.add',compact('msg'));
         }

    }

    public function show(){
	$roles = Roles::all();
	 
	 return view('admin.role.show',compact('roles'));
    
}

public function editform($id){

     $role = Roles::where('id',$id)->get();
     //dd($role);
     return view('admin.role.edit',compact('role'));
}

public function updateRole(Request $request){
        $role_id=$request->id;
        $role_name=$request->role;

        $uprole = Roles::where('id',$role_id)->update([
               'role' => $role_name
        ]);

        return redirect()->to(url('admin/roles/show'));
}


public function deleteRole($id){
     
    $msg = array();
    $role_id = Roles::find($id);
    $roles = Roles::all();
    $del=$role_id->delete();
      if($del==true){
           
        $msg['success']="<span style='color:green;font-size:20px;'>Data Deleted Successfully ...</span>";
           return view('admin.role.show',compact('msg','roles'));
      }else{
        $msg['success']="<span style='color:green;font-size:20px;'>Data Not Deleted Successfully ...</span>";
        return view('admin.role.show',compact('msg','roles'));
      }
}
   
}
