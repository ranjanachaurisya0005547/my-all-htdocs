<form action="{{route('video.save')}}" method="POST" enctype="multipart/form-data">
@csrf
<p>
	Movie Name<br/>
	<input type="text" name="movie_name"/>
</p>

<p>
	Movie Description<br/>
	<textarea name="movie_description"></textarea>
</p>
<p>
	Release Date<br/>
	<input type="date" name="release_date"/>
</p>
<p>
	Movie<br/>
	<input type="file" name="movie"/>
</p>

<input type="submit" name="add_movie" value="ADD"/>
</form>