<a href="{{route('roles.create')}}">Add New Role</a>

<br/><br/>
<?php isset($msg['success'])?print($msg['success']):""; ?>
<h1>All Available Roles Here </h1>

<table border="1" width="100%" cellspacing="0" cellpadding="6" style="text-align:center;">
    <tr>
    	<th>Id</th>
    	<th>Role</th>
    	<th>Created Date</th>
    	<th>Updated Date</th>
    	<th>Edit</th>
    	<th>Delete</th>
    </tr>

@foreach ($roles as $role)
    <tr>
    	<td>{{$role->id}}</td>
    	<td>{{$role->role}}</td>
    	<td>{{$role->created_at}}</td>
    	<td>{{$role->updated_at}}</td>
    	<td><a href="edit/{{$role->id}}">Edit</a></td>
    	<td><a href="delete/{{$role->id}}">Delete</a></td>
    </tr>
 @endforeach
</table>
