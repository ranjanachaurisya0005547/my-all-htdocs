<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

use Illuminate\Http\Request;

################################

#Admin Module

Route::prefix('/admin')->group(function(){
	Route::get('/',function(Request $request){
	           return view('login');
	});

	Route::get('/logout','LoginController@logout')->name('admin.logout');

	Route::post('/login','LoginController@login')->name('admin.login');
    Route::get('/dashboard','LoginController@admindashboard')->name('admin.dashboard');

	// Roles Route
    Route::get('/roles/create','RoleController@insertform')->name('roles.create');
    Route::post('/roles/save','RoleController@createRole')->name('roles.save');
    Route::get('/roles/show','RoleController@show')->name('roles.show');
    Route::get('/roles/edit/{id}','RoleController@editform');
    Route::post('/roles/edit/update','RoleController@updateRole')->name('admin.roles');
    Route::get('/roles/delete/{id}','RoleController@deleteRole');

    #Add Partners
   Route::get('/partner/create','PartnerController@create')->name('partner.create');
   Route::post('/partner/save','PartnerController@save')->name('partner.save');
   Route::get('/partner/show','PartnerController@show')->name('partner.show');

   #end Add Partners

   #Add Video
    Route::get('/video/create','VideoController@create')->name('video.create');
   Route::post('/video/save','VideoController@save')->name('video.save');
   Route::get('/video/show','VideoController@show')->name('video.show');

   #end Add Video

});

#End Admin Module

Route::get('learn/path',function(){
    //Working with Laravel Path File System
    prx(storage_path(),false);
    prx(app_path(),false);
    prx(config_path(),false);

    prx(app_path('Helpers'),false);
    $laravel_session = storage_path('framework\sessions');
    prx($laravel_session);

 });


#Session Routes
 Route::get('learn/session/add',function(Request $request){
    $request->session()->put('name','ravi');

    $name = $request->session()->get('name',NULL);
    echo "The session data is {$name}";
    
 });

 Route::get('learn/session/get',function(Request $request){

    $name = $request->session()->get('name',NULL);
    echo "The session data is still available {$name}";
 });


 Route::get('learn/session/new',function(Request $request){

    $name = $request->session()->get('name','LKG');

    echo "The session data is still available {$name}";
 });

 
 Route::get('learn/session/2',function(Request $request){

    $class = $request->session()->put('class','12th');
    $request->session()->put('marks',array(
        'maths' =>80,
        'hindi' => 100,
        'english' => 50
    ));

 });

 Route::get('learn/session/all',function(Request $request){

   echo '<pre>';
   $data = $request->session()->all();
   print_r($data);

 });

 Route::get('learn/session/class',function(Request $request){
   $class = $request->session()->get('class');
   prx($class);
  });

 Route::get('learn/session/hasclass',function(Request $request){
     prx($request->session()->has('class'));
});

Route::get('learn/session/addroll',function(Request $request){
    $request->session()->put('roll',NULL);
});

Route::get('learn/session/getroll',function(Request $request){
    prx($request->session()->get('roll'),false);
    prx($request->session()->exists('roll'));
});

Route::get('learn/session/destroy',function(Request $request){
    $request->session()->flush();
});


