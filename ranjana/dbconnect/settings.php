<?php

define('HOST','127.0.0.1');
define('PASSWORD','');
define('USER_NAME','root');
define('DB_NAME','patient_db');


class connection{

	/*----------------Connection creation-------------------*/
	public $conn=NULL;
	public $status=false;


	 public function __construct(){
           	try{

				$this->conn = mysqli_connect(HOST,USER_NAME,PASSWORD,DB_NAME);

				if(!$this->conn){
					throw new Exception;
				}

			}catch(exception $ex){
				echo "Connection Error ".$ex;
			}

	    }

	    /********************Insert data*********************/

        public function insert_data($tablename,$data=[]){

        	if($tablename==''){
        		die("Table name can't be empty !");
        	}

        	if($data!=''){
        		$arr_values=array_values($data);
        	  $arr_keys=array_keys($data);
        	  // print_r($arr_values);
        	  // print_r($arr_keys);

        	}
        	        	
        	$columns=implode(",",$arr_keys);
        	$values=implode("','",$arr_values);

    
        	   	$query="INSERT INTO $tablename ($columns) values('$values')";
        	    $result=mysqli_query($this->conn,$query) or die('Query Error '.mysqli_error($this->conn));

      
	            if($result==true){
	  	           if(mysqli_affected_rows($this->conn)>0){
            	     $status=true;
            	     return $status;
            	
	  		         }else{
            	     return $status;
	  		         }

              }else{
            	    return $status;
              }


        	
}

     /**************************Data selection************************/
     public function get_data($tablename,$id=""){
      $query=NULL;

     	if($id!=""){
     		$query="SELECT * FROM $tablename where patient_id=$id";
     	}else{
     		$query="SELECT * FROM $tablename";
     	}

       $result_set=mysqli_query($this->conn,$query);
       $count=mysqli_num_rows($result_set);
       if($count>0){
       	while($row=mysqli_fetch_assoc($result_set)){
       	 $alldata[]=$row;
       	}
       	return $alldata;

       }else{
       	return [];
       }


     }

       /************************Connection close**********************/
        public function __destruct(){
        	if($this->conn!=NULL){
       	   mysqli_close($this->conn);
        	}else{
        		 echo "<script>";
		         echo "alert('Opps ! Mysql server not available the working  state !');";
		         echo "window.location.href='ip2.php';";
		         echo "</script>";

        	}
       }

}
