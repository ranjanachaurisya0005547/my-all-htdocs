
<?php
include __DIR__.'/functions/functions.php';
include __DIR__.'/functions/errors.php';

?>
<!DOCTYPE html>
<html>
<head>
	<title>Patient registration form.........</title>

	<script src="js/jquery.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
	<link rel="stylesheet" href="font-awesome/css/all.css"/>
    <link rel="stylesheet" href="css/regcss.css"/>
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">


</head>
<body>
<div class="container-fluid">
	<!----------------menu area----pt_id------------->
	<div class="row">
<nav class="navbar navbar-dark bg-primary navbar-expand-lg w-100">
                        <a class="navbar-brand ml-2" href="#"><span style="padding:10px;border:1px solid white;border-radius:100%;">EP</span></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
           <li class="nav-item ml-3">
        <a class="nav-link" href="#"><i class="fa fa-bed icons"></i> <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item ml-3">
        <a class="nav-link" href="#"><i class="fa fa-file-signature icons"></i></a>
      </li>
       <li class="nav-item ml-3">
        <a class="nav-link" href="#"><i class="fa fa-hospital-user icons"></i></a>
      </li>
       <li class="nav-item ml-3">
        <a class="nav-link" href="#"><i class="fa fa-wheelchair icons"></i></a>
      </li>
      <li class="nav-item ml-3">
        <a class="nav-link" href="#"><i class="fal fa-procedures icons"></i></a>
      </li>
      <li class="nav-item ml-3 mr-0">
        <a class="nav-link" href="#"><i class="far fa-x-ray icons"></i></a>
      </li>
     <li class="nav-item ml-3">
        <a class="nav-link" href="#"><i class="fal fa-pager icons"></i></a>
      </li> 
     </ul>
     <ul class="navbar-nav">
               <li class="nav-item ml-3 mr-0">
                    <a class="nav-link" href="#"><i class="fa fa-bell icons"></i></a>
                </li>
                <li class="nav-item ml-3 mr-0">
                    <a class="nav-link" href="#"><i class="fa fa-user icons"></i></a>
                </li>
            </ul>
      </div>
</nav>

	</div>
	<!-------------End Menu Area------------------------>


	<!-------------body PArt-1----------------------->
	<div class="row pt-reg">
      <div class="col-md-1 col-sm-1 pt-left-area">
        &nbsp;<i class="fas fa-indent side-bar-icons" aria-hidden="true"></i>

      </div>
      <div class="col-md-11 col-sm-11 pt-right-area">Patient Registration</div> 
  </div>

  <!-------------body PArt-2----------------------->
  <div class="row icons-area">
    <div class="col-md-1 col-sm-1 left-icons-area">
        <i class="fal fa-clock side-bar-icons"></i>
        <i class="fal fa-calendar-alt side-bar-icons"></i>
        <i class="fal fa-plane side-bar-icons"></i>
        <i class="fal fa-calendar side-bar-icons"></i>
        <i class="fal fa-calendar-check side-bar-icons"></i>
        <i class="fal fa-portrait side-bar-icons"></i>
        <i class="far fa-hourglass side-bar-icons"></i>

    </div>

    <!---------------------stages and form------------------------->
    <div class="col-md-11 col-sm-11 form-area">

      <!---------------------Stages-------------------------------->
         <div class="row stages">
            <div class="col-md-2 col-sm-2 stages-part">
               <span class="active">1</span>&nbsp;<i style="margin-top:21px;font-size:16px;"> Patient<i style="position:relative;top:-5px;left:4px;">__________</i></i>            </div>
            <div class="col-sm-3 stages-part">
              <span>2</span>&nbsp;<i style="margin-top:21px;font-size:16px;color:#c4cbd2;">Registration Charge<i style="position:relative;top:-5px;left:4px;">____________</i>
            </i>
            </div>
            <div class="col-sm-2 stages-part">
              <span>3</span>&nbsp;<i style="margin-top:21px;font-size:16px;color:#c4cbd2;">Payment<i style="position:relative;top:-5px;left:4px;">_________</i></i>
            </div>
            <div class="col-sm-3 stages-part">
               <span>4</span>&nbsp;<i style="margin-top:21px;font-size:16px;color:#c4cbd2;">Payment Reciept<i style="position:relative;top:-5px;left:4px;">_____________</i></i>
            </div>
            <div class="col-sm-2 stages-part">
              <span>4</span>&nbsp;<i style="margin-top:21px;font-size:16px;color:#c4cbd2;">Appointment<i style="position:relative;top:-5px;left:4px;">______</i></i>
            </div>
                               
      </div>

      <!---------------------Form-------------------------------->   
<div class="row middile-form-part">

  <div class="col-sm-10 form-part1">
    <!---------------Start Form Tag------------------------->

    <form method="POST" action="senddata.php?/=<?php echo basename($_SERVER['PHP_SELF']);?>">
      <span class="spn-errors">
            <?php 
                echo show_flash();
             ?> 
          </span>
        <div class="row top-form">
          <div class="col-sm-2 mt-5 user-icon pt-0 pl-5">
            <img src="images/user.png" width="100px"/>
          </div>
          <div class="col-sm-4 user-form-submit">
                         
               <div class="form-group row">
    <label class="col-sm-4 pr-0 col-form-label">Patient Id</label>
    <div class="col-sm-8">
      <input type="text" autocomplete="off" name="patient_id" class="form-control" id="patient_id" placeholder="patient id">
      <i id="pt_id" class="error"></i>
        
    </div>
      </div>
    <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i class="start">*</i>Phone No</label>
    <div class="col-sm-8">
      <input type="text" autocomplete="off" name="phone_no" onkeypress="onlyNumbers()" onblur="onlyValidNumber()" class="form-control" id="phone_no" placeholder="Phone No.">
      <i id="ph_no" class="error"></i>
       <?php 
                echo show_flash('phone_no');
        ?>  
       
    </div>
  </div>
   <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i class="start">*</i>Name</label>
    <div class="col-sm-8">
      <input type="text" autocomplete="off" name="name" onblur="onlyCharecter()" class="form-control" id="name" placeholder="Patient Name">
      <i id="pname" class="error"></i>
      <?php 
                echo show_flash('name');
          ?>


    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i class="start">*</i>Dob</label>
    <div class="col-sm-8">
      <input type="date" class="form-control" name ="dob" id="dob" placeholder="dd-mm-yyyy">
      <i id="pdob" class="error"></i>

    </div>
  </div>


        

          </div> 
          <div class="col-sm-3 user-form-submit">
               
               <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i class="start">*</i>Reg.Date</label>
    <div class="col-sm-8">
      <input type="date" class="form-control" name ="regdate" id="regdate" placeholder="dd-mm-yyyy">
      <i id="regd" class="error"></i>

    </div>
  </div>
               <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i class="start">*</i>Email</label>
    <div class="col-sm-8">
      <input type="email" autocomplete="off" class="form-control" name="email" id="email" placeholder="Email Id" required>
      <i id="pemail" class="error"></i>
      <?php 
                echo show_flash('email');
        ?>

    </div>
  </div>
   <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i class="start">*</i>Gender</label>
    <div class="col-sm-8">
      <select class="form-control" id="gender" name="gender">
        <option style="color:#e7eaed;" value="">Please Select Gender</option>
        <option>Male</option>
        <option>Female</option>
        <option>Other</option>
      </select>
      <i id="pgender" class="error"></i>

    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-4 col-form-label"> Age(In Year)</label>
    <div class="col-sm-8">
      <input type="number" autocomplete="off" name="age" class="form-control" placeholder="0">
    </div>
  </div>


          </div>
                    <div class="col-sm-3 user-form-submit">
            
               <b>Emergency Contact</b><br/><br/>
               <div class="form-row">

        <div class="form-group col-md-6">
          <label for="fname"><i class="start">*</i>First Name</label>
          <input type="text" autocomplete="off" onkeypress="onlyCharecter1()" class="form-control" id="fname" name="fname"/>
          <i id="pfname" class="error"></i>
          <?php 
                echo show_flash('fname');
            ?>  

        </div>
        <div class="form-group col-md-6">
          <label for="lname">Email</label>
          <input type="email" autocomplete="off" class="form-control" id="nemail" name="nemail" required/>
          <i id="pnemail" class="error"></i>
           <?php 
                echo show_flash('nemail');
        ?>

        </div>
      </div>
            <div class="form-row">
        <div class="form-group col-md-4 pr-0">
          <label for="fname"><i class="start">*</i>Phone No</label>
          <select class="form-control pl-0" id="country_code" name="mob_no">
            <option>+91 India </option>
          </select>
          <i id="pcountry_code" class="error"></i>
        </div>
        <div class="form-group col-md-8">
          <label for="fname">&nbsp;&nbsp;&nbsp;</label>
          <input type="text" autocomplete="off" onkeypress="onlyNumbers1()" class="form-control" id="mob_no" onblur="onlyValidNumber1()" name="mob_no1"/>
          <i id="pmob_no" class="error"></i>
          <?php 
                echo show_flash('mob_no1');
            ?>  
        </div> 
      </div>
      <div class="form-row">
        <div class="form-group col-md-12">
          <label for="fname"><i class="start">*</i>Relationship</label>
          <select class="form-control" name="rel_ship">
            <option value="">Please Select Relationship </option>
            <option>Mother</option>
            <option>Father</option>
            <option>Brother</option>
            <option>Sister</option>
            <option>cousin</option>
            <option>Husband</option>
            <option>Wife</option>
            <option>Daughter</option>
            <option>Son</option>
            <option>uncle</option>
            <option>Aunty</option>
            <option>Grand Mother</option>
            <option>Grand Father</option>
          </select>
        </div>
        </div>

          </div>

        </div>


        <!--------------------------Address Details------------------------->
        <div class="row bottom-form">
             <div class="col-sm-4 user-form-submit">
              <div class="row txt-area pl-4">
                      <b style="font-size:18px;">Present Address</b>
              </div>
                         
               <div class="row form-group">
    <label class="col-sm-4 col-form-label"><i class="start">*</i>Country</label>
    <div class="col-sm-8">
     <select class="form-control" id="country_name" name="country_name">
      <option>India</option>
      <option>Pakistan</option>
      <option>America</option>
      <option>Austrelia</option>
      <option>Izariel</option>
      <option>Maldives</option>
      <option>China</option>
      <option>Sri lanka</option>
      <option>Africa</option>
     </select>
     <i id="pcountry_name" class="error"></i>
    </div>
  </div>               
               <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i class="start">*</i>State</label>
    <div class="col-sm-8">
     <select class="form-control" id="state_name" name="state_name">
      <option>Andhra Pradesh</option>
      <option>Arunachal Pradesh</option>
      <option>  Assam</option>
      <option>  Bihar</option>
      <option>Chhattisgarh</option>
      <option>  Goa</option>
      <option>Gujarat</option>
      <option>Haryana</option>
      <option>Uttarakhand</option>
      <option>Uttar Pradesh</option>
      <option>Rajasthan</option>
      <option>Punjab</option>
     </select>
     <i id="pstate_name" class="error"></i>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i class="start">*</i>District</label>
    <div class="col-sm-8">
     <select class="form-control" id="distt_name" name="distt_name">
      <option>Sant Kabir Nagar</option>
      <option>Lucknow</option>
      <option>Gorakhpur</option>
      <option>Kanpur</option>
      <option>Basti</option>
      <option>Sant Ravidash Nagar</option>
      <option>Devoria</option>
      <option>Mau</option>
     </select>
     <i id="pdistt_name" class="error"></i>

    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i></i><i class="start">*</i>City/Town/Village</label>
    <div class="col-sm-8">
      <input type="text" autocomplete="off" onkeypress="onlyCharecter2()" class="form-control" id="city_name" name="city_name" placeholder="City/Town/Village">
      <i id="pcity_name" class="error"></i>
      <?php 
                echo show_flash('city_name');
        ?> 

    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i></i><i class="start">*</i>Tahsil</label>
    <div class="col-sm-8">
      <input type="text" id="tahsil" name="tahsil" autocomplete="off" onkeypress="onlyCharecter3()" class="form-control" placeholder="Tahsil">
      <i id="ptahsil" class="error"></i>
      <?php 
                echo show_flash('tahsil');
        ?> 

    </div>
  </div>

          </div> 
          <div class="col-sm-4 user-form-submit">
            <div class="row txt-area pl-4">
               <input type="checkbox" name="filladdress" id="filladdress" style="position:relative;top:7px" onclick="fillAddress()"/>
               &nbsp;&nbsp;<b style="font-size:18px;">Permanent Address</b>  
            </div>             
               <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i class="start">*</i>Country</label>
    <div class="col-sm-8">
     <select class="form-control" id="contry_name1" name="contry_name1">
      <option>India</option>
     </select>
     <i id="pcountry_name1" class="error"></i>

    </div>
  </div>

            
               
               <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i class="start">*</i>State</label>
    <div class="col-sm-8">
     <select class="form-control" id="state_name1" name="state_name1">
      <option>Andhra Pradesh</option>
      <option>Arunachal Pradesh</option>
      <option>  Assam</option>
      <option>  Bihar</option>
      <option>Chhattisgarh</option>
      <option>  Goa</option>
      <option>Gujarat</option>
      <option>Haryana</option>
      <option>Uttarakhand</option>
      <option>Uttar Pradesh</option>
      <option>Rajasthan</option>
      <option>Punjab</option>
     </select>
     <i id="pstate_name1" class="error"></i>

    </div>
  </div>
               
               <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i class="start">*</i>District</label>
    <div class="col-sm-8">
     <select class="form-control" id="distt_name1" name="distt_name1">
      <option>Sant Kabir Nagar</option>
      <option>Lucknow</option>
      <option>Gorakhpur</option>
      <option>Kanpur</option>
      <option>Basti</option>
      <option>Sant Ravidash Nagar</option>
      <option>Devoria</option>
      <option>Mau</option>
     </select>
     <i id="pdistt_name1" class="error"></i>

    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i></i><i class="start">*</i>City/Town/Village</label>
    <div class="col-sm-8">
      <input type="text" autocomplete="off" onkeypress="onlyCharecter4()" class="form-control" id="city_name1"  name="city_name1" placeholder="City/Town/Village">
      <i id="pcity_name1" class="error"></i>
      <?php 
                echo show_flash('city_name1');
        ?> 

    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i></i><i class="start">*</i>Tahsil</label>
    <div class="col-sm-8">
      <input type="text" autocomplete="off" onkeypress="onlyCharecter5()" class="form-control" id="tahsil1" name="tahsil1" placeholder="Tahsil">
      <i id="ptahsil1" class="error"></i>
       <?php 
                echo show_flash('tahsil1');
        ?> 

    </div>
  </div>

          </div>
                    <div class="col-sm-4 user-form-submit">
                      <div class="row txt-area pl-4">
                      <b style="font-size:18px;">Identity Details</b>
                    </div>
               
               <div class="form-group row">
    <label class="col-sm-4 col-form-label">Identity Type</label>
    <div class="col-sm-8">
     <select class="form-control" id="id_type" name="id_type">
      <option value="">Identity Type</option>
      <option>Adhar Card</option>
      <option>Pan Card</option>
      <option>Voter Id Card</option>
     </select>
     <i id="pid_type" class="error"></i>

    </div>
  </div>               
               <div class="form-group row">
    <label class="col-sm-4 col-form-label">Card No</label>
    <div class="col-sm-8">
     <input type="text" autocomplete="off" class="form-control" id="card_no" name="card_no" placeholder="Identity Card Number"> 
     <i id="pcard_no" class="error"></i>
   
   </div>
  </div>
   
               
               <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i class="start">*</i>MaritalStatus</label>
    <div class="col-sm-8">
     <select class="form-control" id="mstatus" name="mstatus">
      <option>Married</option>
      <option>UnMarried</option>
     </select>
     <i id="pmstatus" class="error"></i>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i class="start">*</i>InsuranceRepu..</label>
    <div class="col-sm-8">
     <input type="radio" name="insurance" value="yes" checked="true"/> Yes
     <input type="radio" name="insurance" value="no"/> No
    </div>
  </div>

  <div class="form-group row">
    <label class="col-sm-4 col-form-label"><i class="start">*</i>InsuranceProvider</label>
    <div class="col-sm-8">
     <select class="form-control" id="ins_pro" name="ins_pro">
      <option value="">Please Select Insurance Providers</option>
      <option>We Have No Any Insurance policy</option>
      <option>Apollo Munich Health Insurance Company Limited.</option>
      <option>Star Health & Allied Insurance Company Limited.</option>
      <option>Max Bupa Health Insurance Company Limited.</option>
      <option>Cigna TTK Health Insurance Company Limited.</option>
     </select>
     <i id="pins_pro" class="error"></i>
    </div>
  </div>
            
          </div>

        </div>
        <input type="submit" value="SUBMIT" name="reg_btn" class="btn btn-primary btn-lg" onclick="vailidation()"/>
        <!---------------End Form Tag------------------------->

      </form>
  </div>
  <!---------------------------------side searchbar area----------------->
  <div class="col-sm-2 side-search-bar">
    <div class="row search">Nearest Search</div>
     <div class="row search" style="background:white;"></div>
     <div class="row search">No Data</div>
  </div>

   </div>
    </div>
    <!-----------------------end form start tag----------->
  </div>

<!-----------------Form Vailidation------------------->
<script src="js/vailidate.js"></script>
	
</div>
</body>
</html>
