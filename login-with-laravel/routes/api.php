<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserAPIController;
use App\Http\Controllers\APIFileUploadController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//API
Route::get('/showuser/{id?}',[UserAPIController::class,'showUser']);
Route::post('/storedata',[UserAPIController::class,'store']);
Route::delete('/removeuser/{id}',[UserAPIController::class,'destroy']);
Route::put('/updateuser/{id}',[UserAPIController::class,'update']);

//File Upload Using API
Route::post('/fileupload',[APIFileUploadController::class,'uploadFile']);
