<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\loginController;
use App\Http\Controllers\fileUploadController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\NewEmailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('employee.index');
});

Route::get("/signup",[RegistrationController::class,'index']);
Route::post('/send_data',[RegistrationController::class,'store']);
Route::get('/login',[loginController::class,'index']);
Route::post("/logined_user",[loginController::class,'transferUser'])->name('transfer');
Route::get("/dashboard",[loginController::class,'admindashboard']);
Route::get('/user_info',[loginControllers::class,'show']);
Route::get('/logout',[loginController::class,'logout']);

//file upload 
Route::get('/user/registration',[fileUploadController::class,'show'])->name('form');
Route::post('/user/send/information',[fileUploadController::class,'stores'])->name('send');
Route::get('/user/information',[fileUploadController::class,'showData'])->name('user.data');
Route::get('/user/delete/{id}',[fileUploadController::class,'delete']);
Route::get('/user/editForm',[fileUploadController::class,'editForm']);
Route::put('/user/update',[fileUploadController::class,'update'])->name('update');



//SMTP EMAIL
Route::post('/email',[EmailController::class,'sendEmail']);
Route::view('/mail_view','email.send-mail-form');

//SMTP MAIL
// Route::get('send-mail', function () {
   
//     $details = [
//         'title' => 'Mail from mailTrap',
//         'body' => 'This is for testing email using smtp'
//     ];
   
//     \Mail::to('ranjanachaurisya0005547@gmail.com')->send(new \App\Mail\MyTestMail($details));
   
//     dd("Email is Sent.");
// });

//Another way Of Send Mail
Route::view('/user_mail_view','email.mail-view');
Route::post('/send-mail',[NewEmailController::class,'sendEmail']);


