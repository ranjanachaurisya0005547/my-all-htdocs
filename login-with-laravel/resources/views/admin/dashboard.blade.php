
		
<!--------------------------Hedare Area-------------------------------------------->
  @include('admin.layout.header')


<!------------------------Body Area------------------------------------------------>
			<div class="row message-area">
				@if(session('success'))
	                <div class="col-sm-11 alert alert-success mt-3 mb-3 ml-4" role="alert">
	                    <button type="button" class="close" data-dismiss="alert">x</button>   
	                       {{session('success')}}
	                </div>
	           @endif   
			</div>

			<div class="row dashboard-body-area">
				<h3>Welcome To The Admin Dashboard........</h3>
			</div>
		


<!-------------------------------Footer Area------------------------------------->

@include('admin.layout.footer')