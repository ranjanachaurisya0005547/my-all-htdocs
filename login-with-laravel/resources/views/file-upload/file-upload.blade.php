<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>User Profile Uploading....</title>

	<link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/css/file_upload.css')}}"/>
	
	<script src="{{asset('assets/js/jquery.js')}}"></script>
	<script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/font-awesome/css/all.css')}}"/>


</head>
<body>
<div class="container-fluid outer">

	<div class="row text mt-5 mb-2">
		<div class="col-sm-4"></div>
		<div class="col-sm-3">
		    <h3>Upload Files.......</h3>
	    </div>
		<div class="col-sm-5"></div>
	</div>

    <div class="row error-area">
    	<div class="col-sm-4"></div>
    	<div class="col-sm-4">
    		@if(session('success'))
                 <div class="alert alert-success">{{session('success')}}</div>
    		@elseif(session('failed'))
                 <div class="alert alert-danger">{{session('failed')}}</div>
    		@endif
    	</div>
    	<div class="col-sm-4"></div>
    </div>

	<div class="row section">
		<div class="col-sm-4"></div>
		<div class="col-sm-4 form-area">
		<form action="{{route('send')}}" method="POST" enctype="multipart/form-data">
			<input type="hidden" name="_token" value="{{csrf_token()}}"/>
			<div class="form-group">
				<label class="form-label" for="name">Enter Your Name</label>
				<input type="text" name="name" id="name" class="form-control"/>

				@error('name')
                    <div class="alert alert-danger mt-2 mb-1">{{$message}}</div>
				@enderror
			</div>

			<div class="form-group">
				<label class="form-label" for="mobile">Enter Your Mobile Number</label>
				<input type="text" name="mobile" id="mobile" class="form-control"/>

				@error('mobile')
                   <div class="alert alert-danger mt-2 mb-1">{{$message}}</div>
				@enderror
			</div>

			<div class="form-group">
				<label class="form-label" for="file">Enter Your Profile</label>
				<input type="file" name="profile" id="file" class="form-control"/>

				@error('profile')
                     <div class="alert alert-danger mt-2 mb-1">{{$message}}</div>
				@enderror
			</div>

			<input type="submit" name="sub-btn" class="btn btn-success btn-lg" value="SUBMIT"/>
		</form>
	    </div>
	    <div class="col-sm-4"></div>
    </div>

</div>
</body>
</html>