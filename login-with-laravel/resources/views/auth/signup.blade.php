@include('layout.header')

<!----------------BODY PART --------------------->

<div class="container-fluid signup-form-part">
	    
    <div class="container h-100 mt-5 mb-5">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-12 col-md-9 col-lg-7 col-xl-6">
          <div class="card" style="border-radius: 15px;">
            <div class="card-body p-5">
              <h2 class="text-uppercase text-center mb-5">Sign Up</h2>

              @if(session('failed'))
                  <div class="alert alert-danger">{{session('failed')}}</div>
              @endif

              <form action="{{url('send_data')}}" method="POST">

                @csrf
                <div class="form-outline mb-4">
                  <label class="form-label" for="name">Your Name</label>	
                  <input type="text" name="name" id="name" class="form-control form-control-lg" />
                  @error('name')
                     <div class="alert alert-danger mt-1">{{$message}}</div>
                  @enderror
                </div>

                <div class="form-outline mb-4">
                  <label class="form-label" for="email">Your Email</label>
                  <input type="email" id="email" name="email" class="form-control form-control-lg" />
                  @error('email')
                     <div class="alert alert-danger mt-1">{{$message}}</div>
                  @enderror
                </div>

                <div class="form-outline mb-4">
                  <label class="form-label" for="mobile">Mobile Number</label>
                  <input type="text" id="mobile" name="mobile" class="form-control form-control-lg" />
                  @error('mobile')
                     <div class="alert alert-danger mt-1">{{$message}}</div>
                  @enderror
                </div>

                <div class="form-outline mb-4">
                  <label class="form-label" for="password">Password</label> 	
                  <input type="password" id="password" name="password" class="form-control form-control-lg" />
                  @error('password')
                     <div class="alert alert-danger mt-1">{{$message}}</div>
                  @enderror
                </div>

                <div class="form-outline mb-4">
                  <label class="form-label" for="cpassword">Repeat your password</label>
                  <input type="password" id="cpassword" name="password_confirmation" class="form-control form-control-lg" />
                  @error('password')
                     <div class="alert alert-danger mt-1">{{$message}}</div>
                  @enderror
                </div>

                <div class="form-outline mb-4">
                  <label class="form-label" for="status">Status</label>
                  <input type="radio" id="status" name="status" value="1" />True
                  <input type="radio" id="status" name="status" value="0" />False
                  @error('status')
                     <div class="alert alert-danger mt-1">{{$message}}</div>
                  @enderror
                </div>

                <div class="d-flex justify-content-center">
                  <input type="submit" class="btn btn-primary btn-block btn-lg gradient-custom-4 text-body" value="Register">
                </div>

                <p class="text-center text-muted mt-5 mb-0">Have already an account? <a href="{{url('/login')}}" class="fw-bold text-body"><u>Login here</u></a></p>

              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
</div>


<!------------------ END BODY PART ------------------>
@include('layout.footer')