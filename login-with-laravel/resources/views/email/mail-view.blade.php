<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Send Mail...</title>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/css/dashboard.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/css/user_info.css')}}"/>

	<script src="{{asset('assets/js/jquery.js')}}"></script>
	<script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/font-awesome/css/all.css')}}"/>

	<style type="text/css">
		.col-form-label text{
			height: 100px;
			resize: none;
		}
	</style>
</head>
<body>
<div class="container mt-3">
	@if(session('success'))
          <div class="alert alert-success mt-2 mb-1">{{session('success')}}</div>
	@endif
	<div class="card">
		<div class="card-header">Send Email</div>
		<div class="card-body">
			<div class="card-text">
				<form action="{{url('/send-mail')}}" method="Post">
					@csrf
					<div class="form-group row">
						<label class="col-sm-2 col-form-label" for="femail">From</label>
						<div class="col-sm-10">
							<input type="email" name="sender_email" id="femail" class="form-control"/>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 col-form-label" for="email">To</label>
						<div class="col-sm-10">
							<input type="email" name="reciver_email" id="email" class="form-control"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label" for="subject">Subject</label>
						<div class="col-sm-10">
							<textarea name="subject"rows="2" id="subject" class="form-control mb-3"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label for="txt_msg" class="col-sm-2 col-form-label">Message</label>
						<div class="col-sm-10">
							<textarea name="txt_msg"rows="11" id="txt_msg" class="form-control mb-3"></textarea>
								<input type="submit" name="sub-btn" value="Send" class="btn btn-primary"/>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</body>
</html>