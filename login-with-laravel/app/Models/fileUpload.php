<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class fileUpload extends Model{
     
     protected $table="fileuploads";
     protected $fillable=['name','mobile','profile'];
     public $timestamps=true;
 
}