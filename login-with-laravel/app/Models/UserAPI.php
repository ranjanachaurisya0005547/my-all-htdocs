<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAPI extends Model{
	protected $table="userapis";
	protected $fillable=['name','mobile'];
	public $timestamps=false;

}