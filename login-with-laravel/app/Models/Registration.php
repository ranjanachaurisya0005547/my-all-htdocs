<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class Registration extends Model
{
    protected $table="registrations";
    protected $fillable=['name','email','mobile','password','status'];
    public $timestamps=true;


    //Store User Data
    public function insertData(array $data){
         $name=$data['name'];
         $email=$data['email'];
         $mobile=$data['mobile'];
         $password=$data['password'];
         $status=$data['status'];
         $user=self::create([
             'name'=>$name,
             'email'=>$email,
             'mobile'=>$mobile,
             'password'=>Hash::make($password),
             'status'=>$status
         ]);
         return $user->save(); 
    }


    //Find A Particular User Based On Email 
    public function getUniqueUser($email){
           $data=self::where('email',$email)->get();
           return $data;
    }


}
