<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class NewEmailController extends Controller
{
    
    public function sendEmail(Request $req){
        $details=$req->input();

        Mail::to($details['reciver_email'])->send(new \App\Mail\MyTestMail($details));
        
        return back()->with('success','Congratulation,Email send Successfully !');
    }
}
