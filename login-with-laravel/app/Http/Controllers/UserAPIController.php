<?php 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserAPI;
use Validator;

class UserAPIController extends Controller{

    //show User
	public function showUser($id=null){
		if($id==null){
			return UserAPI::all();
		}else{
			return UserAPI::find($id);
		}
	}

	//Create New User
	public function store(Request $req){
	    $data=$req->input();
        $rules=[
                "name"=>"required|max:70|regex:/^[A-Za-z ]{1,}$/",
                "mobile"=>"required|max:10|regex:/^[6-9]{1}[0-9]{9}$/"
            ];
        $validator=Validator::make($req->all(),$rules);
        if($validator->fails()){
             return response()->json(["msg"=>$validator->errors(),"status"=>302]);
        }else{

           $user=UserAPI::create([
              "name"=>$data['name'],
              "mobile"=>$data['mobile']
		   ]);
		   $status=$user->save();
		   if($status){
			   return response()->json(['msg'=>"Data Inserted Successfully !","status"=>200]);
		   }else{
			   return response()->json(['msg'=>'Data Not Inserted !',"status"=>201]);
		   }
       }
	}

    //delete an existing user
    public function destroy($id){
    	$user=UserAPI::find($id);
    	$status=$user->delete();

    	if($status){
    		return response()->json(["msg"=>"User Deleted Successfully !","status"=>200]);
    	}else{
    		return response()->json(['msg'=>"User Not Deleted !",'status'=>201]);
    	}
    }


    //update existing user
    public function update(Request $req,$id){
    	$updatedUser=UserAPI::where('id',$id)->update([
              'name'=>$req->name,
              'mobile'=>$req->mobile
    	]);

    	if($updatedUser){
    		return response()->json(["msg"=>"User Updated Successfully !","status"=>200]);
    	}else{
    		return response()->json(['msg'=>"User Not Updated !",'status'=>201]);
    	}


    }

}