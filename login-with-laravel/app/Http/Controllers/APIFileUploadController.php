<?php 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\apifileupload;

class APIFileUploadController extends Controller{

	public function uploadFile(Request $req){
        $file=$req->file('userprofile');
         
        $file_name=$file->getClientOriginalName();

        $file_path=public_path()."/apiprofile";

         $file->move($file_path,$file_name);
         $user=new apifileupload();
         $user->profile=$file_name;
         $status=$user->save();

         if($status){
         	return response()->json(['msg'=>"File Uploaded !",'status'=>200]);
         }else{
         	return response()->json(['msg'=>"File Not Uploaded !",'status'=>201]);
         }

	}
}