<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Registration;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{

    //User Login Form
    public function index(Request $request){
        return view('auth.login');
    }


   //After Submit The Login Form
    public function transferUser(Request $request){
         $data=$request->input();
         $user = Registration::where('email', '=', $data['email'])->first();


         if(!$user){
             return redirect('/login')->with('failed','Invailid Username Or Email id !');
         }elseif(!Hash::check($data['password'], $user->password)){
             return redirect('/login')->with('failed','Invailid Password !');
         }else{
            $request->session()->put(['email'=>$data['email'],'password'=>$data['password']]);
             return redirect('/dashboard')->with('success',"Welcome To The Dashboard !");
         }

    }

    

    //transfer to the dashboard 
    public function admindashboard(){
      if(session()->has('email')){
          return view('admin.dashboard');
      }else{
          return redirect('/login')->with('failed',"First ,you have to login !");;
      }
    }


    
    //logout from the dashboard
    Public function logout(Request $request){
        $request->session()->forget('email');
        $request->session()->flush();
        return redirect()->to(url('login'))->with('success',"Logout successfully !");;
    }

}

