<?php
require_once __DIR__."/database/setting.php";
session_start();


?>

<!DOCTYPE html>
<html>
<head>

	<title>All Available Hobbies</title>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="font-awesome/css/all.css"/>

<style type="text/css"></style>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-sm-md mt-4">
			<div class="card">
				<div class="card-header bg-primary text-white h4">
					<div class="card-title">
						Show All Hobbies...
					</div>
				</div>
				<div class="card-body">
					<?php
			     
               if(isset($_SESSION['status'])):
               	?>
               	  <div class="p-3 mb-2 bg-success text-white">
               	    <?php 
               	        echo $_SESSION['status'];
               	        unset($_SESSION['status']);
               	    ?>
               	</div>
           	<?php
               endif;
			?>
					<div class="col-sm-12">
						<table class="table table-hover">
                             <tr class="bg-dark text-white">
								<th>ID</th>
								<th>All Hobbies</th>
								<th>Add Hobbies</th>
								<th>Delete</th>
								<th>Select Hobbies By User</th>
							</tr>
							<?php

                                 $fetch_data = "SELECT * FROM tbl_hobbies order by id desc";
								$rs = mysqli_query($conn,$fetch_data);
								if(mysqli_num_rows($rs)>0):
								while($row=mysqli_fetch_assoc($rs)):
	                        ?>
                              <tr>
                              	<td><?php echo $row['id']; ?></td>                                <td><?php echo $row['hobbies']; ?></td>
                                <td><a href="add_hobbies.php" class="btn btn-primary">Add Hobbies</a></td>
                                <td><a href="delete_added_hobbies.php?id=<?php echo $row['id']; ?>" class="btn btn-primary"><i class="fa fa-trash text-danger"></i></a></td>
                                <td><a href="multiple-select.php" class="btn btn-primary">Add New User</a></td>
                              </tr>
	                        <?php
								endwhile;
								else:
   								echo "<script>";
   								echo "setTimeout(function(){alert('Record Not Found !')},3000);"; 
   								echo "</script>";
								endif; 
							?>
							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>