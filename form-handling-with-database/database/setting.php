<?php

define('HOST',"127.0.0.1");
define('USER','root');
define('PASSWORD','');
define('DB_NAME','form_handling');


try{

	$conn = mysqli_connect(HOST,USER,PASSWORD,DB_NAME);

	if(!$conn){
		throw new Exception;
	}
}catch(Exception $ex){
	echo "Connect Error ".mysqli_connect_error($conn);
	echo $ex->getMessage();
	exit;
}