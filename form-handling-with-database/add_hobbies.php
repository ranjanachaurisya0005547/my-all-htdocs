
<?php
   require_once __DIR__."/database/setting.php";
?>
<!DOCTYPE html>
<html>
<head>
     <title>Add Hobbies......</title>
     <script type="text/javascript" src="js/jquery.js"></script>
     <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
     <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
     <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css"/>
     <style type="text/css">
          .header-area{
               height:60px;
            border-radius:5px 5px 0px 0px;
          }
          .body_area{
               min-height:350px;
               background:#ddd;
               font-family: verdana
               font-size:19px;
          }
          .form_area{
               min-height:350px;
               background:#ddd;
               font-family: verdana
               font-size:19px;

          }
     </style>
</head>
<body>
<div class="container mt-5 rounded">
     <div class="row header-area bg-primary"></div>
     <div class="row body_area">
          <div class="col-sm-3"></div>
          <div class="col-sm-6 form_area">
               <h2 class="text-primary py-3">Add New Hobbies.....</h2>
               <?php
               session_start();
               if(isset($_SESSION['status'])):
                ?>
                  <div class="p-3 mb-2 bg-success text-white">
                    <?php 
                        echo $_SESSION['status'];
                        unset($_SESSION['status']);
                    ?>
                </div>
            <?php
               endif;
      ?>
          <form action="send_new_hobbies.php" method="POST">
               <div class="form-group">
                    <label for="hobbies" class="mb-0">Hobby Name</label>
                    <input type="text" id="hobbies" name="hobbies" class="form-control"/>
               </div>
              
               <input type="submit" value="Add Hobbies" class="btn btn-primary w-25 my-3" name="add_hobbies"/>
          </form>
     </div>
     <div class="col-sm-3"></div>
     </div>
</div>
</body>
</script>
</html>