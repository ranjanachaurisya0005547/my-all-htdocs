<?php

require_once __DIR__."/database/setting.php";
session_start();

if($_SERVER['REQUEST_METHOD']=='POST'){
	if(isset($_POST['mult_select_btn']) and !empty($_POST['mult_select_btn'])){
          
          $user_id = isset($_POST['id'])?$_POST['id']:NULL;  
          $name = isset($_POST['name'])?$_POST['name']:NULL;
          $hobbies_arr = isset($_POST['hobbies'])?$_POST['hobbies']:NULL;
          $hobbies_str = implode(',',$hobbies_arr);
          $update_data = "UPDATE user_hobbies set name='$name',hobbies='$hobbies_str' where user_id=$user_id";
                   
          $result = mysqli_query($conn,$update_data) or die("Query Error ".mysqli_error($conn));
          if($result){
          	$_SESSION['status'] = "Data Updated Successfully !";
          	header("location:multiple-select.php");
          }else{
          	$_SESSION['status'] = "Data Not Updated Successfully !";
          	header("location:multiple-select.php");
          }

       	}else{
		$_SESSION['status'] = "Invailid Button Type !";
		header("location:multiple-select.php");
	}
}else{
	$_SESSION['status'] = "Invalid Request Method !";
	header("location:multiple-select.php");
}