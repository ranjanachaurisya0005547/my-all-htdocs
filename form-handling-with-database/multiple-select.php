
<?php
   require_once __DIR__."/database/setting.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Select Hobbies......</title>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css"/>
	<style type="text/css">
		.header-area{
			height:60px;
            border-radius:5px 5px 0px 0px;
		}
		.body_area{
			min-height:350px;
			background:#ddd;
			font-family: verdana
			font-size:19px;
		}
		.form_area{
			min-height:350px;
			background:#ddd;
			font-family: verdana
			font-size:19px;

		}
	</style>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

</head>
<body>
<div class="container mt-5 rounded">
	<div class="row header-area bg-primary"></div>
	<div class="row body_area">
		<div class="col-sm-3"></div>
		<div class="col-sm-6 form_area">
			<h2 class="text-primary py-3">Select Your Hobbies.....</h2>
			
		<form action="send_data.php?/=<?php echo basename($_SERVER['PHP_SELF']); ?>" method="POST">
			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" id="name" name="name" class="form-control"/>
			</div>
			<div class="form-group">
				<label for="hobbies">Select Hobbies</label>
				<select name="hobbies[]" id="hobbies" class="form-control multi_hobbies" multiple="multiple">
					
					<?php 
                        $get_data = "SELECT * FROM tbl_hobbies";
                        $rs = mysqli_query($conn,$get_data);

                        if(mysqli_num_rows($rs)>0):
                              while($hobbies_arr = mysqli_fetch_assoc($rs)):
                    ?>
                       <option value="<?php echo $hobbies_arr['hobbies']; ?>">
                       	<?php echo $hobbies_arr['hobbies']; ?>                       	
                       </option>

                    <?php
                              endwhile;
                        else:
                           echo "<script>";
                           echo "setTimeout(function(){alert('No Record Found !')},3000);";
                           echo "</script>";
                        endif;
					?>
					<option value="">Reading Books</option>
				</select>
			</div>
			<input type="submit" class="btn btn-primary w-25 my-3" name="mult_select_btn"/>
		</form>
	</div>
	<div class="col-sm-3"></div>
	</div>
</div>
</body>
<script type="text/javascript"></script>
<script type="text/javascript">
	$(".multi_hobbies").select2({
  
});
</script>
</html>